<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.54618932255085</gml:X><gml:Y>50.76720482991597</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.042231863786511</gml:X><gml:Y>51.04384242284442</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                               
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.042231863786511,50.794731942507106</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467600</ogr:osx>
      <ogr:osy>99900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.413036841398313,50.982431705376229</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441300</ogr:osx>
      <ogr:osy>120500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.359155077624886,50.964163885639479</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445100</ogr:osx>
      <ogr:osy>118500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.161207175500611,51.043842422844421</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458900</ogr:osx>
      <ogr:osy>127500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.54618932255085,50.767204829915968</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>432100</ogr:osx>
      <ogr:osy>96500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
