<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.507068121297816</gml:X><gml:Y>50.81039798449643</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.753335017565839</gml:X><gml:Y>51.27895341633376</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.756274541795582,51.276286994761719</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486850</ogr:osx>
      <ogr:osy>153750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.36547076029168,50.970942651739392</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444650</ogr:osx>
      <ogr:osy>119250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.001754390959699,50.863187412974206</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470350</ogr:osx>
      <ogr:osy>107550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.149071459336002,50.810397984496426</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460050</ogr:osx>
      <ogr:osy>101550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.507068121297816,50.914074356087632</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434750</ogr:osx>
      <ogr:osy>112850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.362145950683474,50.901682689481099</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444950</ogr:osx>
      <ogr:osy>111550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.157416646961466,51.213318947051974</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458950</ogr:osx>
      <ogr:osy>146350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.753335017565839,51.278953416333756</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487050</ogr:osx>
      <ogr:osy>154050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.035062510734925,51.176405450598153</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467550</ogr:osx>
      <ogr:osy>142350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
