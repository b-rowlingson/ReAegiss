<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.467087182623585</gml:X><gml:Y>50.78659258550989</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9774848861104365</gml:X><gml:Y>50.97226113109081</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.424470096755933,50.921789838139468</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440550</ogr:osx>
      <ogr:osy>113750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.447093512321079,50.93358926613265</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438950</ogr:osx>
      <ogr:osy>115050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.448058929204803,50.972261131090811</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438850</ogr:osx>
      <ogr:osy>119350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.302864802278848,50.966988962628882</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449050</ogr:osx>
      <ogr:osy>118850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.092759037749764,50.786592585509887</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464050</ogr:osx>
      <ogr:osy>98950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.977484886110436,50.868373002619947</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472050</ogr:osx>
      <ogr:osy>108150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.467087182623585,50.927387474931074</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437550</ogr:osx>
      <ogr:osy>114350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.040149792102702,50.896332689794463</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467600</ogr:osx>
      <ogr:osy>111200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.442021666947954,50.941208712151159</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439300</ogr:osx>
      <ogr:osy>115900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.362556211620655,50.923716518891325</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444900</ogr:osx>
      <ogr:osy>114000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.304066885268509,50.934173687586707</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449000</ogr:osx>
      <ogr:osy>115200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.97820512896345,50.867929672188083</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472000</ogr:osx>
      <ogr:osy>108100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
