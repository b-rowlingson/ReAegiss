<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.669856131519247</gml:X><gml:Y>50.77975465764879</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7483305104354659</gml:X><gml:Y>51.2784504164881</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.376518482789857,50.943575259085151</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443900</ogr:osx>
      <ogr:osy>116200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.842738907312643,51.115761201423247</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481100</ogr:osx>
      <ogr:osy>135800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.118581023447852,51.260241489366749</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461600</ogr:osx>
      <ogr:osy>151600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.669856131519247,50.925888225533882</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423300</ogr:osx>
      <ogr:osy>114100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.05783682200884,50.794858986495221</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466500</ogr:osx>
      <ogr:osy>99900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.361305831580906,50.911120218901139</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445000</ogr:osx>
      <ogr:osy>112600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.350386084547949,50.980301783474211</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445700</ogr:osx>
      <ogr:osy>120300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.080834848893728,50.779754657648787</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464900</ogr:osx>
      <ogr:osy>98200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.748330510435466,51.278450416488099</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487400</ogr:osx>
      <ogr:osy>154000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
