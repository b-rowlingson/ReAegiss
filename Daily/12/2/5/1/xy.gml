<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.520465476702461</gml:X><gml:Y>50.85363895383068</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8441072093771461</gml:X><gml:Y>51.34687611533306</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.330548195138409,50.972995908127736</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447100</ogr:osx>
      <ogr:osy>119500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.027092843388775,51.317066358257549</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467900</ogr:osx>
      <ogr:osy>158000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.279987706128175,50.926833768206833</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450700</ogr:osx>
      <ogr:osy>114400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.844107209377146,51.34687611533306</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480600</ogr:osx>
      <ogr:osy>161500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.099272821594763,50.853638953830675</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463500</ogr:osx>
      <ogr:osy>106400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.215736397150955,50.85537870066603</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455300</ogr:osx>
      <ogr:osy>106500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.475815392432419,51.226417496519282</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436700</ogr:osx>
      <ogr:osy>147600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.520465476702461,50.925370647770364</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>433800</ogr:osx>
      <ogr:osy>114100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.986613124814259,50.873398592986526</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471400</ogr:osx>
      <ogr:osy>108700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
