<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.403061933520046</gml:X><gml:Y>50.927077879615</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8307625746423598</gml:X><gml:Y>51.29398327843568</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                               
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.403061933520046,50.927077879614998</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442050</ogr:osx>
      <ogr:osy>114350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.830874677483583,51.274357908982466</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481650</ogr:osx>
      <ogr:osy>153450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.063435737666601,51.293983278435682</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465400</ogr:osx>
      <ogr:osy>155400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.83076257464236,51.250527367801908</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481700</ogr:osx>
      <ogr:osy>150800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
