<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.453248980498129</gml:X><gml:Y>50.81250017654572</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.070966368459009</gml:X><gml:Y>51.08215147558787</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.358214000970486,50.980794652672763</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445150</ogr:osx>
      <ogr:osy>120350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.334150138337753,51.069684032269357</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446750</ogr:osx>
      <ogr:osy>130250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.453248980498129,50.894051436815452</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438550</ogr:osx>
      <ogr:osy>110650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.278902294708726,50.858933339047205</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450850</ogr:osx>
      <ogr:osy>106850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.295082599886427,50.916584462207851</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449650</ogr:osx>
      <ogr:osy>113250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.070966368459009,50.812500176545718</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465550</ogr:osx>
      <ogr:osy>101850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.174077271850432,51.082151475587871</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457950</ogr:osx>
      <ogr:osy>131750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
