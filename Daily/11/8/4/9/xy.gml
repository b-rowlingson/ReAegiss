<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.824204853954462</gml:X><gml:Y>50.84714119634327</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7777514687805723</gml:X><gml:Y>51.33136562509748</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.489474080247344,51.221532259343185</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435750</ogr:osx>
      <ogr:osy>147050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.384435474716126,50.936872911832239</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443350</ogr:osx>
      <ogr:osy>115450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.824204853954462,50.932966504815603</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>412450</ogr:osx>
      <ogr:osy>114850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.293027383595432,50.864415276709934</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449850</ogr:osx>
      <ogr:osy>107450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.777751468780572,51.331365625097476</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485250</ogr:osx>
      <ogr:osy>159850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.35183549545904,50.97851135372813</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445600</ogr:osx>
      <ogr:osy>120100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.105998413361911,51.24396035537314</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462500</ogr:osx>
      <ogr:osy>149800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.194567842763973,50.847141196343273</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456800</ogr:osx>
      <ogr:osy>105600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.314595572629723,50.89556818486917</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448300</ogr:osx>
      <ogr:osy>110900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.909857948290277,51.298949020391952</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>476100</ogr:osx>
      <ogr:osy>156100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.361207402849388,50.918313667025089</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445000</ogr:osx>
      <ogr:osy>113400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
