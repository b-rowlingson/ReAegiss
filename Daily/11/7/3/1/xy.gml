<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.664695405339324</gml:X><gml:Y>50.75276758035183</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9447427073327859</gml:X><gml:Y>51.15604005050817</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.403831317813667,50.866832088467071</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442050</ogr:osx>
      <ogr:osy>107650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.153377639484749,50.807731483851313</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459750</ogr:osx>
      <ogr:osy>101250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.31425651371368,50.967056370475596</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448250</ogr:osx>
      <ogr:osy>118850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.072563549555652,50.803520109994572</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465450</ogr:osx>
      <ogr:osy>100850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.072919283065776,50.785537355484159</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465450</ogr:osx>
      <ogr:osy>98850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.966850908481497,51.156040050508174</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472350</ogr:osx>
      <ogr:osy>140150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.318222217715945,50.890643519792512</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448050</ogr:osx>
      <ogr:osy>110350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.944742707332786,50.996676578686362</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>474150</ogr:osx>
      <ogr:osy>122450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.097133490161673,50.854072062057938</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463650</ogr:osx>
      <ogr:osy>106450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.000119217856068,50.939611099231826</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470350</ogr:osx>
      <ogr:osy>116050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.358919275633287,50.92954171908687</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445150</ogr:osx>
      <ogr:osy>114650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.315088902987084,50.910408733624791</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448250</ogr:osx>
      <ogr:osy>112550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.269985311430288,50.884057160303513</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451450</ogr:osx>
      <ogr:osy>109650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.664695405339324,50.752767580351829</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423750</ogr:osx>
      <ogr:osy>94850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.403153939336073,50.919884385658946</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442050</ogr:osx>
      <ogr:osy>113550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
