<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.505693322072308</gml:X><gml:Y>50.79590722031059</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7403611546208069</gml:X><gml:Y>51.30255363512205</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.321078559149137,50.889760906262154</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447850</ogr:osx>
      <ogr:osy>110250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.459931582573837,50.930951587769975</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438050</ogr:osx>
      <ogr:osy>114750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.075117385574694,50.817928802850545</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465250</ogr:osx>
      <ogr:osy>102450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.470528737299529,51.000241334263357</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437250</ogr:osx>
      <ogr:osy>122450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.774213309501538,51.302553635122045</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485550</ogr:osx>
      <ogr:osy>156650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.038932797525229,50.851808873243463</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467750</ogr:osx>
      <ogr:osy>106250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.75">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.740361154620807,51.281512255932682</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487950</ogr:osx>
      <ogr:osy>154350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.76">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.04853356352677,50.798830257478883</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467150</ogr:osx>
      <ogr:osy>100350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.77">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.505693322072308,50.909572326338186</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434850</ogr:osx>
      <ogr:osy>112350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.78">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.135145616639472,50.795907220310589</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461050</ogr:osx>
      <ogr:osy>99950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.79">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.979023533134217,50.862990828619203</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471950</ogr:osx>
      <ogr:osy>107550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.80">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.270364317483886,50.859779555011563</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451450</ogr:osx>
      <ogr:osy>106950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.81">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.074071287306347,50.799035693227076</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465350</ogr:osx>
      <ogr:osy>100350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.82">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.299465175440438,51.098256591200105</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449150</ogr:osx>
      <ogr:osy>133450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.83">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.994592970041151,50.865823774372672</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470850</ogr:osx>
      <ogr:osy>107850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.84">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.088663366688388,50.851308437559382</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464250</ogr:osx>
      <ogr:osy>106150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.85">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.164682089954005,50.810510761799463</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458950</ogr:osx>
      <ogr:osy>101550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.86">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.388079269392158,50.984551789772688</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443050</ogr:osx>
      <ogr:osy>120750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
