<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.343056366003327</gml:X><gml:Y>50.96549131888914</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.208900203498191</gml:X><gml:Y>51.04545527113034</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.343056366003327,51.045455271130344</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446150</ogr:osx>
      <ogr:osy>127550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.208900203498191,50.965491318889143</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455650</ogr:osx>
      <ogr:osy>118750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
