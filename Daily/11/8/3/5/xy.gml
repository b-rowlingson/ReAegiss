<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.769160404349497</gml:X><gml:Y>50.75602666685177</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.07205813504282</gml:X><gml:Y>51.28918038582062</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                               
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.490048480073053,50.909505080249041</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435950</ogr:osx>
      <ogr:osy>112350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.081459044298626,51.289180385820622</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464150</ogr:osx>
      <ogr:osy>154850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.769160404349497,50.841148043289436</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>416350</ogr:osx>
      <ogr:osy>104650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.562600497332174,50.756026666851767</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>430950</ogr:osx>
      <ogr:osy>95250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.483907461765637,51.207120684306091</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436150</ogr:osx>
      <ogr:osy>145450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.579059571881055,50.975504049383254</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>429650</ogr:osx>
      <ogr:osy>119650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.132224985281334,50.800381915575358</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461250</ogr:osx>
      <ogr:osy>100450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.07205813504282,50.900637296093002</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465350</ogr:osx>
      <ogr:osy>111650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
