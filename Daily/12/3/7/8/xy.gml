<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.409299340120325</gml:X><gml:Y>50.94014861850548</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.139279988476072</gml:X><gml:Y>51.35038225752015</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.409299340120325,50.940148618505482</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441600</ogr:osx>
      <ogr:osy>115800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.139279988476072,51.226225561934569</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460200</ogr:osx>
      <ogr:osy>147800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.148444071949642,51.350382257520145</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459400</ogr:osx>
      <ogr:osy>161600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
