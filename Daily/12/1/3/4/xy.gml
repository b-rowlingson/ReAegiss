<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.805720563958492</gml:X><gml:Y>50.74624775404725</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9712597824754937</gml:X><gml:Y>51.23841044197337</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.131139503307845,50.782388305074512</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461350</ogr:osx>
      <ogr:osy>98450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.977096948264412,51.145339311035691</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471650</ogr:osx>
      <ogr:osy>138950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.624939995734519,50.760739984253149</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>426550</ogr:osx>
      <ogr:osy>95750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.447952374246298,50.98125298665812</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438850</ogr:osx>
      <ogr:osy>120350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.145498778573703,51.238410441973372</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459750</ogr:osx>
      <ogr:osy>149150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.559824274130211,50.749721357335019</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431150</ogr:osx>
      <ogr:osy>94550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.987043184097625,51.148123546481507</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470950</ogr:osx>
      <ogr:osy>139250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.997350521795341,50.93599032004402</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470550</ogr:osx>
      <ogr:osy>115650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.317236326349119,51.055198867641757</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447950</ogr:osx>
      <ogr:osy>128650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.431561273678515,50.923623047135635</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440050</ogr:osx>
      <ogr:osy>113950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.600685030348344,50.778644795116996</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>428250</ogr:osx>
      <ogr:osy>97750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.742517207716753,50.779043769591404</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>418250</ogr:osx>
      <ogr:osy>97750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.06219929308462,50.825019917807822</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466150</ogr:osx>
      <ogr:osy>103250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.971259782475494,51.150683516103406</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472050</ogr:osx>
      <ogr:osy>139550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.593873647405216,50.746247754047246</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>428750</ogr:osx>
      <ogr:osy>94150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.805720563958492,50.929340317369437</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>413750</ogr:osx>
      <ogr:osy>114450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.648424091668991,50.748673006205991</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>424900</ogr:osx>
      <ogr:osy>94400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.174937741191391,50.832615831875891</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458200</ogr:osx>
      <ogr:osy>104000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
