<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.673981144013982</gml:X><gml:Y>50.74245233052246</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7508849224590756</gml:X><gml:Y>51.28349010666646</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.139164030054066,50.963199837047796</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460550</ogr:osx>
      <ogr:osy>118550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.666080973486892,50.75726787606996</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423650</ogr:osx>
      <ogr:osy>95350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.844981827308157,51.283490106666456</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480650</ogr:osx>
      <ogr:osy>154450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.445723946337442,50.929086562171939</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439050</ogr:osx>
      <ogr:osy>114550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.402287197003008,50.932019763704638</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442100</ogr:osx>
      <ogr:osy>114900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.986574135341077,50.8751967978585</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471400</ogr:osx>
      <ogr:osy>108900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.385164785226797,50.935527885917203</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443300</ogr:osx>
      <ogr:osy>115300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.084422232622988,50.813955432787026</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464600</ogr:osx>
      <ogr:osy>102000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.089151901421491,50.78971196002265</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464300</ogr:osx>
      <ogr:osy>99300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.1829858414168,50.859650420348629</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457600</ogr:osx>
      <ogr:osy>107000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.673981144013982,50.742452330522461</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423100</ogr:osx>
      <ogr:osy>93700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.432409298796294,50.912386602906281</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440000</ogr:osx>
      <ogr:osy>112700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.039190439501897,50.873843121425757</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467700</ogr:osx>
      <ogr:osy>108700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.750884922459076,51.23711311912475</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487300</ogr:osx>
      <ogr:osy>149400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
