<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.660553273541593</gml:X><gml:Y>50.73746807961125</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.979259050496777</gml:X><gml:Y>51.24529254343782</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.103823570123193,51.24529254343782</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462650</ogr:osx>
      <ogr:osy>149950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.35661903392839,50.889962107787433</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445350</ogr:osx>
      <ogr:osy>110250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.079922883536985,50.790089191415809</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464950</ogr:osx>
      <ogr:osy>99350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.660553273541593,50.737468079611247</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>424050</ogr:osx>
      <ogr:osy>93150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.370799008248881,50.892737117940058</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444350</ogr:osx>
      <ogr:osy>110550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.451362455218747,50.93360939028554</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438650</ogr:osx>
      <ogr:osy>115050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.617795736402148,50.941466220633274</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>426950</ogr:osx>
      <ogr:osy>115850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.450003074955478,50.928207541812057</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438750</ogr:osx>
      <ogr:osy>114450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.029739133528994,50.884106548101201</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468350</ogr:osx>
      <ogr:osy>109850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.979259050496777,50.852201593246399</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471950</ogr:osx>
      <ogr:osy>106350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
