<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.573352890583519</gml:X><gml:Y>50.7934815965555</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8274589692967241</gml:X><gml:Y>51.2959050443281</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                               
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.338349117762402,50.975288437294367</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446550</ogr:osx>
      <ogr:osy>119750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.484037994102676,51.195431655533767</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436150</ogr:osx>
      <ogr:osy>144150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.1936794963665,50.857476606127634</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456850</ogr:osx>
      <ogr:osy>106750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.573352890583519,50.820813297838846</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>430150</ogr:osx>
      <ogr:osy>102450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.306194637946919,50.93463596733249</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448850</ogr:osx>
      <ogr:osy>115250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.054317440962461,50.793481596555502</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466750</ogr:osx>
      <ogr:osy>99750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.84709038048836,51.256534247468693</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480550</ogr:osx>
      <ogr:osy>151450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.215154363359795,50.847731047765571</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455350</ogr:osx>
      <ogr:osy>105650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.266031713644238,50.864248698840512</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451750</ogr:osx>
      <ogr:osy>107450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.827458969296724,51.295905044328101</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481850</ogr:osx>
      <ogr:osy>155850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.088680882795887,50.85040930455699</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464250</ogr:osx>
      <ogr:osy>106050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.39117168573096,50.856874960715395</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442950</ogr:osx>
      <ogr:osy>106550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
