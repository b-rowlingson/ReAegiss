<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.585433193560804</gml:X><gml:Y>50.73902392815742</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8278251864558347</gml:X><gml:Y>51.28152119374544</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.452722145729582,50.939011218901136</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438550</ogr:osx>
      <ogr:osy>115650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.304261632844165,51.064114669756655</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448850</ogr:osx>
      <ogr:osy>129650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.370665758109325,50.902628156276741</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444350</ogr:osx>
      <ogr:osy>111650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.154731817792797,50.811338391171411</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459650</ogr:osx>
      <ogr:osy>101650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.331593626046347,51.048987220936098</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446950</ogr:osx>
      <ogr:osy>127950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.361801842583095,50.926859763365172</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444950</ogr:osx>
      <ogr:osy>114350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.342250880091273,50.900673093445242</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446350</ogr:osx>
      <ogr:osy>111450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.393172714064792,50.921631455743572</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442750</ogr:osx>
      <ogr:osy>113750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.331593626046347,51.048987220936098</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446950</ogr:osx>
      <ogr:osy>127950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.939204358451,50.864434337038581</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>474750</ogr:osx>
      <ogr:osy>107750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.516959799007893,50.92041037495293</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434050</ogr:osx>
      <ogr:osy>113550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.077991475277768,50.816152993407997</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465050</ogr:osx>
      <ogr:osy>102250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.585433193560804,50.739023928157415</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>429350</ogr:osx>
      <ogr:osy>93350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.340101442856458,51.052632459411292</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446350</ogr:osx>
      <ogr:osy>128350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.454712736212931,50.890461284884879</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438450</ogr:osx>
      <ogr:osy>110250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.051480253720904,50.793458567743194</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466950</ogr:osx>
      <ogr:osy>99750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.886902166859772,51.210159853110952</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>477850</ogr:osx>
      <ogr:osy>146250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.75">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.378131642194696,50.982700808583914</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443750</ogr:osx>
      <ogr:osy>120550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.76">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.220426701401456,50.957475571361215</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>454850</ogr:osx>
      <ogr:osy>117850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.77">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.397463739455938,50.919855174787855</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442450</ogr:osx>
      <ogr:osy>113550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.78">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.827825186455835,51.281521193745441</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481850</ogr:osx>
      <ogr:osy>154250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.79">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.14644611948556,50.798688251333367</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460250</ogr:osx>
      <ogr:osy>100250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
