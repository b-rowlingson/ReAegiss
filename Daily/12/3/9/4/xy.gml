<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.422419629954968</gml:X><gml:Y>50.80739915158473</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7451567352377385</gml:X><gml:Y>51.35772937205116</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.139061957208244,51.237913682954783</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460200</ogr:osx>
      <ogr:osy>149100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.169704902367299,50.807399151584733</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458600</ogr:osx>
      <ogr:osy>101200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.091302177579961,51.26273060557844</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463500</ogr:osx>
      <ogr:osy>151900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.034100791922801,50.845024444105789</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468100</ogr:osx>
      <ogr:osy>105500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.18322136900062,50.846163094524272</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457600</ogr:osx>
      <ogr:osy>105500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.985445824344541,50.861697909395765</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471500</ogr:osx>
      <ogr:osy>107400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.169850623998499,51.357729372051161</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457900</ogr:osx>
      <ogr:osy>162400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.137050173200306,50.808061529937568</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460900</ogr:osx>
      <ogr:osy>101300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.422419629954968,50.915035376050184</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440700</ogr:osx>
      <ogr:osy>113000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.745156735237739,51.237051855024347</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487700</ogr:osx>
      <ogr:osy>149400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
