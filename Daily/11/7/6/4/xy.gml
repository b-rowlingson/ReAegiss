<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.636330896305485</gml:X><gml:Y>50.75448117795521</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7860417841689477</gml:X><gml:Y>51.33202155963784</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.147766806195896,50.804093550172979</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460150</ogr:osx>
      <ogr:osy>100850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.338264861885198,51.081397307063057</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446450</ogr:osx>
      <ogr:osy>131550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.786041784168948,51.289188508379972</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484750</ogr:osx>
      <ogr:osy>155150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.40859345556862,50.939695426176108</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441650</ogr:osx>
      <ogr:osy>115750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.314296212352271,50.964358876725022</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448250</ogr:osx>
      <ogr:osy>118550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.636330896305485,50.754481177955213</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>425750</ogr:osx>
      <ogr:osy>95050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.075436843991205,50.801744354953136</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465250</ogr:osx>
      <ogr:osy>100650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.842327602315506,51.332021559637838</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480750</ogr:osx>
      <ogr:osy>159850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.385929561764876,50.931485307669753</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443250</ogr:osx>
      <ogr:osy>114850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.75">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.110014091050613,51.295695255649264</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462150</ogr:osx>
      <ogr:osy>155550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.76">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.075554467915832,51.011273334329623</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464950</ogr:osx>
      <ogr:osy>123950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.77">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.296586321048251,50.911198022335014</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449550</ogr:osx>
      <ogr:osy>112650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
