<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.777673200454917</gml:X><gml:Y>50.83134797540396</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7973704360656464</gml:X><gml:Y>51.29470072133203</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.299465175440438,51.098256591200105</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449150</ogr:osx>
      <ogr:osy>133450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.489523779012284,51.217036489086844</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435750</ogr:osx>
      <ogr:osy>146550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.032823737681212,50.872441649220228</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468150</ogr:osx>
      <ogr:osy>108550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.428814971768669,50.915516444634363</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440250</ogr:osx>
      <ogr:osy>113050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.364046660438986,50.970934910406591</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444750</ogr:osx>
      <ogr:osy>119250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.054611291305215,50.84923908643767</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466650</ogr:osx>
      <ogr:osy>105950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.074850945252953,50.831415807129304</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465250</ogr:osx>
      <ogr:osy>103950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.777673200454917,50.84296305098038</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>415750</ogr:osx>
      <ogr:osy>104850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.993307718884358,50.859517787342369</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470950</ogr:osx>
      <ogr:osy>107150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.057396967431836,51.273702699372762</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465850</ogr:osx>
      <ogr:osy>153150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.058407665535623,51.294392568188698</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465750</ogr:osx>
      <ogr:osy>155450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.367955578579863,50.892721764704199</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444550</ogr:osx>
      <ogr:osy>110550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.797370436065646,51.294700721332028</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>483950</ogr:osx>
      <ogr:osy>155750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.06633244766805,50.831347975403958</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465850</ogr:osx>
      <ogr:osy>103950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
