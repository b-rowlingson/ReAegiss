<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.658319782899996</gml:X><gml:Y>50.75229945791101</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7547436479543984</gml:X><gml:Y>51.30639474992227</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.068614119711846,50.823722373597548</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465700</ogr:osx>
      <ogr:osy>103100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.348743691690868,50.893965092249516</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445900</ogr:osx>
      <ogr:osy>110700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.754743647954398,51.306394749922269</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486900</ogr:osx>
      <ogr:osy>157100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.658319782899996,50.752299457911015</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>424200</ogr:osx>
      <ogr:osy>94800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.20160030111732,50.934417563100311</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456200</ogr:osx>
      <ogr:osy>115300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.513246559161252,51.207695929393687</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434100</ogr:osx>
      <ogr:osy>145500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
