<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.554722613346933</gml:X><gml:Y>50.76453990322173</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7415813348377321</gml:X><gml:Y>51.26309119237457</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.858561487568117,51.111420475932633</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480000</ogr:osx>
      <ogr:osy>135300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.490793926907223,50.906360973806848</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435900</ogr:osx>
      <ogr:osy>112000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.35327031383156,50.875105928893319</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445600</ogr:osx>
      <ogr:osy>108600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.320822723660958,51.053870911337356</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447700</ogr:osx>
      <ogr:osy>128500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.741581334837732,51.263091192374574</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487900</ogr:osx>
      <ogr:osy>152300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.330119195074616,50.903751521999567</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447200</ogr:osx>
      <ogr:osy>111800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.776399695253133,51.24727413076657</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485500</ogr:osx>
      <ogr:osy>150500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.499162941461971,50.921684302660474</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435300</ogr:osx>
      <ogr:osy>113700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.554722613346933,50.764539903221731</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431500</ogr:osx>
      <ogr:osy>96200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.408184176935636,50.915863369766939</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441700</ogr:osx>
      <ogr:osy>113100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.264529581166322,51.094894850144257</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451600</ogr:osx>
      <ogr:osy>133100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
