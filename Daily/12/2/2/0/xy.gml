<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.303502852287039</gml:X><gml:Y>50.80585204826833</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7810833799349686</gml:X><gml:Y>51.34689026848172</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.08316138741141,50.805852048268335</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464700</ogr:osx>
      <ogr:osy>101100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.845542726763198,51.346890268481722</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480500</ogr:osx>
      <ogr:osy>161500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.303502852287039,50.97193859898384</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449000</ogr:osx>
      <ogr:osy>119400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.097016595075409,51.263674016430635</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463100</ogr:osx>
      <ogr:osy>152000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.781083379934969,51.286888946619754</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485100</ogr:osx>
      <ogr:osy>154900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.96614613015382,51.15558420950947</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472400</ogr:osx>
      <ogr:osy>140100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.178992204133139,50.844334895017091</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457900</ogr:osx>
      <ogr:osy>105300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.801224404741271,51.118940032647238</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484000</ogr:osx>
      <ogr:osy>136200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.302630483704687,50.935064369249567</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449100</ogr:osx>
      <ogr:osy>115300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.026700773526031,50.859351339922831</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468600</ogr:osx>
      <ogr:osy>107100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.852804115078079,51.113162631108025</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480400</ogr:osx>
      <ogr:osy>135500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.986749556506063,50.86710487137249</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471400</ogr:osx>
      <ogr:osy>108000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
