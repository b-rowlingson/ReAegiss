<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.820731856563123</gml:X><gml:Y>50.8102917730831</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8171319098386921</gml:X><gml:Y>51.279165615168</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                                
  <gml:featureMember>
    <ogr:points fid="points.24">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.820731856563123,50.910929934196979</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>412700</ogr:osx>
      <ogr:osy>112400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.25">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.817131909838692,51.279165615167997</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>482600</ogr:osx>
      <ogr:osy>154000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.26">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.194567842763973,50.847141196343273</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456800</ogr:osx>
      <ogr:osy>105600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.27">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.075977833696402,50.810291773083101</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465200</ogr:osx>
      <ogr:osy>101600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.107413443926515,51.244870360564107</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462400</ogr:osx>
      <ogr:osy>149900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.354492805498227,50.889500748942837</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445500</ogr:osx>
      <ogr:osy>110200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.987905335376451,51.14138677584863</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470900</ogr:osx>
      <ogr:osy>138500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.101280650923346,51.265505232192396</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462800</ogr:osx>
      <ogr:osy>152200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
