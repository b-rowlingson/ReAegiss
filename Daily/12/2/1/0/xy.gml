<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.779759217339336</gml:X><gml:Y>50.7259773563983</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7791730401006288</gml:X><gml:Y>51.30485349078729</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.855900901042171,51.27685314760528</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>479900</ogr:osx>
      <ogr:osy>153700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.134782062625015,51.236982924368633</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460500</ogr:osx>
      <ogr:osy>149000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.998135248552206,50.86630386334285</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470600</ogr:osx>
      <ogr:osy>107900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.603257118323679,50.726046483084545</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>428100</ogr:osx>
      <ogr:osy>91900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.197485501616618,50.842664962564534</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456600</ogr:osx>
      <ogr:osy>105100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.472982189275109,51.223707188715288</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436900</ogr:osx>
      <ogr:osy>147300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.275873018468376,50.916916578761246</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451000</ogr:osx>
      <ogr:osy>113300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.086524046420674,50.851741351512835</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464400</ogr:osx>
      <ogr:osy>106200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.048393756112687,50.840645475427316</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467100</ogr:osx>
      <ogr:osy>105000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.779759217339336,50.852409112316955</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>415600</ogr:osx>
      <ogr:osy>105900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.194335579829254,50.860628557747773</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456800</ogr:osx>
      <ogr:osy>107100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.066474009295977,50.788633447762713</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465900</ogr:osx>
      <ogr:osy>99200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.073210759449498,50.806672723022182</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465400</ogr:osx>
      <ogr:osy>101200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.082165053280123,50.78426153188903</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464800</ogr:osx>
      <ogr:osy>98700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.779173040100629,51.304853490787288</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485200</ogr:osx>
      <ogr:osy>156900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.583423127120271,50.725977356398303</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>429500</ogr:osx>
      <ogr:osy>91900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.221478390830369,50.851820079373084</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>454900</ogr:osx>
      <ogr:osy>106100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.963438678562182,50.892979112738303</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>473000</ogr:osx>
      <ogr:osy>110900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
