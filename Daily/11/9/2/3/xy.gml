<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.604173896641776</gml:X><gml:Y>50.78058999681319</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7694463446745621</gml:X><gml:Y>51.30528053343112</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.211423758715234,50.816231567342896</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455650</ogr:osx>
      <ogr:osy>102150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.077481725769892,51.272963429397052</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464450</ogr:osx>
      <ogr:osy>153050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.081006464261178,50.80718401326034</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464850</ogr:osx>
      <ogr:osy>101250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.309426557243059,50.908576891121754</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448650</ogr:osx>
      <ogr:osy>112350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.131172819709694,50.780589996813191</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461350</ogr:osx>
      <ogr:osy>98250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.777009838407806,51.305280533431123</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485350</ogr:osx>
      <ogr:osy>156950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.997415218281248,50.866747318104139</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470650</ogr:osx>
      <ogr:osy>107950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.769446344674562,51.293061809970901</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485900</ogr:osx>
      <ogr:osy>155600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.072427321772333,50.846234565898811</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465400</ogr:osx>
      <ogr:osy>105600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.964236000985629,50.857015275365406</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>473000</ogr:osx>
      <ogr:osy>106900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.310144290266585,50.908131506881965</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448600</ogr:osx>
      <ogr:osy>112300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.310144290266585,50.908131506881965</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448600</ogr:osx>
      <ogr:osy>112300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.604173896641776,50.785401141501154</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>428000</ogr:osx>
      <ogr:osy>98500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.352522769096257,50.929056818969769</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445600</ogr:osx>
      <ogr:osy>114600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.33195123048692,51.073718005690274</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446900</ogr:osx>
      <ogr:osy>130700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
