<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.332587800332093</gml:X><gml:Y>50.78885843701874</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7599878556000541</gml:X><gml:Y>51.26095848551293</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.759987855600054,51.244853368952441</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486650</ogr:osx>
      <ogr:osy>150250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.154894009312681,50.802346833621257</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459650</ogr:osx>
      <ogr:osy>100650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.839813751867452,51.26095848551293</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481050</ogr:osx>
      <ogr:osy>151950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.051934705227345,50.841123926183741</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466850</ogr:osx>
      <ogr:osy>105050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.983859949756359,51.097738810178463</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471250</ogr:osx>
      <ogr:osy>133650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.038805139418937,50.788858437018739</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467850</ogr:osx>
      <ogr:osy>99250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.332587800332093,50.979751874047835</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446950</ogr:osx>
      <ogr:osy>120250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
