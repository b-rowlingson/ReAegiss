<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.664105590244232</gml:X><gml:Y>50.73612945636578</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7965242427970689</gml:X><gml:Y>51.29963774297629</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.072581344880257,50.802620973637282</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465450</ogr:osx>
      <ogr:osy>100750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.16603721830711,50.814117540707358</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458850</ogr:osx>
      <ogr:osy>101950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.390327511624994,50.921616641412186</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442950</ogr:osx>
      <ogr:osy>113750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.01929906117522,50.907400257989202</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469050</ogr:osx>
      <ogr:osy>112450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.264766397108806,50.854348872997669</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451850</ogr:osx>
      <ogr:osy>106350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.826053018834407,51.266666277479175</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>482000</ogr:osx>
      <ogr:osy>152600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.796524242797069,51.299637742976294</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484000</ogr:osx>
      <ogr:osy>156300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.56758285053522,50.753797067782372</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>430600</ogr:osx>
      <ogr:osy>95000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.664105590244232,50.736129456365781</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423800</ogr:osx>
      <ogr:osy>93000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.489011840750994,51.198600673354704</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435800</ogr:osx>
      <ogr:osy>144500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
