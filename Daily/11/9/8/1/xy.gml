<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.549833368333353</gml:X><gml:Y>50.756877382656</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7446859332847454</gml:X><gml:Y>51.29843255479184</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                               
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.365519742816696,50.967345958463376</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444650</ogr:osx>
      <ogr:osy>118850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.300252637466246,50.856365471242682</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449350</ogr:osx>
      <ogr:osy>106550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.509653681843404,51.209029735775786</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434350</ogr:osx>
      <ogr:osy>145650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.744685933284745,51.280659470380407</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487650</ogr:osx>
      <ogr:osy>154250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.549833368333353,50.756877382656</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431850</ogr:osx>
      <ogr:osy>95350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.088538794166491,50.784761135824667</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464350</ogr:osx>
      <ogr:osy>98750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.05760786545735,51.298432554791844</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465800</ogr:osx>
      <ogr:osy>155900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.292778656890713,50.927811156890648</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449800</ogr:osx>
      <ogr:osy>114500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.349664602467407,50.929940189219622</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445800</ogr:osx>
      <ogr:osy>114700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.365890131673089,50.88776468345835</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444700</ogr:osx>
      <ogr:osy>110000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.33146399595785,50.909154731080008</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447100</ogr:osx>
      <ogr:osy>112400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
