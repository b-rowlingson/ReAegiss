<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.768408052988608</gml:X><gml:Y>50.75454132839668</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.051197340953312</gml:X><gml:Y>50.92173848796327</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.428020310933112,50.864255314900603</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440350</ogr:osx>
      <ogr:osy>107350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.181444881657697,50.825917131166683</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457750</ogr:osx>
      <ogr:osy>103250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.656177050916691,50.754541328396677</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>424350</ogr:osx>
      <ogr:osy>95050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.768408052988608,50.849689421215352</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>416400</ogr:osx>
      <ogr:osy>105600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.511966621333188,50.921738487963268</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434400</ogr:osx>
      <ogr:osy>113700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.051197340953312,50.84246684438282</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466900</ogr:osx>
      <ogr:osy>105200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.319450274448137,50.855130061506252</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448000</ogr:osx>
      <ogr:osy>106400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
