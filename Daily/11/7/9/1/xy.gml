<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.636337859255908</gml:X><gml:Y>50.75358193422248</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7712546258400902</gml:X><gml:Y>51.36843319650582</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.041660416948342,50.787982641942904</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467650</ogr:osx>
      <ogr:osy>99150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.286005605948076,50.858976994044838</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450350</ogr:osx>
      <ogr:osy>106850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.372923148357551,51.368433196505819</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443750</ogr:osx>
      <ogr:osy>163450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.063169028801825,50.847509537096208</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466050</ogr:osx>
      <ogr:osy>105750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.401047030772102,50.862321567370991</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442250</ogr:osx>
      <ogr:osy>107150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.576183909855374,50.821722864860668</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>429950</ogr:osx>
      <ogr:osy>102550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.030056404590916,50.868821612242172</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468350</ogr:osx>
      <ogr:osy>108150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.376317804220636,50.905356221544736</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443950</ogr:osx>
      <ogr:osy>111950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.084648329499288,51.273019663792901</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463950</ogr:osx>
      <ogr:osy>153050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.340625794051394,50.915051931764069</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446450</ogr:osx>
      <ogr:osy>113050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.417424372421946,50.916359440255079</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441050</ogr:osx>
      <ogr:osy>113150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.060310499260773,50.848385832443704</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466250</ogr:osx>
      <ogr:osy>105850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.792903836291147,51.300949316024422</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484250</ogr:osx>
      <ogr:osy>156450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.77125462584009,51.252166004993207</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485850</ogr:osx>
      <ogr:osy>151050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.076288439831913,50.830527918997952</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465150</ogr:osx>
      <ogr:osy>103850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.274389192739253,50.875092038066491</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451150</ogr:osx>
      <ogr:osy>108650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.385443806805996,50.968351677609434</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443250</ogr:osx>
      <ogr:osy>118950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.636337859255908,50.753581934222481</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>425750</ogr:osx>
      <ogr:osy>94950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.333462509722348,50.918608271804928</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446950</ogr:osx>
      <ogr:osy>113450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.0571184599437,50.795302823864859</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466550</ogr:osx>
      <ogr:osy>99950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.75">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.092602422329433,50.794684875600105</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464050</ogr:osx>
      <ogr:osy>99850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.76">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.488151858120403,51.211635310943549</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435850</ogr:osx>
      <ogr:osy>145950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.77">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.801451926361608,51.082522761609361</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484050</ogr:osx>
      <ogr:osy>132150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.78">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.167680129591263,50.801539454178524</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458750</ogr:osx>
      <ogr:osy>100550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.79">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.264299630866744,50.884021478282705</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451850</ogr:osx>
      <ogr:osy>109650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
