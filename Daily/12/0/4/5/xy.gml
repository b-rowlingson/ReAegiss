<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.512767103269669</gml:X><gml:Y>50.78194400650123</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9971958485297375</gml:X><gml:Y>51.28589175058738</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.362158231121928,50.900783506218417</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444950</ogr:osx>
      <ogr:osy>111450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.060245298756403,51.274624797980557</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465650</ogr:osx>
      <ogr:osy>153250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.997195848529738,50.943183082175636</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470550</ogr:osx>
      <ogr:osy>116450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.452880338060059,50.92552332128308</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438550</ogr:osx>
      <ogr:osy>114150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.071073344809905,50.807105367039128</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465550</ogr:osx>
      <ogr:osy>101250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.027008804011843,50.878688217436206</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468550</ogr:osx>
      <ogr:osy>109250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.512767103269669,50.913199019440846</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434350</ogr:osx>
      <ogr:osy>112750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.356207909674874,50.971341608452526</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445300</ogr:osx>
      <ogr:osy>119300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.401809181649711,50.858278825672315</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442200</ogr:osx>
      <ogr:osy>106700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.0636002905775,51.285891750587382</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465400</ogr:osx>
      <ogr:osy>154500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.119027668051682,51.236865440026641</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461600</ogr:osx>
      <ogr:osy>149000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.46078331070028,50.918815735085964</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438000</ogr:osx>
      <ogr:osy>113400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.131856970543654,50.781944006501227</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461300</ogr:osx>
      <ogr:osy>98400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.241043458607212,50.871732653281441</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>453500</ogr:osx>
      <ogr:osy>108300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.503381221521941,51.193267303887808</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434800</ogr:osx>
      <ogr:osy>143900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
