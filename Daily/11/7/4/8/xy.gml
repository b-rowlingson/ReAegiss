<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.770505719051923</gml:X><gml:Y>50.75810434709611</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7412699835426305</gml:X><gml:Y>51.29107532129142</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.794596861243847,51.291075321291423</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484150</ogr:osx>
      <ogr:osy>155350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.644809179374167,50.758104347096108</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>425150</ogr:osx>
      <ogr:osy>95450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.079040826639999,51.266681251505908</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464350</ogr:osx>
      <ogr:osy>152350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.390987113578054,50.980070793846437</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442850</ogr:osx>
      <ogr:osy>120250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.70674973255779,50.828418523806086</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>420750</ogr:osx>
      <ogr:osy>103250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.067157266233708,51.287268942684875</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465150</ogr:osx>
      <ogr:osy>154650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.067299498626519,50.782794724900299</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465850</ogr:osx>
      <ogr:osy>98550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.02289568847005,50.87145971178429</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468850</ogr:osx>
      <ogr:osy>108450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.741269983542631,51.248250535848065</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487950</ogr:osx>
      <ogr:osy>150650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.924588752440287,51.005485401148356</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>475550</ogr:osx>
      <ogr:osy>123450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.369035796161544,50.812693686337624</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444550</ogr:osx>
      <ogr:osy>101650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.770505719051923,50.856437919521341</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>416250</ogr:osx>
      <ogr:osy>106350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.457022990283459,50.936333559913308</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438250</ogr:osx>
      <ogr:osy>115350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.211145880843963,50.916946988808562</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455550</ogr:osx>
      <ogr:osy>113350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.75">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.392950467956459,50.938715952683523</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442750</ogr:osx>
      <ogr:osy>115650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.76">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.456834510423104,50.952519014231314</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438250</ogr:osx>
      <ogr:osy>117150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.77">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.312809972968659,51.065064444534315</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448250</ogr:osx>
      <ogr:osy>129750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
