<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.442236705188701</gml:X><gml:Y>50.80291344013518</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.077134159359848</gml:X><gml:Y>51.25452692632759</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.282488432528003,50.85670728260385</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450600</ogr:osx>
      <ogr:osy>106600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.287582813909686,51.081549171089613</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450000</ogr:osx>
      <ogr:osy>131600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.203038597970683,50.933528132940019</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456100</ogr:osx>
      <ogr:osy>115200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.077134159359848,51.254526926327593</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464500</ogr:osx>
      <ogr:osy>151000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.442236705188701,50.923224880829899</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439300</ogr:osx>
      <ogr:osy>113900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.171203484850981,50.802913440135178</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458500</ogr:osx>
      <ogr:osy>100700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
