<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.773381912538214</gml:X><gml:Y>50.74555303152334</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7455128130667623</gml:X><gml:Y>51.34447012546659</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.424614196482719,50.910100350501175</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440550</ogr:osx>
      <ogr:osy>112450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.657660176359367,50.745553031523336</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>424250</ogr:osx>
      <ogr:osy>94050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.373821982278254,50.985375509147666</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444050</ogr:osx>
      <ogr:osy>120850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.164633990835092,50.813208233533167</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458950</ogr:osx>
      <ogr:osy>101850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.066388697815241,50.899692844177459</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465750</ogr:osx>
      <ogr:osy>111550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.136299200353039,50.810304158791112</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460950</ogr:osx>
      <ogr:osy>101550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.307083198121986,50.970611021745249</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448750</ogr:osx>
      <ogr:osy>119250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.773381912538214,50.849249549519072</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>416050</ogr:osx>
      <ogr:osy>105550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.139221115820581,51.344470125466593</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460050</ogr:osx>
      <ogr:osy>160950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.773381912538214,50.849249549519072</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>416050</ogr:osx>
      <ogr:osy>105550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.135354950160223,51.24463042814979</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460450</ogr:osx>
      <ogr:osy>149850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.407193155770517,50.93788984364339</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441750</ogr:osx>
      <ogr:osy>115550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.327534647401735,51.230605405636609</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447050</ogr:osx>
      <ogr:osy>148150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.024785512064182,50.848993721269537</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468750</ogr:osx>
      <ogr:osy>105950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.352491403965246,50.98256156314617</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445550</ogr:osx>
      <ogr:osy>120550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.745512813066762,51.276621809270836</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487600</ogr:osx>
      <ogr:osy>153800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.33963667401536,51.235619886714439</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446200</ogr:osx>
      <ogr:osy>148700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.165415692808327,50.809167101826112</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458900</ogr:osx>
      <ogr:osy>101400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.346823377029372,51.233861796288032</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445700</ogr:osx>
      <ogr:osy>148500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.978499790840302,50.85444313963594</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472000</ogr:osx>
      <ogr:osy>106600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
