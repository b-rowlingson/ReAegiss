<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.514133331250828</gml:X><gml:Y>50.7946848756001</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8002338848709745</gml:X><gml:Y>51.25883328990876</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.141909874293815,50.813942639410392</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460550</ogr:osx>
      <ogr:osy>101950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.110725887445393,51.258833289908765</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462150</ogr:osx>
      <ogr:osy>151450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.37865937416456,50.943137042375426</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443750</ogr:osx>
      <ogr:osy>116150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.514133331250828,50.918600169305769</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434250</ogr:osx>
      <ogr:osy>113350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.800233884870974,51.074416993872411</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484150</ogr:osx>
      <ogr:osy>131250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.492834392117483,50.914912675157041</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435750</ogr:osx>
      <ogr:osy>112950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.075259403433969,50.81073572065565</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465250</ogr:osx>
      <ogr:osy>101650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.010316589035933,50.861461769388214</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469750</ogr:osx>
      <ogr:osy>107350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.092602422329433,50.794684875600105</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464050</ogr:osx>
      <ogr:osy>99850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
