<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.505205185874622</gml:X><gml:Y>50.78841289740235</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.000468240145758</gml:X><gml:Y>51.27751499513931</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.084559016779292,51.277514995139306</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463950</ogr:osx>
      <ogr:osy>153550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.095561154077679,50.788412897402345</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463850</ogr:osx>
      <ogr:osy>99150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.389022618249544,50.91261735435053</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443050</ogr:osx>
      <ogr:osy>112750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.192445191503201,50.846676899232435</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456950</ogr:osx>
      <ogr:osy>105550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.034727392071829,50.849076356467677</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468050</ogr:osx>
      <ogr:osy>105950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.420915998174773,50.864220300229341</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440850</ogr:osx>
      <ogr:osy>107350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.000468240145758,50.856881496915129</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470450</ogr:osx>
      <ogr:osy>106850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.350832578230039,50.89712410233804</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445750</ogr:osx>
      <ogr:osy>111050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.505205185874622,51.223398180042885</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434650</ogr:osx>
      <ogr:osy>147250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.050441687285767,50.844708881663024</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466950</ogr:osx>
      <ogr:osy>105450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.318274698666315,50.887046810821531</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448050</ogr:osx>
      <ogr:osy>109950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.302060248423292,50.925618858629377</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449150</ogr:osx>
      <ogr:osy>114250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.342113783506242,51.11199254049091</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446150</ogr:osx>
      <ogr:osy>134950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.020721101020406,50.907412191389128</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468950</ogr:osx>
      <ogr:osy>112450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
