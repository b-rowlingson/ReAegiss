<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.659794537292728</gml:X><gml:Y>50.78841289740235</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8340824135467567</gml:X><gml:Y>51.2609015544798</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.834082413546757,51.260901554479801</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481450</ogr:osx>
      <ogr:osy>151950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.951823783298447,51.251224166739618</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>473250</ogr:osx>
      <ogr:osy>150750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.095561154077679,50.788412897402345</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463850</ogr:osx>
      <ogr:osy>99150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.123621952751456,51.258930514574537</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461250</ogr:osx>
      <ogr:osy>151450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.304785065474587,50.933728339997856</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448950</ogr:osx>
      <ogr:osy>115150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.107093817832623,50.853249402524114</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462950</ogr:osx>
      <ogr:osy>106350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.659794537292728,51.232042003139341</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423850</ogr:osx>
      <ogr:osy>148150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.37349772999102,50.903542630561866</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444150</ogr:osx>
      <ogr:osy>111750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.086419544636292,50.820715647211202</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464450</ogr:osx>
      <ogr:osy>102750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.422381281522871,50.86063054051381</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440750</ogr:osx>
      <ogr:osy>106950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.090048748439696,50.853117783719462</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464150</ogr:osx>
      <ogr:osy>106350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.471426865527896,50.921112486450873</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437250</ogr:osx>
      <ogr:osy>113650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.377308156155575,50.93773438598884</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443850</ogr:osx>
      <ogr:osy>115550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
