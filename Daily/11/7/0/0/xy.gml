<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.566341420757238</gml:X><gml:Y>50.80803255707308</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8594636641469362</gml:X><gml:Y>51.10378569920432</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.859463664146936,51.103785699204316</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>479950</ogr:osx>
      <ogr:osy>134450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.544489824960898,51.014940577144294</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>432050</ogr:osx>
      <ogr:osy>124050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.75">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.566341420757238,50.964666753690985</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>430550</ogr:osx>
      <ogr:osy>118450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.76">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.189293046630215,50.864640335856151</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457150</ogr:osx>
      <ogr:osy>107550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.77">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.388729838542252,50.826287412090743</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443150</ogr:osx>
      <ogr:osy>103150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.78">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.256143217489777,50.860589077474799</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452450</ogr:osx>
      <ogr:osy>107050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.79">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.25905585106019,50.856111293696664</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452250</ogr:osx>
      <ogr:osy>106550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.80">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.006443339339224,50.90998938585625</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469950</ogr:osx>
      <ogr:osy>112750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.81">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.195949941371266,50.808032557073076</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456750</ogr:osx>
      <ogr:osy>101250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.82">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.351041890662439,50.984351999419644</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445650</ogr:osx>
      <ogr:osy>120750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
