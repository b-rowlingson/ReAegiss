<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.646294596909394</gml:X><gml:Y>50.7491162049835</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7642606565294285</gml:X><gml:Y>51.24579778030594</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.379472231094682,50.989002591434151</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443650</ogr:osx>
      <ogr:osy>121250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.338991382696403,51.230670643324231</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446250</ogr:osx>
      <ogr:osy>148150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.418925167576466,50.910072189271901</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440950</ogr:osx>
      <ogr:osy>112450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.764260656529429,51.245797780305942</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486350</ogr:osx>
      <ogr:osy>150350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.646294596909394,50.749116204983501</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>425050</ogr:osx>
      <ogr:osy>94450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.078522054638815,50.789178857579195</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465050</ogr:osx>
      <ogr:osy>99250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
