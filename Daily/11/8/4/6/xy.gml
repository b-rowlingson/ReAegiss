<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.523356885158302</gml:X><gml:Y>50.78634578228473</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9256148132969145</gml:X><gml:Y>51.07135848347585</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.413258746876151,50.908245369642579</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441350</ogr:osx>
      <ogr:osy>112250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.368343344434422,50.969159734984828</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444450</ogr:osx>
      <ogr:osy>119050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.166501101517463,50.788041921423527</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458850</ogr:osx>
      <ogr:osy>99050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.312716798625522,51.071358483475848</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448250</ogr:osx>
      <ogr:osy>130450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.370774786540369,50.894535489820314</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444350</ogr:osx>
      <ogr:osy>110750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.127554854423042,50.974803973772651</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461350</ogr:osx>
      <ogr:osy>119850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.75">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.446054806856627,50.901211523835499</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439050</ogr:osx>
      <ogr:osy>111450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.76">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.079481448562626,50.812567652251225</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464950</ogr:osx>
      <ogr:osy>101850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.77">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.061554423293854,50.786345782284734</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466250</ogr:osx>
      <ogr:osy>98950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.78">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.472793579225398,50.989011165932133</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437100</ogr:osx>
      <ogr:osy>121200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.79">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.925614813296915,50.868356802223573</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>475700</ogr:osx>
      <ogr:osy>108200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.80">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.300148703714262,50.910769796150731</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449300</ogr:osx>
      <ogr:osy>112600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.81">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.178660728355188,50.863217128172771</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457900</ogr:osx>
      <ogr:osy>107400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.82">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.523356885158302,50.920886269373561</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>433600</ogr:osx>
      <ogr:osy>113600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
