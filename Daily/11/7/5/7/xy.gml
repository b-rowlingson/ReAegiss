<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.486756008191318</gml:X><gml:Y>50.8087668394015</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7916716171290866</gml:X><gml:Y>51.29276575513175</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.976201880498559,50.862066843004037</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472150</ogr:osx>
      <ogr:osy>107450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.346138999819327,50.927672392665308</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446050</ogr:osx>
      <ogr:osy>114450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.264766397108806,50.854348872997669</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451850</ogr:osx>
      <ogr:osy>106350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.483997839624242,51.199028282300631</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436150</ogr:osx>
      <ogr:osy>144550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.791671617129087,51.074328815628959</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484750</ogr:osx>
      <ogr:osy>131250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.486756008191318,51.079445508772757</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436050</ogr:osx>
      <ogr:osy>131250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.054008935648852,50.8087668394015</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466750</ogr:osx>
      <ogr:osy>101450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.211900055957494,50.956519119335773</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455450</ogr:osx>
      <ogr:osy>117750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.085175746034524,50.811713152727343</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464550</ogr:osx>
      <ogr:osy>101750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.147537784943013,50.81668169971686</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460150</ogr:osx>
      <ogr:osy>102250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.82825601539851,51.292765755131754</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481800</ogr:osx>
      <ogr:osy>155500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.279550296094178,50.862984007608958</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450800</ogr:osx>
      <ogr:osy>107300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.955293225096454,51.256200866519123</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>473000</ogr:osx>
      <ogr:osy>151300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.385912840847114,50.986788518198004</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443200</ogr:osx>
      <ogr:osy>121000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.995206484626294,50.936421526761798</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470700</ogr:osx>
      <ogr:osy>115700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.29852378274466,50.924248801863968</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449400</ogr:osx>
      <ogr:osy>114100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.987767720781291,51.147680202307463</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470900</ogr:osx>
      <ogr:osy>139200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.995042423648046,50.877967810071567</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470800</ogr:osx>
      <ogr:osy>109200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
