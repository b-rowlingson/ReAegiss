<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.333422235543104</gml:X><gml:Y>50.80177794889113</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7707121099151887</gml:X><gml:Y>51.29936966228107</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.27">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.258181565118516,50.86644723668033</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452300</ogr:osx>
      <ogr:osy>107700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.196019017912902,50.845352668885653</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456700</ogr:osx>
      <ogr:osy>105400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.823931593906977,51.293621628579729</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>482100</ogr:osx>
      <ogr:osy>155600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.770712109915189,51.299369662281066</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485800</ogr:osx>
      <ogr:osy>156300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.982089687639666,51.145832474756318</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471300</ogr:osx>
      <ogr:osy>139000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.314344769818654,50.912652494057951</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448300</ogr:osx>
      <ogr:osy>112800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.138584956416387,50.801777948891129</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460800</ogr:osx>
      <ogr:osy>100600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.333422235543104,50.971213856386662</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446900</ogr:osx>
      <ogr:osy>119300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
