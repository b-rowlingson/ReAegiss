<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.606282793122531</gml:X><gml:Y>50.7876564260481</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7576335551772313</gml:X><gml:Y>51.29350376026846</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.433082247760334,50.915537231720258</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439950</ogr:osx>
      <ogr:osy>113050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.768717423827428,51.293503760268464</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485950</ogr:osx>
      <ogr:osy>155650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.08869934406652,51.28564012763151</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463650</ogr:osx>
      <ogr:osy>154450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.606282793122531,50.787656426048102</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>427850</ogr:osx>
      <ogr:osy>98750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.176632497604006,50.857357625437729</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458050</ogr:osx>
      <ogr:osy>106750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.092841342809971,51.257347292789156</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463400</ogr:osx>
      <ogr:osy>151300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.212986103196949,50.849964611007991</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455500</ogr:osx>
      <ogr:osy>105900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.299967261538373,51.064988268284793</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449150</ogr:osx>
      <ogr:osy>129750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.080930882441146,50.847201098399474</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464800</ogr:osx>
      <ogr:osy>105700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.757633555177231,51.252471868966509</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486800</ogr:osx>
      <ogr:osy>151100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
