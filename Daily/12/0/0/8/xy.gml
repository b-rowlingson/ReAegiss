<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.550443170225483</gml:X><gml:Y>50.76133484068072</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7695985591090836</gml:X><gml:Y>51.3141951199493</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.976201880498559,50.862066843004037</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472150</ogr:osx>
      <ogr:osy>107450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.769598559109084,51.314195119949304</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485850</ogr:osx>
      <ogr:osy>157950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.377548115490008,50.919750753251542</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443850</ogr:osx>
      <ogr:osy>113550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.381827648446521,50.918874231382389</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443550</ogr:osx>
      <ogr:osy>113450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.321319236516003,51.068711045804456</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447650</ogr:osx>
      <ogr:osy>130150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.408616257543664,50.937897055644726</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441650</ogr:osx>
      <ogr:osy>115550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.53986585646859,50.761334840680725</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>432550</ogr:osx>
      <ogr:osy>95850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.074408639442725,50.781952057315429</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465350</ogr:osx>
      <ogr:osy>98450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.08436950443621,50.816652845126292</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464600</ogr:osx>
      <ogr:osy>102300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.313224161323218,51.085299452930172</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448200</ogr:osx>
      <ogr:osy>132000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.822520741716076,51.292708232914926</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>482200</ogr:osx>
      <ogr:osy>155500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.550443170225483,50.767221293400958</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431800</ogr:osx>
      <ogr:osy>96500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.973577686705854,50.852151774617973</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472350</ogr:osx>
      <ogr:osy>106350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.306381893869989,51.065476106601409</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448700</ogr:osx>
      <ogr:osy>129800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.894404089918579,51.046119804919428</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>477600</ogr:osx>
      <ogr:osy>128000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.060890253918878,50.78409223579159</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466300</ogr:osx>
      <ogr:osy>98700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
