<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.647664801765239</gml:X><gml:Y>50.75541520785294</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7834280290278242</gml:X><gml:Y>51.30669620559281</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.463763522852168,51.216022151538773</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437550</ogr:osx>
      <ogr:osy>146450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.350544986282302,50.917805248644775</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445750</ogr:osx>
      <ogr:osy>113350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.469624441269506,51.204359254197904</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437150</ogr:osx>
      <ogr:osy>145150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.34218755454502,50.905168990751704</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446350</ogr:osx>
      <ogr:osy>111950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.076048407805624,51.272952129885127</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464550</ogr:osx>
      <ogr:osy>153050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.201972893055969,50.871022525271556</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456250</ogr:osx>
      <ogr:osy>108250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.647664801765239,50.755415207852941</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>424950</ogr:osx>
      <ogr:osy>95150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.035940341661466,50.893600215187007</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467900</ogr:osx>
      <ogr:osy>110900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.201615691539103,50.933518416516627</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456200</ogr:osx>
      <ogr:osy>115200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.783428029027824,51.306696205592814</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484900</ogr:osx>
      <ogr:osy>157100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.935101283718531,51.013225332049501</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>474800</ogr:osx>
      <ogr:osy>124300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.903993303872257,51.304289564700554</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>476500</ogr:osx>
      <ogr:osy>156700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
