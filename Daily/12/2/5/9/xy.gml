<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.796485377467604</gml:X><gml:Y>50.80906475449063</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7621001028665398</gml:X><gml:Y>51.246224569647</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                               
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.796485377467604,50.926177282858546</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>414400</ogr:osx>
      <ogr:osy>114100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.774081300454269,50.851499024598944</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>416000</ogr:osx>
      <ogr:osy>105800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.436524345136132,50.924995957097899</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439700</ogr:osx>
      <ogr:osy>114100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.76210010286654,51.246224569647005</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486500</ogr:osx>
      <ogr:osy>150400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.082862161847575,50.821137383718913</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464700</ogr:osx>
      <ogr:osy>102800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.548607332400511,50.957855342613215</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431800</ogr:osx>
      <ogr:osy>117700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.389751411569281,50.91127229243051</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443000</ogr:osx>
      <ogr:osy>112600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.340149716467956,50.898413113170442</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446500</ogr:osx>
      <ogr:osy>111200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.151224610689972,50.809064754490628</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459900</ogr:osx>
      <ogr:osy>101400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.386918650103068,50.910358210883487</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443200</ogr:osx>
      <ogr:osy>112500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
