<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.641234037308852</gml:X><gml:Y>50.76214010238376</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.947948787326116</gml:X><gml:Y>51.24576943711337</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.262132007798594,50.886255943394744</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452000</ogr:osx>
      <ogr:osy>109900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.014445302041371,50.901514169757533</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469400</ogr:osx>
      <ogr:osy>111800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.641234037308852,50.762140102383761</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>425400</ogr:osx>
      <ogr:osy>95900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.381627130365074,50.987665158023965</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443500</ogr:osx>
      <ogr:osy>121100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.008233306843523,50.859195908030571</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469900</ogr:osx>
      <ogr:osy>107100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.947948787326116,51.012442438485031</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>473900</ogr:osx>
      <ogr:osy>124200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.107396045015823,51.245769437113374</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462400</ogr:osx>
      <ogr:osy>150000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.467844723576955,50.923344333603993</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437500</ogr:osx>
      <ogr:osy>113900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
