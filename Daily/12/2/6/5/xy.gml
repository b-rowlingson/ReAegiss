<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.776948073593723</gml:X><gml:Y>50.78775697256071</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9410247748872814</gml:X><gml:Y>51.2229273190846</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.378982620199189,50.918859141209083</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443750</ogr:osx>
      <ogr:osy>113450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.941024774887281,51.097358766854299</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>474250</ogr:osx>
      <ogr:osy>133650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.47383646164985,51.211571855871014</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436850</ogr:osx>
      <ogr:osy>145950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.522722656859389,51.191099749072066</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>433450</ogr:osx>
      <ogr:osy>143650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.078292236938809,50.800867665679938</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465050</ogr:osx>
      <ogr:osy>100550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.465252110730709,50.963348697222962</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437650</ogr:osx>
      <ogr:osy>118350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.06742974109355,50.847543653858807</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465750</ogr:osx>
      <ogr:osy>105750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.069328764957118,50.787756972560715</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465700</ogr:osx>
      <ogr:osy>99100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.500198367389345,51.222927319084597</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435000</ogr:osx>
      <ogr:osy>147200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.776948073593723,50.846109041060963</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>415800</ogr:osx>
      <ogr:osy>105200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
