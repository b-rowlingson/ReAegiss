<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.48681021563237</gml:X><gml:Y>50.81800711659626</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7430209179652344</gml:X><gml:Y>51.28792781079969</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.751658015169851,51.287927810799694</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487150</ogr:osx>
      <ogr:osy>155050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.085052812095718,50.818007116596263</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464550</ogr:osx>
      <ogr:osy>102450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.337338037777146,51.046322231350985</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446550</ogr:osx>
      <ogr:osy>127650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.004500049641391,50.867707218554791</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470150</ogr:osx>
      <ogr:osy>108050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.48681021563237,51.20353664493333</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435950</ogr:osx>
      <ogr:osy>145050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.971900423049066,50.863827588326458</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472450</ogr:osx>
      <ogr:osy>107650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.437165208553684,50.930844130525927</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439650</ogr:osx>
      <ogr:osy>114750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.251002349048509,50.915410866313934</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452750</ogr:osx>
      <ogr:osy>113150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.743020917965234,51.236579323477883</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487850</ogr:osx>
      <ogr:osy>149350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.246493720056636,51.194143610377296</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452750</ogr:osx>
      <ogr:osy>144150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.42161372513584,50.922674976273051</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440750</ogr:osx>
      <ogr:osy>113850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.038988212072184,50.849111512326914</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467750</ogr:osx>
      <ogr:osy>105950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.209229999726353,50.862079283440664</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455750</ogr:osx>
      <ogr:osy>107250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.456243325935586,50.94217500305885</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438300</ogr:osx>
      <ogr:osy>116000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
