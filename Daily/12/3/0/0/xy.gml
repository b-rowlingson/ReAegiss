<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.576409752687308</gml:X><gml:Y>50.75999991367478</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7454148766786577</gml:X><gml:Y>51.36104161051812</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.24142274456929,50.848354324210369</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>453500</ogr:osx>
      <ogr:osy>105700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.26171278189647,51.361041610518122</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451500</ogr:osx>
      <ogr:osy>162700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.543423400656243,50.759999913674775</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>432300</ogr:osx>
      <ogr:osy>95700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.76767563252202,51.305632305996816</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486000</ogr:osx>
      <ogr:osy>157000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.411629504975566,50.86822052130951</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441500</ogr:osx>
      <ogr:osy>107800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.745414876678658,51.28021766893746</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487600</ogr:osx>
      <ogr:osy>154200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.576409752687308,50.87522918633632</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>429900</ogr:osx>
      <ogr:osy>108500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.359241622993751,50.957869671294212</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445100</ogr:osx>
      <ogr:osy>117800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.055162656323165,50.786743838867054</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466700</ogr:osx>
      <ogr:osy>99000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.038009960965506,50.862142893515177</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467800</ogr:osx>
      <ogr:osy>107400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
