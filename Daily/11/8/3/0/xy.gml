<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.321078559149137</gml:X><gml:Y>50.84582439317792</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8178832123645988</gml:X><gml:Y>51.27782436881186</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.077406895985493,50.845824393177921</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465050</ogr:osx>
      <ogr:osy>105550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.857969231719592,51.106468849739819</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480050</ogr:osx>
      <ogr:osy>134750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.136025889078832,50.902027330995828</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460850</ogr:osx>
      <ogr:osy>111750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.266172808686038,50.855256982481173</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451750</ogr:osx>
      <ogr:osy>106450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.133601585027068,51.26170239337884</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460550</ogr:osx>
      <ogr:osy>151750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.077406895985493,50.845824393177921</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465050</ogr:osx>
      <ogr:osy>105550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.321078559149137,50.889760906262154</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447850</ogr:osx>
      <ogr:osy>110250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.817883212364599,51.277824368811864</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>482550</ogr:osx>
      <ogr:osy>153850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
