<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.794353140419494</gml:X><gml:Y>50.8486753696756</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.763711158119932</gml:X><gml:Y>51.29300140730831</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                               
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.783174411881437,51.289158750127037</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484950</ogr:osx>
      <ogr:osy>155150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.00669203915972,50.89830104291606</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469950</ogr:osx>
      <ogr:osy>111450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.554313191411177,51.031164498973347</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431350</ogr:osx>
      <ogr:osy>125850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.794353140419494,50.925723934045273</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>414550</ogr:osx>
      <ogr:osy>114050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.469165244360345,50.994839758223861</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437350</ogr:osx>
      <ogr:osy>121850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.29255499448675,50.848675369675604</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449900</ogr:osx>
      <ogr:osy>105700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.622802578314812,50.938335158029943</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>426600</ogr:osx>
      <ogr:osy>115500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.763711158119932,51.29300140730831</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486300</ogr:osx>
      <ogr:osy>155600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.352846987555416,50.905678162533832</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445600</ogr:osx>
      <ogr:osy>112000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
