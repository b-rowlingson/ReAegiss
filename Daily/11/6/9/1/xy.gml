<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.439827449651877</gml:X><gml:Y>50.79277541650937</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7547683693585769</gml:X><gml:Y>51.28822994652419</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.200628762388947,50.866517022044853</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456350</ogr:osx>
      <ogr:osy>107750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.968140197863476,51.16234603839527</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472250</ogr:osx>
      <ogr:osy>140850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.754768369358577,51.278968667410005</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486950</ogr:osx>
      <ogr:osy>154050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.439827449651877,50.946144039699476</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439450</ogr:osx>
      <ogr:osy>116450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.332310106895532,50.899717436166391</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447050</ogr:osx>
      <ogr:osy>111350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.427271188889737,50.925400583014422</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440350</ogr:osx>
      <ogr:osy>114150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.780330857031348,51.28822994652419</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485150</ogr:osx>
      <ogr:osy>155050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.145498778573703,51.238410441973372</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459750</ogr:osx>
      <ogr:osy>149150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.078451357784235,50.792775416509372</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465050</ogr:osx>
      <ogr:osy>99650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.40038969606095,50.913575503503246</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442250</ogr:osx>
      <ogr:osy>112850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.373220119332038,50.924223845167077</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444150</ogr:osx>
      <ogr:osy>114050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.379687848029969,50.972817460904182</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443650</ogr:osx>
      <ogr:osy>119450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.407702350073456,51.00983143634523</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441650</ogr:osx>
      <ogr:osy>123550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
