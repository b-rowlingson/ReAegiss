<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.451195533053945</gml:X><gml:Y>50.78830208659237</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7392230125381034</gml:X><gml:Y>51.27070927108683</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.081376622194906,50.788302086592367</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464850</ogr:osx>
      <ogr:osy>99150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.739223012538103,51.27070927108683</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>488050</ogr:osx>
      <ogr:osy>153150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.451195533053945,50.887297438961269</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438700</ogr:osx>
      <ogr:osy>109900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.43656776944214,50.921399188673213</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439700</ogr:osx>
      <ogr:osy>113700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.034945928723148,50.872908845014372</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468000</ogr:osx>
      <ogr:osy>108600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
