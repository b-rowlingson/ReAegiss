<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.506266580332422</gml:X><gml:Y>50.79843257417185</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.784814780402235</gml:X><gml:Y>51.30850904146086</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.22835223219368,50.822639592604929</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>454450</ogr:osx>
      <ogr:osy>102850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.11619890182768,51.272362809979903</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461750</ogr:osx>
      <ogr:osy>152950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.305728906545964,51.061425707152729</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448750</ogr:osx>
      <ogr:osy>129350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.988987692869127,50.862178273992136</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471250</ogr:osx>
      <ogr:osy>107450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.388268249176349,50.970164975052008</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443050</ogr:osx>
      <ogr:osy>119150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.784814780402235,51.308509041460859</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484800</ogr:osx>
      <ogr:osy>157300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.337980770360898,51.051271629764742</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446500</ogr:osx>
      <ogr:osy>128200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.506266580332422,50.922613781288504</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434800</ogr:osx>
      <ogr:osy>113800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.054927032016003,50.798432574171848</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466700</ogr:osx>
      <ogr:osy>100300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.828164486362722,51.296361714890942</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481800</ogr:osx>
      <ogr:osy>155900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.331154802680716,50.930734927205648</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447100</ogr:osx>
      <ogr:osy>114800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
