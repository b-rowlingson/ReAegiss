<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.625673970075592</gml:X><gml:Y>50.75759491356416</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.056436284382497</gml:X><gml:Y>51.24142519712803</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.23">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.625673970075592,50.757594913564162</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>426500</ogr:osx>
      <ogr:osy>95400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.24">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.295271006075865,50.951206640926202</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449600</ogr:osx>
      <ogr:osy>117100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.25">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.203943621158184,50.880478213586606</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456100</ogr:osx>
      <ogr:osy>109300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.26">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.127535951721883,51.241425197128031</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461000</ogr:osx>
      <ogr:osy>149500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.27">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.056436284382497,50.793948389723752</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466600</ogr:osx>
      <ogr:osy>99800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.420930296458232,50.920423477595889</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440800</ogr:osx>
      <ogr:osy>113600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
