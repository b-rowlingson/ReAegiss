<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.553398844574514</gml:X><gml:Y>50.75464291464983</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8612564386154556</gml:X><gml:Y>51.29219205699973</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.24">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.553398844574514,50.754642914649828</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431600</ogr:osx>
      <ogr:osy>95100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.25">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.174009639096246,50.804731881182775</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458300</ogr:osx>
      <ogr:osy>100900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.26">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.400563841305552,50.955391238840214</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442200</ogr:osx>
      <ogr:osy>117500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.27">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.861256438615456,51.292192056999731</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>479500</ogr:osx>
      <ogr:osy>155400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
