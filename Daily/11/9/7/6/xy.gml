<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.555512432973845</gml:X><gml:Y>50.75599988475997</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7808453576529037</gml:X><gml:Y>51.2958786961106</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.354177693380745,50.963686862085908</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445450</ogr:osx>
      <ogr:osy>118450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.175924621180426,50.816885586605586</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458150</ogr:osx>
      <ogr:osy>102250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.555512432973845,50.755999884759973</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431450</ogr:osx>
      <ogr:osy>95250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.440043371837347,50.928160230401247</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439450</ogr:osx>
      <ogr:osy>114450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.3988283780643,50.924358434794875</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442350</ogr:osx>
      <ogr:osy>114050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.150588413514486,50.805013395511295</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459950</ogr:osx>
      <ogr:osy>100950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.780845357652904,51.295878696110599</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485100</ogr:osx>
      <ogr:osy>155900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.072076642934827,50.792275256824269</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465500</ogr:osx>
      <ogr:osy>99600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.111728133052228,51.244003964207273</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462100</ogr:osx>
      <ogr:osy>149800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.492670567102726,50.995393753322581</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435700</ogr:osx>
      <ogr:osy>121900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
