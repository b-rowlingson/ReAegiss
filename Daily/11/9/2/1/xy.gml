<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.574780629070003</gml:X><gml:Y>50.78807529142796</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8164498626698572</gml:X><gml:Y>51.27780989057647</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.392282427848458,50.989969137418335</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442750</ogr:osx>
      <ogr:osy>121350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.816449862669857,51.277809890576471</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>482650</ogr:osx>
      <ogr:osy>153850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.053007830262884,50.788075291427958</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466850</ogr:osx>
      <ogr:osy>99150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.267114264884721,50.885837689778313</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451650</ogr:osx>
      <ogr:osy>109850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.422914052807159,50.932573093336615</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440650</ogr:osx>
      <ogr:osy>114950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.574780629070003,50.819919249797948</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>430050</ogr:osx>
      <ogr:osy>102350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.994322182967888,50.878411243678173</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470850</ogr:osx>
      <ogr:osy>109250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
