<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.605378419094501</gml:X><gml:Y>50.7265033118352</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7360608892509044</gml:X><gml:Y>51.28146591935884</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.774239180704422,51.247701144377054</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485650</ogr:osx>
      <ogr:osy>150550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.059649077714959,50.810610978917055</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466350</ogr:osx>
      <ogr:osy>101650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.336901624178424,51.076893465645298</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446550</ogr:osx>
      <ogr:osy>131050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.315703672171918,51.062383732004243</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448050</ogr:osx>
      <ogr:osy>129450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.110812588878859,51.254337911763059</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462150</ogr:osx>
      <ogr:osy>150950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.020721101020406,50.907412191389128</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468950</ogr:osx>
      <ogr:osy>112450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.102798046095738,50.855015003179417</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463250</ogr:osx>
      <ogr:osy>106550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.736060889250904,51.281465919358837</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>488250</ogr:osx>
      <ogr:osy>154350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.37318388509501,50.926921389380787</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444150</ogr:osx>
      <ogr:osy>114350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.549755759646811,50.764970479754311</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431850</ogr:osx>
      <ogr:osy>96250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.217979935134818,50.848649273912287</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455150</ogr:osx>
      <ogr:osy>105750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.181130348129974,50.843900279954504</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457750</ogr:osx>
      <ogr:osy>105250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.202350208236324,50.932174556519819</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456150</ogr:osx>
      <ogr:osy>115050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.033984551987468,50.885041017087502</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468050</ogr:osx>
      <ogr:osy>109950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.75">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.343274875949539,51.231593947189964</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445950</ogr:osx>
      <ogr:osy>148250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.76">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.995781517124322,51.008816608995794</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470550</ogr:osx>
      <ogr:osy>123750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.77">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.167664160026504,50.802438613643453</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458750</ogr:osx>
      <ogr:osy>100650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.78">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.605378419094501,50.726503311835195</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>427950</ogr:osx>
      <ogr:osy>91950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.79">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.974603778084422,50.870146278769369</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472250</ogr:osx>
      <ogr:osy>108350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.80">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.30242935093425,51.091080522512208</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448950</ogr:osx>
      <ogr:osy>132650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
