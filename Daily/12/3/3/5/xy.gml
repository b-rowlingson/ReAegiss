<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.506050983131573</gml:X><gml:Y>50.85272870679986</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.751245920498406</gml:X><gml:Y>51.29654158220873</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.78395060865118,51.28691875514081</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484900</ogr:osx>
      <ogr:osy>154900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.373840963975612,50.930971518653799</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444100</ogr:osx>
      <ogr:osy>114800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.751245920498406,51.276683089364631</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487200</ogr:osx>
      <ogr:osy>153800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.84427633696672,51.283033536644396</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480700</ogr:osx>
      <ogr:osy>154400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.289933134918718,50.927793894944145</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450000</ogr:osx>
      <ogr:osy>114500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.39654899354614,50.935587241876007</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442500</ogr:osx>
      <ogr:osy>115300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.111554950326435,51.252994735126158</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462100</ogr:osx>
      <ogr:osy>150800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.046172324877401,51.296541582208732</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466600</ogr:osx>
      <ogr:osy>155700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.506050983131573,51.211262588615931</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434600</ogr:osx>
      <ogr:osy>145900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.222883880429202,50.852728706799859</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>454800</ogr:osx>
      <ogr:osy>106200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.386007692435167,50.979595121044241</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443200</ogr:osx>
      <ogr:osy>120200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
