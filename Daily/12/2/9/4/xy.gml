<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.42380874272045</gml:X><gml:Y>50.78410364289577</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7576820009216566</gml:X><gml:Y>51.27402940922324</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.25">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.757682000921657,51.250673922487643</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486800</ogr:osx>
      <ogr:osy>150900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.26">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.414651545351066,50.853847633433112</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441300</ogr:osx>
      <ogr:osy>106200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.27">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.843068625163493,51.274029409223239</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480800</ogr:osx>
      <ogr:osy>153400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.062308567478633,50.784103642895772</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466200</ogr:osx>
      <ogr:osy>98700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.338498585703364,50.914590279916688</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446600</ogr:osx>
      <ogr:osy>113000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.42380874272045,50.917739973788386</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440600</ogr:osx>
      <ogr:osy>113300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.078387187803547,51.263528882039573</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464400</ogr:osx>
      <ogr:osy>152000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.352023126183433,50.965023795811454</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445600</ogr:osx>
      <ogr:osy>118600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.355405672193877,50.92637502215986</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445400</ogr:osx>
      <ogr:osy>114300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
