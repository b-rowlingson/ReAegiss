<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.669634634584985</gml:X><gml:Y>50.7531256676336</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7436136298856202</gml:X><gml:Y>51.29416998517631</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.09620971739086,50.791565397819333</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463800</ogr:osx>
      <ogr:osy>99500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.789496015954793,51.29416998517631</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484500</ogr:osx>
      <ogr:osy>155700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.083654506780061,51.286949572814962</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464000</ogr:osx>
      <ogr:osy>154600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.130606193956763,50.849380170016687</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461300</ogr:osx>
      <ogr:osy>105900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.317097925025238,51.016081491444844</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448000</ogr:osx>
      <ogr:osy>124300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.989688304714254,50.862633976110793</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471200</ogr:osx>
      <ogr:osy>107500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.74361362988562,51.293686748484653</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487700</ogr:osx>
      <ogr:osy>155700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.669634634584985,50.755929091541603</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423400</ogr:osx>
      <ogr:osy>95200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.084316768150376,50.81935025617048</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464600</ogr:osx>
      <ogr:osy>102600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.315976559132251,50.995392505668306</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448100</ogr:osx>
      <ogr:osy>122000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.334859173265239,50.970322803485431</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446800</ogr:osx>
      <ogr:osy>119200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.10447870741018,51.248444787884523</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462600</ogr:osx>
      <ogr:osy>150300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.430855420527502,50.923169988198126</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440100</ogr:osx>
      <ogr:osy>113900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.089186837297446,50.787913674519579</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464300</ogr:osx>
      <ogr:osy>99100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.856933676529413,51.119497768964479</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480100</ogr:osx>
      <ogr:osy>136200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.634215032071194,50.753125667633604</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>425900</ogr:osx>
      <ogr:osy>94900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.039689121224981,50.849566916851231</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467700</ogr:osx>
      <ogr:osy>106000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.511900796841638,50.928032906923718</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434400</ogr:osx>
      <ogr:osy>114400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
