<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.466154586439937</gml:X><gml:Y>50.88085195175606</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.260795868680245</gml:X><gml:Y>51.06541643071797</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.260795868680245,50.880851951756064</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452100</ogr:osx>
      <ogr:osy>109300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.377653707022244,50.965163110927925</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443800</ogr:osx>
      <ogr:osy>118600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.296393038544317,51.065416430717974</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449400</ogr:osx>
      <ogr:osy>129800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.466154586439937,50.946716908022069</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437600</ogr:osx>
      <ogr:osy>116500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
