<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.375308674879396</gml:X><gml:Y>50.80190239084259</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7494772434005764</gml:X><gml:Y>51.34122447657628</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.326346659274576,51.066492049182251</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447300</ogr:osx>
      <ogr:osy>129900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.337254922345658,50.901993728135004</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446700</ogr:osx>
      <ogr:osy>111600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.749477243400576,51.236198856627169</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487400</ogr:osx>
      <ogr:osy>149300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.337903872732684,51.056666561457192</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446500</ogr:osx>
      <ogr:osy>128800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.375308674879396,50.821270423052972</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444100</ogr:osx>
      <ogr:osy>102600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.087155550403533,50.819372494481435</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464400</ogr:osx>
      <ogr:osy>102600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.155611560966565,50.801902390842585</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459600</ogr:osx>
      <ogr:osy>100600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.895894425529751,51.043436136443532</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>477500</ogr:osx>
      <ogr:osy>127700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.94094554769718,51.006983661060538</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>474400</ogr:osx>
      <ogr:osy>123600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.818406792404393,51.341224476576279</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>482400</ogr:osx>
      <ogr:osy>160900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.370837551298024,50.942645561809805</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444300</ogr:osx>
      <ogr:osy>116100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
