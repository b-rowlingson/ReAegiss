<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.448994258530971</gml:X><gml:Y>50.78286244010714</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8060964504563666</gml:X><gml:Y>51.15705202058899</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.448994258530971,50.89313219420189</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438850</ogr:osx>
      <ogr:osy>110550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.068038571232632,50.816973213589762</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465750</ogr:osx>
      <ogr:osy>102350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.287508451915659,50.853590614717227</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450250</ogr:osx>
      <ogr:osy>106250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.075809173695825,50.782862440107138</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465250</ogr:osx>
      <ogr:osy>98550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.015506890764324,50.884886681234846</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469350</ogr:osx>
      <ogr:osy>109950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.226106352878796,50.958412577007991</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>454450</ogr:osx>
      <ogr:osy>117950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.806096450456367,51.123935722229731</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>483650</ogr:osx>
      <ogr:osy>136750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.979697935594112,51.157052020588992</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471450</ogr:osx>
      <ogr:osy>140250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
