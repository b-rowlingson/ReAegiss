<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.663938383599715</gml:X><gml:Y>50.75950988750272</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7671012775838638</gml:X><gml:Y>51.24672696086463</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.04853356352677,50.798830257478883</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467150</ogr:osx>
      <ogr:osy>100350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.042840191367682,50.799682975305622</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467550</ogr:osx>
      <ogr:osy>100450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.498104277018374,51.217972779203144</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435150</ogr:osx>
      <ogr:osy>146650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.090677846026576,50.820748902680094</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464150</ogr:osx>
      <ogr:osy>102750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.032563187675315,50.885029248920702</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468150</ogr:osx>
      <ogr:osy>109950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.472456267852053,51.207069648233038</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436950</ogr:osx>
      <ogr:osy>145450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.33435640435493,51.055297579612258</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446750</ogr:osx>
      <ogr:osy>128650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.767101277583864,51.246726960864628</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486150</ogr:osx>
      <ogr:osy>150450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.967435311520498,51.161890205027731</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472300</ogr:osx>
      <ogr:osy>140800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.371080047500994,50.924661966493908</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444300</ogr:osx>
      <ogr:osy>114100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.943071109146592,50.850530621414975</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>474500</ogr:osx>
      <ogr:osy>106200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.074683031821713,50.803986574296829</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465300</ogr:osx>
      <ogr:osy>100900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.070693726752665,50.790465691802005</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465600</ogr:osx>
      <ogr:osy>99400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.663938383599715,50.759509887502723</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423800</ogr:osx>
      <ogr:osy>95600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
