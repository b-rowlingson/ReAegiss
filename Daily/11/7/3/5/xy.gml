<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.619283512119639</gml:X><gml:Y>50.75691524062508</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7642606565294285</gml:X><gml:Y>51.24579778030594</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.764260656529429,51.245797780305942</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486350</ogr:osx>
      <ogr:osy>150350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.619283512119639,50.75892314050796</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>426950</ogr:osx>
      <ogr:osy>95550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.559756839449765,50.756915240625084</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431150</ogr:osx>
      <ogr:osy>95350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.324474296514473,51.048047077032173</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447450</ogr:osx>
      <ogr:osy>127850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.394257432080767,50.838006602936993</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442750</ogr:osx>
      <ogr:osy>104450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.360773193019284,50.898078182259177</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445050</ogr:osx>
      <ogr:osy>111150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.808930488066392,51.124863862557419</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>483450</ogr:osx>
      <ogr:osy>136850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.971419634599196,51.143491068800024</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472050</ogr:osx>
      <ogr:osy>138750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.028373890571006,50.881397390938943</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468450</ogr:osx>
      <ogr:osy>109550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.507068121297816,50.914074356087632</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434750</ogr:osx>
      <ogr:osy>112850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
