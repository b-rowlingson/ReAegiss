<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.583106889826133</gml:X><gml:Y>50.87869479028775</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9631616618809997</gml:X><gml:Y>51.35856709584027</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.161218237898663,51.358567095840272</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458500</ogr:osx>
      <ogr:osy>162500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.583106889826133,50.921114741293401</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>429400</ogr:osx>
      <ogr:osy>113600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.42949871136305,50.917767900050698</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440200</ogr:osx>
      <ogr:osy>113300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.351799265834127,50.878694790287753</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445700</ogr:osx>
      <ogr:osy>109000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.963161661881,51.288642487298468</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472400</ogr:osx>
      <ogr:osy>154900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.500422084280266,51.202246717656244</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435000</ogr:osx>
      <ogr:osy>144900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
