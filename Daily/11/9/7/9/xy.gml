<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.573929573366032</gml:X><gml:Y>50.76316653629195</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7493310067417487</gml:X><gml:Y>51.35141089960822</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.396320269660217,50.898267320571421</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442550</ogr:osx>
      <ogr:osy>111150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.573929573366032,50.913438083088224</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>430050</ogr:osx>
      <ogr:osy>112750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.994485334909831,50.936864951844022</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470750</ogr:osx>
      <ogr:osy>115750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.503390752914413,50.992292476947405</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434950</ogr:osx>
      <ogr:osy>121550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.077920669781345,50.819749535062428</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465050</ogr:osx>
      <ogr:osy>102650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.384643199654077,51.351410899608219</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442950</ogr:osx>
      <ogr:osy>161550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.066386242884779,50.828650583619357</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465850</ogr:osx>
      <ogr:osy>103650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.548355181954994,50.763166536291955</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431950</ogr:osx>
      <ogr:osy>96050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.35355181589101,50.906131690555618</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445550</ogr:osx>
      <ogr:osy>112050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.313551154203413,50.966602607893982</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448300</ogr:osx>
      <ogr:osy>118800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.039652205490002,50.851365157834515</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467700</ogr:osx>
      <ogr:osy>106200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.040942009336268,50.788426379948412</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467700</ogr:osx>
      <ogr:osy>99200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.992335861009841,50.871649583087802</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471000</ogr:osx>
      <ogr:osy>108500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.060656080334125,50.795780995678264</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466300</ogr:osx>
      <ogr:osy>100000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.749331006741749,51.24159269090115</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487400</ogr:osx>
      <ogr:osy>149900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.30306893057573,51.096030062707683</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448900</ogr:osx>
      <ogr:osy>133200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.838587582540981,51.281178816330403</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481100</ogr:osx>
      <ogr:osy>154200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.76059528086374,51.248906339806766</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486600</ogr:osx>
      <ogr:osy>150700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
