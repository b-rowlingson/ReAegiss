<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.669748438173287</gml:X><gml:Y>50.73974262994459</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8024556646829702</gml:X><gml:Y>51.33674404152507</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.377250669632436,50.888725065809929</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443900</ogr:osx>
      <ogr:osy>110100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.266890127191883,50.85481185841028</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451700</ogr:osx>
      <ogr:osy>106400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.282557388095155,50.852211403985976</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450600</ogr:osx>
      <ogr:osy>106100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.058189158289779,50.847919100508634</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466400</ogr:osx>
      <ogr:osy>105800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.070497614320711,50.800356199269785</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465600</ogr:osx>
      <ogr:osy>100500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.471539985026686,51.224599886106986</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437000</ogr:osx>
      <ogr:osy>147400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.819957525878401,51.336744041525066</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>482300</ogr:osx>
      <ogr:osy>160400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.488822852100248,51.215684634465049</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435800</ogr:osx>
      <ogr:osy>146400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.119739253942039,51.274637484862517</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461500</ogr:osx>
      <ogr:osy>153200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.307286617176439,50.90901384486169</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448800</ogr:osx>
      <ogr:osy>112400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.80245566468297,51.071292384979536</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484000</ogr:osx>
      <ogr:osy>130900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.488952180124878,51.203995613920902</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435800</ogr:osx>
      <ogr:osy>145100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.669748438173287,50.739742629944587</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423400</ogr:osx>
      <ogr:osy>93400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.377461481095594,50.979549909316091</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443800</ogr:osx>
      <ogr:osy>120200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
