<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.425275517590586</gml:X><gml:Y>50.81292144505625</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.067409477870367</gml:X><gml:Y>51.26437133652944</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.067409477870367,50.812921445056247</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465800</ogr:osx>
      <ogr:osy>101900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.425275517590586,50.914150215472944</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440500</ogr:osx>
      <ogr:osy>112900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.071203956181234,51.264371336529436</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464900</ogr:osx>
      <ogr:osy>152100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.30128830831837,50.929660862428292</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449200</ogr:osx>
      <ogr:osy>114700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
