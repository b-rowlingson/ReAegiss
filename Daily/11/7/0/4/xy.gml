<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.58253506558628</gml:X><gml:Y>50.74620774249983</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9891626151145502</gml:X><gml:Y>51.09914318735554</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.36474137077923,50.815368183932058</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444850</ogr:osx>
      <ogr:osy>101950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.58253506558628,50.746207742499834</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>429550</ogr:osx>
      <ogr:osy>94150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.160921259683627,51.099143187355544</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458850</ogr:osx>
      <ogr:osy>133650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.366943733336474,50.967353681450923</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444550</ogr:osx>
      <ogr:osy>118850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.243313269922522,50.863204414062217</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>453350</ogr:osx>
      <ogr:osy>107350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.292714538358464,50.885096292430219</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449850</ogr:osx>
      <ogr:osy>109750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.468561179386403,50.922897973115525</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437450</ogr:osx>
      <ogr:osy>113850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.397742160618159,50.898274660996911</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442450</ogr:osx>
      <ogr:osy>111150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.98916261511455,50.854086323310682</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471250</ogr:osx>
      <ogr:osy>106550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.363249136401056,50.925069168048978</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444850</ogr:osx>
      <ogr:osy>114150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.388728237643837,50.935096962595573</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443050</ogr:osx>
      <ogr:osy>115250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.451911591509678,50.886851176645052</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438650</ogr:osx>
      <ogr:osy>109850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
