<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.601128229165738</gml:X><gml:Y>50.72648886020099</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7612822869696794</gml:X><gml:Y>51.35716169656119</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.153344992024849,51.357161696561185</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459050</ogr:osx>
      <ogr:osy>162350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.026952639471904,50.881385555065506</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468550</ogr:osx>
      <ogr:osy>109550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.761282286969679,51.303316808098629</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486450</ogr:osx>
      <ogr:osy>156750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.161875956959256,50.808692098264025</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459150</ogr:osx>
      <ogr:osy>101350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.774334755841074,51.244105225411957</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485650</ogr:osx>
      <ogr:osy>150150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.601128229165738,50.726488860200995</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>428250</ogr:osx>
      <ogr:osy>91950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.401015652144575,50.97562643623256</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442150</ogr:osx>
      <ogr:osy>119750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.396754540737114,50.974705281008319</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442450</ogr:osx>
      <ogr:osy>119650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.970380731098262,50.868310563929271</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472550</ogr:osx>
      <ogr:osy>108150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.49678030076221,51.208075917426783</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435250</ogr:osx>
      <ogr:osy>145550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.33342398392261,50.921305797486433</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446950</ogr:osx>
      <ogr:osy>113750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
