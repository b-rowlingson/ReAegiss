<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.364642205840428</gml:X><gml:Y>50.8538768689376</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9718846403932202</gml:X><gml:Y>51.15473554721915</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.364642205840428,50.979480919530175</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444700</ogr:osx>
      <ogr:osy>120200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.9783033747521,50.863434164960502</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472000</ogr:osx>
      <ogr:osy>107600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.97188464039322,51.154735547219154</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472000</ogr:osx>
      <ogr:osy>140000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.261222207797993,50.853876868937604</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452100</ogr:osx>
      <ogr:osy>106300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
