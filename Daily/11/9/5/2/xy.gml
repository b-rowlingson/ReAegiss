<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.653321968718079</gml:X><gml:Y>50.75723067753515</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8287061128535058</gml:X><gml:Y>51.35634505332363</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.331441501050332,50.861044517981611</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447150</ogr:osx>
      <ogr:osy>107050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.411757249654957,50.9145325474055</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441450</ogr:osx>
      <ogr:osy>112950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.355327861236383,50.983476482129284</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445350</ogr:osx>
      <ogr:osy>120650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.33700168099679,50.969885358557811</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446650</ogr:osx>
      <ogr:osy>119150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.321022505635963,50.991375358370362</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447750</ogr:osx>
      <ogr:osy>121550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.317157158688317,50.96347638811794</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448050</ogr:osx>
      <ogr:osy>118450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.340562279478139,50.919547816457431</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446450</ogr:osx>
      <ogr:osy>113550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.216950810876842,50.825261463213145</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455250</ogr:osx>
      <ogr:osy>103150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.164849153836912,51.356345053323629</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458250</ogr:osx>
      <ogr:osy>162250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.494276294008484,50.913120437614161</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435650</ogr:osx>
      <ogr:osy>112750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.653321968718079,50.757230677535148</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>424550</ogr:osx>
      <ogr:osy>95350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.260824311685195,50.879053616886445</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452100</ogr:osx>
      <ogr:osy>109100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.828706112853506,51.331436556280472</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481700</ogr:osx>
      <ogr:osy>159800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.075693965040591,50.824677932273865</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465200</ogr:osx>
      <ogr:osy>103200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.507622835544588,50.928914198718566</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434700</ogr:osx>
      <ogr:osy>114500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.334872010589146,50.96942363573536</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446800</ogr:osx>
      <ogr:osy>119100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.300108262994953,50.91346730675955</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449300</ogr:osx>
      <ogr:osy>112900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
