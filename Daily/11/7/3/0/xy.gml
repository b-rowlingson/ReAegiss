<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.428802124803425</gml:X><gml:Y>50.78895136537516</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7593613298286612</gml:X><gml:Y>51.29475385560326</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.273944492952321,51.086411038166695</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450950</ogr:osx>
      <ogr:osy>132150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.050152755355428,50.788951365375162</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467050</ogr:osx>
      <ogr:osy>99250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.402536746979022,50.856933667291074</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442150</ogr:osx>
      <ogr:osy>106550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.014862179090992,50.915456293931186</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469350</ogr:osx>
      <ogr:osy>113350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.335462182632409,50.977969775452983</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446750</ogr:osx>
      <ogr:osy>120050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.302824437815654,50.969686448656432</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449050</ogr:osx>
      <ogr:osy>119150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.428802124803425,50.858413989411069</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440300</ogr:osx>
      <ogr:osy>106700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.360406169932862,50.976760097238056</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445000</ogr:osx>
      <ogr:osy>119900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.076208305841038,50.79860299160034</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465200</ogr:osx>
      <ogr:osy>100300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.759361329828661,51.294753855603261</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486600</ogr:osx>
      <ogr:osy>155800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.043766194654487,50.858593102093181</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467400</ogr:osx>
      <ogr:osy>107000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
