<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.781963333060381</gml:X><gml:Y>50.74288799362825</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9279212826324678</gml:X><gml:Y>51.25620103149239</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.939612136113469,50.846452498138227</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>474750</ogr:osx>
      <ogr:osy>105750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.119374776923924,51.256201031492388</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461550</ogr:osx>
      <ogr:osy>151150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.781963333060381,50.836676386417835</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>415450</ogr:osx>
      <ogr:osy>104150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.927921282632468,50.860734155830173</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>475550</ogr:osx>
      <ogr:osy>107350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.558220741880943,50.769499155754353</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431250</ogr:osx>
      <ogr:osy>96750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.307196008910737,51.05873672423558</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448650</ogr:osx>
      <ogr:osy>129050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.329907255903249,50.968046409595289</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447150</ogr:osx>
      <ogr:osy>118950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.639131508611799,50.75898619559382</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>425550</ogr:osx>
      <ogr:osy>95550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.052662601374558,50.805158804877273</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466850</ogr:osx>
      <ogr:osy>101050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.555469866959442,50.760496055383854</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431450</ogr:osx>
      <ogr:osy>95750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.472456267852053,51.207069648233038</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436950</ogr:osx>
      <ogr:osy>145450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.566767555851079,50.918807196980538</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>430550</ogr:osx>
      <ogr:osy>113350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.357855269992998,50.903457679201104</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445250</ogr:osx>
      <ogr:osy>111750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.669017700425693,50.742887993628251</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423450</ogr:osx>
      <ogr:osy>93750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
