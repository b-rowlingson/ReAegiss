<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.211399006832989</gml:X><gml:Y>50.78614852557465</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.741949388920782</gml:X><gml:Y>51.27289298024835</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.856452440443341,51.110051006364394</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480150</ogr:osx>
      <ogr:osy>135150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.093476930538814,50.786148525574646</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464000</ogr:osx>
      <ogr:osy>98900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.176737017845667,50.811046057563203</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458100</ogr:osx>
      <ogr:osy>101600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.082918792322891,51.251874746782043</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464100</ogr:osx>
      <ogr:osy>150700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.126940272150565,51.272892980248351</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461000</ogr:osx>
      <ogr:osy>153000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.741949388920782,51.249606686561407</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487900</ogr:osx>
      <ogr:osy>150800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.181659588120702,50.854245545352882</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457700</ogr:osx>
      <ogr:osy>106400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.211399006832989,50.85984579793417</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455600</ogr:osx>
      <ogr:osy>107000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
