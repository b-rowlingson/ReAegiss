<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.577587298447005</gml:X><gml:Y>50.78555986040175</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9873138799497752</gml:X><gml:Y>51.35365805691521</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.075756008231342,50.785559860401747</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465250</ogr:osx>
      <ogr:osy>98850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.577587298447005,50.823526462747452</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>429850</ogr:osx>
      <ogr:osy>102750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.50483466680494,50.990500138423783</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434850</ogr:osx>
      <ogr:osy>121350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.987313879949775,50.873854309326823</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471350</ogr:osx>
      <ogr:osy>108750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.135519163925758,50.852564091814884</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460950</ogr:osx>
      <ogr:osy>106250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.317790876114858,51.017434389080044</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447950</ogr:osx>
      <ogr:osy>124450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.297996352649741,51.10094545224262</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449250</ogr:osx>
      <ogr:osy>133750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.297316903600502,50.862642653385876</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449550</ogr:osx>
      <ogr:osy>107250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.498280085320232,51.20178795839049</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435150</ogr:osx>
      <ogr:osy>144850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.485318829427995,51.208925295445006</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436050</ogr:osx>
      <ogr:osy>145650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.50060869081444,50.985985997604473</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435150</ogr:osx>
      <ogr:osy>120850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.199315675940487,50.860213186304783</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456450</ogr:osx>
      <ogr:osy>107050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.166334049476853,51.353658056915208</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458150</ogr:osx>
      <ogr:osy>161950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.039522143294557,50.892280833082317</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467650</ogr:osx>
      <ogr:osy>110750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
