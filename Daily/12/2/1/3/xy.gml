<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.457357976547959</gml:X><gml:Y>50.84326137858728</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.038398199146692</gml:X><gml:Y>51.21284544360582</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.457357976547959,51.212845443605822</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438000</ogr:osx>
      <ogr:osy>146100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.038398199146692,50.843261378587279</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467800</ogr:osx>
      <ogr:osy>105300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
