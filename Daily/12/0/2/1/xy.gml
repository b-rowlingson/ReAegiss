<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.408912470405506</gml:X><gml:Y>50.82322960015061</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7907645610644816</gml:X><gml:Y>51.30047763794028</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.408912470405506,50.914518187327616</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441650</ogr:osx>
      <ogr:osy>112950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.058998743824048,50.842979633153767</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466350</ogr:osx>
      <ogr:osy>105250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.325706688035056,50.864608570439614</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447550</ogr:osx>
      <ogr:osy>107450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.402301442579696,50.986423859805321</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442050</ogr:osx>
      <ogr:osy>120950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.182911578315493,50.823229600150611</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457650</ogr:osx>
      <ogr:osy>102950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.05557659470161,51.292571365673759</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465950</ogr:osx>
      <ogr:osy>155250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.318080877240777,50.997652895757902</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447950</ogr:osx>
      <ogr:osy>122250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.375029268002607,51.001568214681903</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443950</ogr:osx>
      <ogr:osy>122650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.117155618407103,50.847031050000297</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462250</ogr:osx>
      <ogr:osy>105650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.790764561064482,51.300477637940276</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484400</ogr:osx>
      <ogr:osy>156400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.025745156332204,50.905206051588202</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468600</ogr:osx>
      <ogr:osy>112200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
