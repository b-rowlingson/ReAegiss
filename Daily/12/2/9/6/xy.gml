<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.376029401947747</gml:X><gml:Y>50.7906111025584</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9965133147386537</gml:X><gml:Y>50.94182832941772</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.350757586942752,50.902519191206643</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445750</ogr:osx>
      <ogr:osy>111650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.08098882776716,50.808083151042162</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464850</ogr:osx>
      <ogr:osy>101350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.376029401947747,50.926936622550635</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443950</ogr:osx>
      <ogr:osy>114350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.356259687783421,50.916038385364232</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445350</ogr:osx>
      <ogr:osy>113150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.192352064433922,50.852071845267709</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456950</ogr:osx>
      <ogr:osy>106150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.362568493790381,50.922817338972905</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444900</ogr:osx>
      <ogr:osy>113900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.089134432142679,50.790611102558401</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464300</ogr:osx>
      <ogr:osy>99400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.996513314738654,50.941828329417717</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470600</ogr:osx>
      <ogr:osy>116300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.036329851090518,50.874718814165035</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467900</ogr:osx>
      <ogr:osy>108800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.356977091805605,50.91559271261228</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445300</ogr:osx>
      <ogr:osy>113100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
