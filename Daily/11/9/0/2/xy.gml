<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.732578610005908</gml:X><gml:Y>50.75275115367397</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.028317799432938</gml:X><gml:Y>50.95921452615654</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.028317799432938,50.884094728399759</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468450</ogr:osx>
      <ogr:osy>109850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.190838321898864,50.857456948901174</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457050</ogr:osx>
      <ogr:osy>106750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.408536440391994,50.944191350033186</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441650</ogr:osx>
      <ogr:osy>116250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.358511210238025,50.959214526156536</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445150</ogr:osx>
      <ogr:osy>117950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.08720791602042,50.853095605261068</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464350</ogr:osx>
      <ogr:osy>106350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.151209068495213,50.84908215697935</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459850</ogr:osx>
      <ogr:osy>105850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.502867946717221,50.907761847693564</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435050</ogr:osx>
      <ogr:osy>112150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.36348214750447,50.907984727081207</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444850</ogr:osx>
      <ogr:osy>112250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.66750500873985,50.756372676809661</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423550</ogr:osx>
      <ogr:osy>95250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.659025277779734,50.752751153673969</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>424150</ogr:osx>
      <ogr:osy>94850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.732578610005908,50.780819950694465</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>418950</ogr:osx>
      <ogr:osy>97950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
