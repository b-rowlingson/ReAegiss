<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.549885087613281</gml:X><gml:Y>50.75148197826153</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7769620533371749</gml:X><gml:Y>51.30707847609332</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.776962053337175,51.307078476093324</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485350</ogr:osx>
      <ogr:osy>157150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.092602422329433,50.794684875600105</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464050</ogr:osx>
      <ogr:osy>99850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.427227041481129,50.928997343166984</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440350</ogr:osx>
      <ogr:osy>114550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.367565979817202,50.92149563120379</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444550</ogr:osx>
      <ogr:osy>113750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.342088258462681,51.113790834313853</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446150</ogr:osx>
      <ogr:osy>135150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.401431518217107,50.943255925203353</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442150</ogr:osx>
      <ogr:osy>116150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.549885087613281,50.751481978261531</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431850</ogr:osx>
      <ogr:osy>94750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.842272932364042,51.277168817630937</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480850</ogr:osx>
      <ogr:osy>153750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.26010608077289,51.148370580185286</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451850</ogr:osx>
      <ogr:osy>139050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.179023754660799,50.842536583808297</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457900</ogr:osx>
      <ogr:osy>105100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.951097263882366,51.251667269004891</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>473300</ogr:osx>
      <ogr:osy>150800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.06316989808098,50.81198821170441</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466100</ogr:osx>
      <ogr:osy>101800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.996513314738654,50.941828329417717</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470600</ogr:osx>
      <ogr:osy>116300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.272390424071448,50.866536645766658</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451300</ogr:osx>
      <ogr:osy>107700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.125626354683723,51.266588745986155</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461100</ogr:osx>
      <ogr:osy>152300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.092788267372525,51.260044505435822</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463400</ogr:osx>
      <ogr:osy>151600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.069346612349905,50.786857834581646</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465700</ogr:osx>
      <ogr:osy>99000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.400979721435434,50.923020617574956</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442200</ogr:osx>
      <ogr:osy>113900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
