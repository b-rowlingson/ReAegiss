<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.654792512175745</gml:X><gml:Y>50.75004091495803</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7448387272991498</gml:X><gml:Y>51.32170331655875</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.654792512175745,50.750040914958035</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>424450</ogr:osx>
      <ogr:osy>94550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.100379831535402,51.274939976331595</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462850</ogr:osx>
      <ogr:osy>153250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.298648951721476,51.057786542920439</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449250</ogr:osx>
      <ogr:osy>128950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.826483335490115,51.277910869815848</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481950</ogr:osx>
      <ogr:osy>153850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.946004518955738,51.003882007593845</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>474050</ogr:osx>
      <ogr:osy>123250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.467853445898089,50.984942227788309</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437450</ogr:osx>
      <ogr:osy>120750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.764029798003737,51.307841943696076</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486250</ogr:osx>
      <ogr:osy>157250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.337662505300987,51.223469522468477</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446350</ogr:osx>
      <ogr:osy>147350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.452216466063006,50.921473612726793</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438600</ogr:osx>
      <ogr:osy>113700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.30173576960739,51.089727548276805</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449000</ogr:osx>
      <ogr:osy>132500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.123358475596026,51.235099527769421</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461300</ogr:osx>
      <ogr:osy>148800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.125059442394878,50.842144579052153</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461700</ogr:osx>
      <ogr:osy>105100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.146239870341816,51.237067035813595</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459700</ogr:osx>
      <ogr:osy>149000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.044215574283577,51.321703316558754</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466700</ogr:osx>
      <ogr:osy>158500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.74483872729915,51.248738468240965</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487700</ogr:osx>
      <ogr:osy>150700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
