<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.483730793288142</gml:X><gml:Y>50.77324010246173</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7497882434273129</gml:X><gml:Y>51.27756676225977</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.783894649793172,51.125955100233497</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485200</ogr:osx>
      <ogr:osy>137000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.304361846355512,50.91439196741279</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449000</ogr:osx>
      <ogr:osy>113000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.078426879923884,50.830095186527551</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465000</ogr:osx>
      <ogr:osy>103800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.401175842440858,50.907734427217896</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442200</ogr:osx>
      <ogr:osy>112200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.749788243427313,51.277566762259767</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487300</ogr:osx>
      <ogr:osy>153900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.300722113690797,50.967425780875757</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449200</ogr:osx>
      <ogr:osy>118900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.064642915758786,50.809302196104106</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466000</ogr:osx>
      <ogr:osy>101500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.483730793288142,50.773240102461735</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436500</ogr:osx>
      <ogr:osy>97200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.850705760227032,51.25522095751721</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480300</ogr:osx>
      <ogr:osy>151300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
