<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.467863720818202</gml:X><gml:Y>50.82897010577215</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8363124592642374</gml:X><gml:Y>51.2861019399185</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.359786721211655,50.970012410129378</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445050</ogr:osx>
      <ogr:osy>119150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.836312459264237,51.286101939918495</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481250</ogr:osx>
      <ogr:osy>154750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.385855059538483,50.82897010577215</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443350</ogr:osx>
      <ogr:osy>103450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.311408581750437,50.967039622658845</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448450</ogr:osx>
      <ogr:osy>118850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.467863720818202,50.984043040480479</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437450</ogr:osx>
      <ogr:osy>120650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
