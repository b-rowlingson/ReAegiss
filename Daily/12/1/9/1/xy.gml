<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.797877055397359</gml:X><gml:Y>50.81103603793017</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7662173407264469</gml:X><gml:Y>51.30651617508126</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.20">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.450666845966502,50.932257257972076</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438700</ogr:osx>
      <ogr:osy>114900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.21">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.302457267235319,50.851432734687023</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449200</ogr:osx>
      <ogr:osy>106000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.22">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.175317844925415,50.811036037930172</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458200</ogr:osx>
      <ogr:osy>101600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.23">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.301546216133888,51.102315536307721</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449000</ogr:osx>
      <ogr:osy>133900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.24">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.36681572496902,50.819426138394533</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444700</ogr:osx>
      <ogr:osy>102400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.25">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.408115767690276,50.921258499739317</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441700</ogr:osx>
      <ogr:osy>113700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.26">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.361145864379133,50.9228095675021</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445000</ogr:osx>
      <ogr:osy>113900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.27">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.766217340726447,51.306516175081263</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486100</ogr:osx>
      <ogr:osy>157100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.096024297236397,51.241186142629623</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463200</ogr:osx>
      <ogr:osy>149500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.797877055397359,50.933373582542671</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>414300</ogr:osx>
      <ogr:osy>114900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.36184276635853,50.975868712015981</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444900</ogr:osx>
      <ogr:osy>119800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.785289366438321,51.290529540348786</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484800</ogr:osx>
      <ogr:osy>155300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.122118038039016,50.847518108985163</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461900</ogr:osx>
      <ogr:osy>105700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
