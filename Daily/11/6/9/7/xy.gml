<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.588976147523159</gml:X><gml:Y>50.81022004104194</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7689174155910456</gml:X><gml:Y>51.33643659277134</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.318182849939957,50.893341050031054</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448050</ogr:osx>
      <ogr:osy>110650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.337224066864642,51.154228081370462</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446450</ogr:osx>
      <ogr:osy>139650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.321608264877201,51.048929712736602</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447650</ogr:osx>
      <ogr:osy>127950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.203654250060277,50.855746543784782</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456150</ogr:osx>
      <ogr:osy>106550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.368952095503929,50.92420086760837</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444450</ogr:osx>
      <ogr:osy>114050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.588976147523159,50.819970085752132</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>429050</ogr:osx>
      <ogr:osy>102350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.946611678769306,50.851012265239817</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>474250</ogr:osx>
      <ogr:osy>106250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.768917415591046,51.312839132488882</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485900</ogr:osx>
      <ogr:osy>157800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.504503703694345,51.222046419301535</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434700</ogr:osx>
      <ogr:osy>147100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.78223168801625,51.297691562999418</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485000</ogr:osx>
      <ogr:osy>156100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.440738612401398,50.929512425774071</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439400</ogr:osx>
      <ogr:osy>114600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.841951715126515,51.261429262069377</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480900</ogr:osx>
      <ogr:osy>152000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.32861867967908,50.810220041041937</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447400</ogr:osx>
      <ogr:osy>101400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.789818843239545,51.336436592771342</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484400</ogr:osx>
      <ogr:osy>160400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
