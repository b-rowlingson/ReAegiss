<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.678226654932599</gml:X><gml:Y>50.73097535806131</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.069346612349905</gml:X><gml:Y>51.25492208104144</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.368757465074825,50.938587743650338</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444450</ogr:osx>
      <ogr:osy>115650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.504077680450697,51.061535834713482</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434850</ogr:osx>
      <ogr:osy>129250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.136332351770517,50.808505856741782</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460950</ogr:osx>
      <ogr:osy>101350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.424414648193054,50.926285788571747</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440550</ogr:osx>
      <ogr:osy>114250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.420246932885112,50.918171974265718</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440850</ogr:osx>
      <ogr:osy>113350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.329466273847465,50.899701143921085</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447250</ogr:osx>
      <ogr:osy>111350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.071108996292908,50.805307096051344</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465550</ogr:osx>
      <ogr:osy>101050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.392054600367236,50.898245195398623</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442850</ogr:osx>
      <ogr:osy>111150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.076430341438328,50.823334850064818</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465150</ogr:osx>
      <ogr:osy>103050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.086524771982446,50.815320821935352</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464450</ogr:osx>
      <ogr:osy>102150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.75">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.564034808925317,50.754233500302732</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>430850</ogr:osx>
      <ogr:osy>95050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.76">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.59825634491748,50.730975358061308</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>428450</ogr:osx>
      <ogr:osy>92450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.77">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.269562692748258,50.865619730962422</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451500</ogr:osx>
      <ogr:osy>107600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.78">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.678226654932599,50.74336337873649</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>422800</ogr:osx>
      <ogr:osy>93800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.79">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.069346612349905,50.786857834581646</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465700</ogr:osx>
      <ogr:osy>99000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.80">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.128713569248397,51.254922081041435</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460900</ogr:osx>
      <ogr:osy>151000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
