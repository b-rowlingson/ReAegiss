<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.754882897349156</gml:X><gml:Y>50.75510604092068</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7543550374904442</gml:X><gml:Y>51.32930396483674</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.556938529715736,50.75510604092068</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431350</ogr:osx>
      <ogr:osy>95150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.312716798625522,51.071358483475848</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448250</ogr:osx>
      <ogr:osy>130450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.286087951425734,50.853581937248798</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450350</ogr:osx>
      <ogr:osy>106250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.02519372824269,50.897557693576466</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468650</ogr:osx>
      <ogr:osy>111350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.162609006315482,51.084767609831275</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458750</ogr:osx>
      <ogr:osy>132050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.096962293845477,50.789323028315465</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463750</ogr:osx>
      <ogr:osy>99250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.754355037490444,51.294251102471947</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486950</ogr:osx>
      <ogr:osy>155750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.137773517210226,50.961391082683924</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460650</ogr:osx>
      <ogr:osy>118350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.350782586602055,50.900720828817001</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445750</ogr:osx>
      <ogr:osy>111450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.474241690336962,50.923822918201616</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437050</ogr:osx>
      <ogr:osy>113950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.318080877240777,50.997652895757902</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447950</ogr:osx>
      <ogr:osy>122250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.035906763349504,50.860776637262525</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467950</ogr:osx>
      <ogr:osy>107250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.295150489281619,50.91208861773746</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449650</ogr:osx>
      <ogr:osy>112750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.754882897349156,50.855506943097978</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>417350</ogr:osx>
      <ogr:osy>106250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.79010661797129,51.298222790832497</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484450</ogr:osx>
      <ogr:osy>156150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.209260407141976,50.860280964375768</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455750</ogr:osx>
      <ogr:osy>107050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.341991591680733,51.170890391808804</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446100</ogr:osx>
      <ogr:osy>141500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.75">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.137747319586037,51.231609544136312</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460300</ogr:osx>
      <ogr:osy>148400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.76">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.795747905820774,51.32930396483674</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484000</ogr:osx>
      <ogr:osy>159600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.77">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.179354840156023,50.823654281577227</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457900</ogr:osx>
      <ogr:osy>103000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.78">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.17544444241849,50.803842754968436</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458200</ogr:osx>
      <ogr:osy>100800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.79">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.078639214456149,50.819305572377857</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465000</ogr:osx>
      <ogr:osy>102600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
