<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.672588983972997</gml:X><gml:Y>50.7388513661688</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.038654049247216</gml:X><gml:Y>51.29485936849023</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                               
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.038654049247216,50.899917446216989</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467700</ogr:osx>
      <ogr:osy>111600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.34466764505308,50.880453495421627</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446200</ogr:osx>
      <ogr:osy>109200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.342753044810011,50.915513544976662</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446300</ogr:osx>
      <ogr:osy>113100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.142792254465153,50.804506752894653</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460500</ogr:osx>
      <ogr:osy>100900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.060549460977421,51.294859368490229</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465600</ogr:osx>
      <ogr:osy>155500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.672588983972997,50.738851366168795</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423200</ogr:osx>
      <ogr:osy>93300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.470434956299339,50.94583714534825</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437300</ogr:osx>
      <ogr:osy>116400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.401594928697515,50.985970626634668</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442100</ogr:osx>
      <ogr:osy>120900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.450730360029062,50.926862098302458</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438700</ogr:osx>
      <ogr:osy>114300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
