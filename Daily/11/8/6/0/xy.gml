<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.471045460197033</gml:X><gml:Y>50.81040832327707</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7570263490331333</gml:X><gml:Y>51.28030127771763</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.027682105465464,50.846320064508333</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468550</ogr:osx>
      <ogr:osy>105650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.345351532465583,51.08503408136108</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445950</ogr:osx>
      <ogr:osy>131950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.471045460197033,51.205264883039135</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437050</ogr:osx>
      <ogr:osy>145250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.757026349033133,51.248418886633047</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486850</ogr:osx>
      <ogr:osy>150650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.095973822085859,51.280301277717633</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463150</ogr:osx>
      <ogr:osy>153850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.15049060342173,50.810408323277066</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459950</ogr:osx>
      <ogr:osy>101550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.8326040432133,51.262685270732682</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481550</ogr:osx>
      <ogr:osy>152150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.363224223480818,50.822554020372898</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444950</ogr:osx>
      <ogr:osy>102750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.204937229003985,50.863848654246716</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456050</ogr:osx>
      <ogr:osy>107450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.006978794596786,50.884814463004666</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469950</ogr:osx>
      <ogr:osy>109950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
