<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.378777899233665</gml:X><gml:Y>50.86850666251487</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.046405640978895</gml:X><gml:Y>51.26447292235194</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.046405640978895,50.868506662514875</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467200</ogr:osx>
      <ogr:osy>108100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.084101431567181,51.264472922351942</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464000</ogr:osx>
      <ogr:osy>152100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.378777899233665,50.987650048381305</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443700</ogr:osx>
      <ogr:osy>121100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
