<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.450740943805517</gml:X><gml:Y>50.80398657429683</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.054831952104945</gml:X><gml:Y>51.29391416632147</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.074683031821713,50.803986574296829</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465300</ogr:osx>
      <ogr:osy>100900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.16972083557966,50.806499992424335</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458600</ogr:osx>
      <ogr:osy>101100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.386493195843771,50.942728818658878</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443200</ogr:osx>
      <ogr:osy>116100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.151862175272744,50.852234337308865</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459800</ogr:osx>
      <ogr:osy>106200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.054831952104945,51.293914166321471</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466000</ogr:osx>
      <ogr:osy>155400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.365694810130506,50.902151641383526</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444700</ogr:osx>
      <ogr:osy>111600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.450740943805517,50.925962904530728</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438700</ogr:osx>
      <ogr:osy>114200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
