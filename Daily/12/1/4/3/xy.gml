<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.773833634222511</gml:X><gml:Y>50.74379524617551</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7678294662225564</gml:X><gml:Y>51.2930771071938</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.50282965311242,50.911358663125341</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435050</ogr:osx>
      <ogr:osy>112550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.314128901073748,50.878929241807704</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448350</ogr:osx>
      <ogr:osy>109050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.079552116122958,50.808971104563526</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464950</ogr:osx>
      <ogr:osy>101450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.082175617275574,51.253217737899924</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464150</ogr:osx>
      <ogr:osy>150850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.814874939363808,51.115931790436619</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>483050</ogr:osx>
      <ogr:osy>135850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.671845890723847,50.743795246175509</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423250</ogr:osx>
      <ogr:osy>93850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.370972895540434,50.985360209645457</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444250</ogr:osx>
      <ogr:osy>120850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.300714599772651,51.062744670312163</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449100</ogr:osx>
      <ogr:osy>129500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.447884809562688,50.926848685406839</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438900</ogr:osx>
      <ogr:osy>114300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.120545566345857,51.232380801114701</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461500</ogr:osx>
      <ogr:osy>148500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.377034852687356,50.904910423486193</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443900</ogr:osx>
      <ogr:osy>111900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.773833634222511,50.902755462509056</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>416000</ogr:osx>
      <ogr:osy>111500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.425319792066322,50.910553447341094</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440500</ogr:osx>
      <ogr:osy>112500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.767829466222556,51.24628501224214</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486100</ogr:osx>
      <ogr:osy>150400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.859800356037568,51.293077107193795</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>479600</ogr:osx>
      <ogr:osy>155500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
