<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.504503703694345</gml:X><gml:Y>50.81323863142685</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.168891698023313</gml:X><gml:Y>51.22204641930153</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.168891698023313,50.813238631426849</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458650</ogr:osx>
      <ogr:osy>101850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.443722024968056,50.97763598632185</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439150</ogr:osx>
      <ogr:osy>119950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.504503703694345,51.222046419301535</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434700</ogr:osx>
      <ogr:osy>147100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.44829931193228,50.891780040488264</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438900</ogr:osx>
      <ogr:osy>110400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
