<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.394257573857794</gml:X><gml:Y>50.79999010751025</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7671012775838638</gml:X><gml:Y>51.28466048701782</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.320102828189877,51.054316341880664</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447750</ogr:osx>
      <ogr:osy>128550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.767101277583864,51.246726960864628</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486150</ogr:osx>
      <ogr:osy>150450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.041773512703182,50.851832226304928</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467550</ogr:osx>
      <ogr:osy>106250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.962717907091474,50.893422347054802</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>473050</ogr:osx>
      <ogr:osy>110950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.296423694481902,50.921988048082568</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449550</ogr:osx>
      <ogr:osy>113850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.172320636802894,50.820007531310452</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458400</ogr:osx>
      <ogr:osy>102600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.143175039225212,51.247835140226194</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459900</ogr:osx>
      <ogr:osy>150200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.140036834698646,50.799990107510247</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460700</ogr:osx>
      <ogr:osy>100400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.001206215486535,50.92208478350134</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470300</ogr:osx>
      <ogr:osy>114100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.394257573857794,51.111824551369331</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442500</ogr:osx>
      <ogr:osy>134900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.827028318265644,51.284660487017824</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481900</ogr:osx>
      <ogr:osy>154600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
