<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.461115430723479</gml:X><gml:Y>50.79997964311521</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9782051289634501</gml:X><gml:Y>51.28949944202877</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.3091020476734,51.07448447475187</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448500</ogr:osx>
      <ogr:osy>130800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.064960992990424,51.28949944202877</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465300</ogr:osx>
      <ogr:osy>154900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.97820512896345,50.867929672188083</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472000</ogr:osx>
      <ogr:osy>108100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.116975859394127,51.269221453867594</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461700</ogr:osx>
      <ogr:osy>152600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.02984035032925,50.844989111852044</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468400</ogr:osx>
      <ogr:osy>105500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.461115430723479,50.890041376575475</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438000</ogr:osx>
      <ogr:osy>110200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.138618010050341,50.799979643115208</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460800</ogr:osx>
      <ogr:osy>100400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.134697751140983,51.241478344752174</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460500</ogr:osx>
      <ogr:osy>149500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
