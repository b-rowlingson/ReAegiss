<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.367759432915829</gml:X><gml:Y>50.85996121316813</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9925877486865241</gml:X><gml:Y>51.25570282651081</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.351861606970305,50.874198863854055</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445700</ogr:osx>
      <ogr:osy>108500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.112935773319303,51.255702826510806</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462000</ogr:osx>
      <ogr:osy>151100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.349626998307031,50.93263771840477</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445800</ogr:osx>
      <ogr:osy>115000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.367759432915829,50.959714552571349</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444500</ogr:osx>
      <ogr:osy>118000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.360216197392846,50.886834485713109</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445100</ogr:osx>
      <ogr:osy>109900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.320678057519424,51.063761576417853</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447700</ogr:osx>
      <ogr:osy>129600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.008195168615903,50.860994129192648</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469900</ogr:osx>
      <ogr:osy>107300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.999402078193011,50.873508900869794</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470500</ogr:osx>
      <ogr:osy>108700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.992587748686524,50.859961213168127</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471000</ogr:osx>
      <ogr:osy>107200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.286606313051228,50.959247260417627</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450200</ogr:osx>
      <ogr:osy>118000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.327604374913335,51.078189218339638</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447200</ogr:osx>
      <ogr:osy>131200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
