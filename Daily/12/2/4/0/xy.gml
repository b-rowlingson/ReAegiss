<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.408706365745759</gml:X><gml:Y>50.80040126725844</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.763604747446419</gml:X><gml:Y>51.28832803437177</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.052599215224897,50.843377503830972</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466800</ogr:osx>
      <ogr:osy>105300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.408706365745759,50.986906056362173</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441600</ogr:osx>
      <ogr:osy>121000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.834105133005898,51.288328034371766</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481400</ogr:osx>
      <ogr:osy>155000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.314344769818654,50.912652494057951</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448300</ogr:osx>
      <ogr:osy>112800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.965517261666368,50.863321561594297</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472900</ogr:osx>
      <ogr:osy>107600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.291328580373027,50.929600866455871</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449900</ogr:osx>
      <ogr:osy>114700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.844907538553323,51.257861567658217</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480700</ogr:osx>
      <ogr:osy>151600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.07617285856883,50.800401267258444</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465200</ogr:osx>
      <ogr:osy>100500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.763604747446419,51.243542777737979</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486400</ogr:osx>
      <ogr:osy>150100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
