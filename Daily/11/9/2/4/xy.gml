<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.665407374452041</gml:X><gml:Y>50.75231999093152</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8232261608144076</gml:X><gml:Y>51.29316493284465</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.42848465504966,50.942492143972991</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440250</ogr:osx>
      <ogr:osy>116050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.006385923525814,50.912686692295374</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469950</ogr:osx>
      <ogr:osy>113050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.823226160814408,51.293164932844654</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>482150</ogr:osx>
      <ogr:osy>155550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.348218050928562,50.982537808494996</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445850</ogr:osx>
      <ogr:osy>120550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.153968404260449,50.853598519779844</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459650</ogr:osx>
      <ogr:osy>106350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.660410301370148,50.757251515704006</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>424050</ogr:osx>
      <ogr:osy>95350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.978546108614025,51.144452741489289</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471550</ogr:osx>
      <ogr:osy>138850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.372348956119483,50.988964554945952</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444150</ogr:osx>
      <ogr:osy>121250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.074759508916614,51.265748295364382</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464650</ogr:osx>
      <ogr:osy>152250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.318636614055927,51.057005492203118</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447850</ogr:osx>
      <ogr:osy>128850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.446398027345065,50.932237108775126</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439000</ogr:osx>
      <ogr:osy>114900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.51334237689036,50.926240437087735</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434300</ogr:osx>
      <ogr:osy>114200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.665407374452041,50.752319990931518</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423700</ogr:osx>
      <ogr:osy>94800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.342196771536327,51.055791562543163</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446200</ogr:osx>
      <ogr:osy>128700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
