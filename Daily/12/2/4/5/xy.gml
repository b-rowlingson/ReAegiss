<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.543423400656243</gml:X><gml:Y>50.75999991367478</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7824456838345215</gml:X><gml:Y>51.33826415108859</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.27">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.376578584344849,50.939079361405625</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443900</ogr:osx>
      <ogr:osy>115700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.074434367164224,50.816574474701937</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465300</ogr:osx>
      <ogr:osy>102300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.792641945963857,51.338264151088595</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484200</ogr:osx>
      <ogr:osy>160600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.333422235543104,50.971213856386662</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446900</ogr:osx>
      <ogr:osy>119300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.543423400656243,50.759999913674775</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>432300</ogr:osx>
      <ogr:osy>95700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.078479975789036,50.827397784933353</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465000</ogr:osx>
      <ogr:osy>103500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.342603636923529,51.22844297777246</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446000</ogr:osx>
      <ogr:osy>147900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.782445683834522,51.289600787473262</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485000</ogr:osx>
      <ogr:osy>155200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.783594368760844,51.300403388158003</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484900</ogr:osx>
      <ogr:osy>156400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.423052836920197,50.863781250443793</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440700</ogr:osx>
      <ogr:osy>107300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.50183395106975,51.20405113247832</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434900</ogr:osx>
      <ogr:osy>145100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.012757284249296,50.914089623268318</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469500</ogr:osx>
      <ogr:osy>113200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
