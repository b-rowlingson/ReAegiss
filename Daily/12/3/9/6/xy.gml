<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.743243591864034</gml:X><gml:Y>50.75556905989891</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.831830634999287</gml:X><gml:Y>51.29212761628329</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.945531332382385,51.244423529230716</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>473700</ogr:osx>
      <ogr:osy>150000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.461705089274351,51.208369512070341</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437700</ogr:osx>
      <ogr:osy>145600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.831830634999287,51.264925615067256</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481600</ogr:osx>
      <ogr:osy>152400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.449222675310252,50.934048943912131</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438800</ogr:osx>
      <ogr:osy>115100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.579833726414994,50.81049534310354</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>429700</ogr:osx>
      <ogr:osy>101300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.415059688388931,50.934782141676465</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441200</ogr:osx>
      <ogr:osy>115200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.930391457579515,50.845918727775775</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>475400</ogr:osx>
      <ogr:osy>105700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.079514957108624,51.278824267654706</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464300</ogr:osx>
      <ogr:osy>153700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.560478285362069,50.75556905989891</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431100</ogr:osx>
      <ogr:osy>95200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.040353001648201,50.886442432504616</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467600</ogr:osx>
      <ogr:osy>110100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.063583228767289,50.791308124453998</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466100</ogr:osx>
      <ogr:osy>99500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.997074057868892,50.915754521992376</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470600</ogr:osx>
      <ogr:osy>113400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.743243591864034,50.775897959046461</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>418200</ogr:osx>
      <ogr:osy>97400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.05630276788183,51.292127616283288</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465900</ogr:osx>
      <ogr:osy>155200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.556182991121033,50.760049136459337</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431400</ogr:osx>
      <ogr:osy>95700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.306435620148077,51.061879514922943</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448700</ogr:osx>
      <ogr:osy>129400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
