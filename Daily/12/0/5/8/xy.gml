<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.547624604534816</gml:X><gml:Y>50.76541187117079</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7411392937143275</gml:X><gml:Y>51.27927255556691</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.94555188603313,51.243524495669142</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>473700</ogr:osx>
      <ogr:osy>149900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.404146412458323,50.897857883199194</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442000</ogr:osx>
      <ogr:osy>111100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.055330511948613,50.848795273173053</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466600</ogr:osx>
      <ogr:osy>105900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.940648248121003,51.207511358313489</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>474100</ogr:osx>
      <ogr:osy>145900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.547624604534816,50.765411871170791</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>432000</ogr:osx>
      <ogr:osy>96300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.201423655893729,50.861576518312795</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456300</ogr:osx>
      <ogr:osy>107200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.137050173200306,50.808061529937568</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460900</ogr:osx>
      <ogr:osy>101300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.741139293714328,51.279272555566912</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487900</ogr:osx>
      <ogr:osy>154100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
