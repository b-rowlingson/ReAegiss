<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.423686614463227</gml:X><gml:Y>50.80040126725844</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7620839766010568</gml:X><gml:Y>51.35289954347674</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.423686614463227,50.927631065839122</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440600</ogr:osx>
      <ogr:osy>114400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.123984856146437,51.352899543476738</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461100</ogr:osx>
      <ogr:osy>161900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.762083976601057,51.300178005327219</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486400</ogr:osx>
      <ogr:osy>156400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.07617285856883,50.800401267258444</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465200</ogr:osx>
      <ogr:osy>100500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.947725005237769,51.022332200653295</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>473900</ogr:osx>
      <ogr:osy>125300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.068506796651277,50.829117163874592</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465700</ogr:osx>
      <ogr:osy>103700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.097069429933064,51.260976802320307</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463100</ogr:osx>
      <ogr:osy>151700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
