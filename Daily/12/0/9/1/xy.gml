<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.572647203651128</gml:X><gml:Y>50.78504823693903</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.741924860842711</gml:X><gml:Y>51.35005037500093</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.067963959055128,50.785048236939026</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465800</ogr:osx>
      <ogr:osy>98800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.741924860842711,51.250505654647597</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487900</ogr:osx>
      <ogr:osy>150900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.819975664927468,51.280093527533218</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>482400</ogr:osx>
      <ogr:osy>154100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.361625446224802,50.887741449927454</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445000</ogr:osx>
      <ogr:osy>110000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.861345369440443,51.288596052746286</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>479500</ogr:osx>
      <ogr:osy>155000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.401947127666363,50.847488455790156</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442200</ogr:osx>
      <ogr:osy>105500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.572647203651128,50.820361088741194</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>430200</ogr:osx>
      <ogr:osy>102400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.18592104868139,50.854275339070753</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457400</ogr:osx>
      <ogr:osy>106400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.926110434456624,50.846778698579222</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>475700</ogr:osx>
      <ogr:osy>105800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.348596905890532,51.006369689344126</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445800</ogr:osx>
      <ogr:osy>123200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.385841676578263,50.992183560127877</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443200</ogr:osx>
      <ogr:osy>121600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.103935930657144,51.350050375000926</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462500</ogr:osx>
      <ogr:osy>161600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
