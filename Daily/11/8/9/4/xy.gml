<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.668922650751128</gml:X><gml:Y>50.75637670646207</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8483209416587921</gml:X><gml:Y>51.2646393611154</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.335461407679248,51.077784515252205</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446650</ogr:osx>
      <ogr:osy>131150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.848320941658792,51.2646393611154</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480450</ogr:osx>
      <ogr:osy>152350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.044283531857449,50.868039675202141</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467350</ogr:osx>
      <ogr:osy>108050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.003790985698487,50.900974129937467</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470150</ogr:osx>
      <ogr:osy>111750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.668922650751128,50.756376706462071</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423450</ogr:osx>
      <ogr:osy>95250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.406609572228869,50.983748104015866</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441750</ogr:osx>
      <ogr:osy>120650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.170215368900656,50.818643676874146</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458550</ogr:osx>
      <ogr:osy>102450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.322039344992536,51.068265609668714</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447600</ogr:osx>
      <ogr:osy>130100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.182923003485508,50.863247035135721</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457600</ogr:osx>
      <ogr:osy>107400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.084422232622988,50.813955432787026</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464600</ogr:osx>
      <ogr:osy>102000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
