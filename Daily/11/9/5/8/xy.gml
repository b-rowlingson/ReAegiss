<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.351182232728261</gml:X><gml:Y>50.80207493321949</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.752073764976963</gml:X><gml:Y>51.32883233203146</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.79360728613373,51.32883233203146</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484150</ogr:osx>
      <ogr:osy>159550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.334987490454737,50.911422969939693</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446850</ogr:osx>
      <ogr:osy>112650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.899427839312355,51.044818334829451</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>477250</ogr:osx>
      <ogr:osy>127850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.836357891243565,51.284303951943983</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481250</ogr:osx>
      <ogr:osy>154550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.779968753556391,51.247760994066326</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485250</ogr:osx>
      <ogr:osy>150550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.351182232728261,50.871946953526496</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445750</ogr:osx>
      <ogr:osy>108250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.8499782154951,51.255663425147489</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480350</ogr:osx>
      <ogr:osy>151350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.003588409521318,50.877141786698779</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470200</ogr:osx>
      <ogr:osy>109100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.762204853457646,51.295683167328185</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486400</ogr:osx>
      <ogr:osy>155900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.752073764976963,51.246118120490145</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487200</ogr:osx>
      <ogr:osy>150400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.752195384373518,51.241623257765454</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487200</ogr:osx>
      <ogr:osy>149900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.073690566164967,51.283274373398939</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464700</ogr:osx>
      <ogr:osy>154200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.060529922322004,50.802074933219494</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466300</ogr:osx>
      <ogr:osy>100700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
