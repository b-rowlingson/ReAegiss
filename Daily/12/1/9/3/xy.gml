<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.566190223307537</gml:X><gml:Y>50.75109409761285</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.009539612851788</gml:X><gml:Y>50.98163787759777</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.434414789699865,50.98163787759777</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439800</ogr:osx>
      <ogr:osy>120400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.229687834303995,50.957087374324139</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>454200</ogr:osx>
      <ogr:osy>117800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.091378840994438,50.821203999307862</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464100</ogr:osx>
      <ogr:osy>102800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.356927540956377,50.919189431991221</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445300</ogr:osx>
      <ogr:osy>113500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.566190223307537,50.75109409761285</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>430700</ogr:osx>
      <ogr:osy>94700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.009539612851788,50.864602632111847</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469800</ogr:osx>
      <ogr:osy>107700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.559026906424625,50.759160650954314</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431200</ogr:osx>
      <ogr:osy>95600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
