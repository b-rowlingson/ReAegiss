<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.373797797318771</gml:X><gml:Y>50.8008001143793</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7360608892509044</gml:X><gml:Y>51.35262471316594</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.736060889250904,51.281465919358837</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>488250</ogr:osx>
      <ogr:osy>154350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.038766502776222,50.859900948200568</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467750</ogr:osx>
      <ogr:osy>107150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.069779286184791,50.8008001143793</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465650</ogr:osx>
      <ogr:osy>100550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.98593188974692,50.872043769953308</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471450</ogr:osx>
      <ogr:osy>108550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.287997940307092,50.914742989914892</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450150</ogr:osx>
      <ogr:osy>113050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.980463808928082,50.862104142267405</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471850</ogr:osx>
      <ogr:osy>107450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.367419715345914,50.932285793782093</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444550</ogr:osx>
      <ogr:osy>114950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.147684501282641,51.352624713165937</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459450</ogr:osx>
      <ogr:osy>161850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.268521663965698,50.886745770205437</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451550</ogr:osx>
      <ogr:osy>109950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.373797797318771,50.987173853350058</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444050</ogr:osx>
      <ogr:osy>121050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.30141387751731,50.968778782157656</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449150</ogr:osx>
      <ogr:osy>119050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.047774717031219,51.323081100593186</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466450</ogr:osx>
      <ogr:osy>158650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.207794034660165,50.862968811129527</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455850</ogr:osx>
      <ogr:osy>107350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.833172939134201,51.240210318139276</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481550</ogr:osx>
      <ogr:osy>149650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
