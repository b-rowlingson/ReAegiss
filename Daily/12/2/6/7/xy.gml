<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.795783801280695</gml:X><gml:Y>50.74698032877794</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.856695660623232</gml:X><gml:Y>51.35458769563105</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.856695660623232,51.273713640545843</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>479850</ogr:osx>
      <ogr:osy>153350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.012666107751823,51.086295469985529</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469250</ogr:osx>
      <ogr:osy>132350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.795783801280695,50.923927968467467</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>414450</ogr:osx>
      <ogr:osy>113850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.013252729341622,50.856990261866081</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469550</ogr:osx>
      <ogr:osy>106850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.61642605247634,50.761611579103459</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>427150</ogr:osx>
      <ogr:osy>95850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.170625431352509,51.354587695631054</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457850</ogr:osx>
      <ogr:osy>162050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.212328903091304,50.846812753559611</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455550</ogr:osx>
      <ogr:osy>105550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.548510836624123,50.746980328777944</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431950</ogr:osx>
      <ogr:osy>94250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
