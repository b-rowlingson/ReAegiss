<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.212328903091304</gml:X><gml:Y>50.79170628830981</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7408280673027671</gml:X><gml:Y>51.35366826253181</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.966038704447136,50.871869282539606</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472850</ogr:osx>
      <ogr:osy>108550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.167769918958954,51.353668262531805</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458050</ogr:osx>
      <ogr:osy>161950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.118474212482262,50.85243662556347</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462150</ogr:osx>
      <ogr:osy>106250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.057190800188603,50.791706288309811</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466550</ogr:osx>
      <ogr:osy>99550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.740828067302767,51.264431938396747</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487950</ogr:osx>
      <ogr:osy>152450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.212328903091304,50.846812753559611</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455550</ogr:osx>
      <ogr:osy>105550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
