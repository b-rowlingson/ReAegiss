<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.083038208421784</gml:X><gml:Y>50.81214601496132</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7297220824070899</gml:X><gml:Y>51.2813273711041</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.25">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.72972208240709,51.277350801583481</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>488700</ogr:osx>
      <ogr:osy>153900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.26">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.055090241268965,51.281327371104105</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466000</ogr:osx>
      <ogr:osy>154000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.27">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.083038208421784,50.812146014961321</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464700</ogr:osx>
      <ogr:osy>101800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
