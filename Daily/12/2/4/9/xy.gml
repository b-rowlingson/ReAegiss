<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.452121463773904</gml:X><gml:Y>50.83983911592254</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9838102409198421</gml:X><gml:Y>51.30895880594804</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.088697840407406,51.031609544811339</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464000</ogr:osx>
      <ogr:osy>126200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.983810240919842,50.871575685989747</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471600</ogr:osx>
      <ogr:osy>108500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.436140722933367,51.308958805948045</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439400</ogr:osx>
      <ogr:osy>156800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.179071074392542,50.839839115922537</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457900</ogr:osx>
      <ogr:osy>104800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.39654899354614,50.935587241876007</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442500</ogr:osx>
      <ogr:osy>115300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.166218225130122,51.082545321476914</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458500</ogr:osx>
      <ogr:osy>131800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.452121463773904,50.929566359857112</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438600</ogr:osx>
      <ogr:osy>114600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.124346941347359,51.258486335004868</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461200</ogr:osx>
      <ogr:osy>151400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
