<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.666055395947044</gml:X><gml:Y>50.76086485924362</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7454937860486149</gml:X><gml:Y>51.33912804095685</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.78">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.745493786048615,51.250993568142782</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487650</ogr:osx>
      <ogr:osy>150950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.79">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.445745308057457,50.927288176691491</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439050</ogr:osx>
      <ogr:osy>114350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.80">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.477998971489616,51.223280061604846</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436550</ogr:osx>
      <ogr:osy>147250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.81">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.082214114286632,50.817984827557119</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464750</ogr:osx>
      <ogr:osy>102450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.82">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.351182232728261,50.871946953526496</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445750</ogr:osx>
      <ogr:osy>108250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.83">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.833534832692111,51.339128040956851</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481350</ogr:osx>
      <ogr:osy>160650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.84">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.373147645281027,50.929618932321361</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444150</ogr:osx>
      <ogr:osy>114650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.85">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.666055395947044,50.760864859243618</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423650</ogr:osx>
      <ogr:osy>95750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.86">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.202126343195993,50.862030953908061</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456250</ogr:osx>
      <ogr:osy>107250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.87">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.745493786048615,51.250993568142782</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487650</ogr:osx>
      <ogr:osy>150950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.88">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.090257367999878,51.279357808383608</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463550</ogr:osx>
      <ogr:osy>153750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.89">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.113902688511783,51.242671627569614</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461950</ogr:osx>
      <ogr:osy>149650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.90">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.012702186764611,50.883064461520114</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469550</ogr:osx>
      <ogr:osy>109750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
