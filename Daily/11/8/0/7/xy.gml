<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.67327565911446</gml:X><gml:Y>50.74200072445255</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7431789410750335</gml:X><gml:Y>51.35627299124641</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.860471673037105,51.120881157022026</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>479850</ogr:osx>
      <ogr:osy>136350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.743178941075033,51.283340986295414</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487750</ogr:osx>
      <ogr:osy>154550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.828533872092794,51.253652376664853</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481850</ogr:osx>
      <ogr:osy>151150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.828602394428473,51.250955386905197</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481850</ogr:osx>
      <ogr:osy>150850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.377915407977686,50.998885906777993</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443750</ogr:osx>
      <ogr:osy>122350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.15479750700702,51.356272991246414</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458950</ogr:osx>
      <ogr:osy>162250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.199146292633782,50.870103910344284</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456450</ogr:osx>
      <ogr:osy>108150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.67327565911446,50.742000724452552</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423150</ogr:osx>
      <ogr:osy>93650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.395549760199057,50.847905140238318</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442650</ogr:osx>
      <ogr:osy>105550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.957350845226288,51.260265678510535</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472850</ogr:osx>
      <ogr:osy>151750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.206312229365868,50.866555795596859</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455950</ogr:osx>
      <ogr:osy>107750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.743178941075033,51.283340986295414</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487750</ogr:osx>
      <ogr:osy>154550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.304261632844165,51.064114669756655</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448850</ogr:osx>
      <ogr:osy>129650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.769911022890438,51.302508528617473</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485850</ogr:osx>
      <ogr:osy>156650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
