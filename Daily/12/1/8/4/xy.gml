<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.563355292869875</gml:X><gml:Y>50.75108352446978</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9420229303023762</gml:X><gml:Y>51.28594539152564</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.27">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.563355292869875,50.751083524469784</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>430900</ogr:osx>
      <ogr:osy>94700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.388340906918602,50.910365667069648</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443100</ogr:osx>
      <ogr:osy>112500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.149854452937398,50.806356961443782</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460000</ogr:osx>
      <ogr:osy>101100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.942022930302376,51.022280724077717</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>474300</ogr:osx>
      <ogr:osy>125300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.365907405698925,50.991177876171754</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444600</ogr:osx>
      <ogr:osy>121500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.319224757531257,51.065551578628487</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447800</ogr:osx>
      <ogr:osy>129800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.96322238440011,51.285945391525637</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472400</ogr:osx>
      <ogr:osy>154600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
