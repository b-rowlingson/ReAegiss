<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.432929314760982</gml:X><gml:Y>50.79832544005308</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.967071672401993</gml:X><gml:Y>51.28693834669426</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.397440522218932,50.921653547262757</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442450</ogr:osx>
      <ogr:osy>113750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.396331890486712,50.897368130964061</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442550</ogr:osx>
      <ogr:osy>111050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.320983158692167,50.994072843336681</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447750</ogr:osx>
      <ogr:osy>121850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.311302974739134,51.070450939517919</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448350</ogr:osx>
      <ogr:osy>130350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.967071672401993,51.14615045589774</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472350</ogr:osx>
      <ogr:osy>139050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.098207820126522,50.798325440053084</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463650</ogr:osx>
      <ogr:osy>100250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.221895380348955,50.954787619181829</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>454750</ogr:osx>
      <ogr:osy>117550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.432929314760982,50.928125916492434</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439950</ogr:osx>
      <ogr:osy>114450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.333732024408946,50.899725556314529</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446950</ogr:osx>
      <ogr:osy>111350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.129421470331486,51.255376949484813</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460850</ogr:osx>
      <ogr:osy>151050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.04598617638328,51.270912673659318</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466650</ogr:osx>
      <ogr:osy>152850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.082220751009421,51.286938346694257</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464100</ogr:osx>
      <ogr:osy>154600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
