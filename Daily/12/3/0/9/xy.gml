<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.499891565716728</gml:X><gml:Y>50.81913849282159</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7676756325220203</gml:X><gml:Y>51.34044073637723</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.383931749723805,50.921133463767987</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443400</ogr:osx>
      <ogr:osy>113700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.317335452716823,50.999896636936022</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448000</ogr:osx>
      <ogr:osy>122500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.316463442616175,51.059240878935555</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448000</ogr:osx>
      <ogr:osy>129100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.499891565716728,50.986432548000671</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435200</ogr:osx>
      <ogr:osy>120900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.76767563252202,51.305632305996816</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486000</ogr:osx>
      <ogr:osy>157000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.176594778887622,50.819138492821594</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458100</ogr:osx>
      <ogr:osy>102500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.137175648837642,51.26217838437173</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460300</ogr:osx>
      <ogr:osy>151800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.342209505919475,51.054892406702947</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446200</ogr:osx>
      <ogr:osy>128600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.829912334454054,51.340440736377225</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481600</ogr:osx>
      <ogr:osy>160800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.950380833473149,51.283133369359867</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>473300</ogr:osx>
      <ogr:osy>154300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.966798855818349,50.86962782903916</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472800</ogr:osx>
      <ogr:osy>108300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.339458466661481,51.047683072906018</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446400</ogr:osx>
      <ogr:osy>127800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.059482735563596,51.276866694314869</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465700</ogr:osx>
      <ogr:osy>153500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.296545222807,50.96110600805293</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449500</ogr:osx>
      <ogr:osy>118200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.945688736784484,50.860446338891954</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>474300</ogr:osx>
      <ogr:osy>107300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.161887121052471,51.085212053835278</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458800</ogr:osx>
      <ogr:osy>132100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
