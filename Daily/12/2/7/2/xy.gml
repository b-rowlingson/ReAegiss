<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.787915843383919</gml:X><gml:Y>50.75910621919449</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8416440514180346</gml:X><gml:Y>51.33066598420631</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.965537153625071,50.862422463300312</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472900</ogr:osx>
      <ogr:osy>107500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.289646030811577,50.853154009863275</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450100</ogr:osx>
      <ogr:osy>106200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.787915843383919,50.933355939794922</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>415000</ogr:osx>
      <ogr:osy>114900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.841644051418035,51.330665984206313</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480800</ogr:osx>
      <ogr:osy>159700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.081971404892416,50.794152081902311</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464800</ogr:osx>
      <ogr:osy>99800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.461863678918081,50.948495705271249</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437900</ogr:osx>
      <ogr:osy>116700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.157256801495909,50.789324436351052</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459500</ogr:osx>
      <ogr:osy>99200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.320704366541143,51.061963274951147</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447700</ogr:osx>
      <ogr:osy>129400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.544849848770614,50.759106219194493</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>432200</ogr:osx>
      <ogr:osy>95600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
