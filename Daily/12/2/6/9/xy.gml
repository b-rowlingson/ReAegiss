<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.464958144714627</gml:X><gml:Y>50.92692812273333</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7405415118478584</gml:X><gml:Y>51.30755040693204</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.359019014908978,50.974054779886053</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445100</ogr:osx>
      <ogr:osy>119600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.983321534196395,51.154835490521137</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471200</ogr:osx>
      <ogr:osy>140000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.740541511847858,51.248692317085698</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>488000</ogr:osx>
      <ogr:osy>150700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.779101488528709,51.307550406932037</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485200</ogr:osx>
      <ogr:osy>157200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.464958144714627,50.926928122733329</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437700</ogr:osx>
      <ogr:osy>114300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
