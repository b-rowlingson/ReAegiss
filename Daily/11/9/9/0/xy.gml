<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.408854757778625</gml:X><gml:Y>50.79017812165132</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.758117787770595</gml:X><gml:Y>51.29251027289854</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.248996391373558,50.863241107544169</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452950</ogr:osx>
      <ogr:osy>107350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.921655399925356,51.009055392264528</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>475750</ogr:osx>
      <ogr:osy>123850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.765933533830189,51.236823114782602</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486250</ogr:osx>
      <ogr:osy>149350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.089258411334063,50.82073783478895</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464250</ogr:osx>
      <ogr:osy>102750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.010633454294952,50.913622018042517</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469650</ogr:osx>
      <ogr:osy>113150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.091270929969126,50.790178121651316</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464150</ogr:osx>
      <ogr:osy>99350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.067865047557685,51.28772418025364</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465100</ogr:osx>
      <ogr:osy>154700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.408854757778625,50.975216732690804</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441600</ogr:osx>
      <ogr:osy>119700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.105056124888782,51.292510272898539</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462500</ogr:osx>
      <ogr:osy>155200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.75">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.758117787770595,51.234492377678308</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486800</ogr:osx>
      <ogr:osy>149100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.76">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.173581407086015,50.829009161986988</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458300</ogr:osx>
      <ogr:osy>103600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.77">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.184343763474221,50.863256969538675</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457500</ogr:osx>
      <ogr:osy>107400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
