<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.656895696963</gml:X><gml:Y>50.75319454554514</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9853092433730029</gml:X><gml:Y>51.35770899307806</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                                
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.172257068139376,50.823604161074741</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458400</ogr:osx>
      <ogr:osy>103000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.176468289222725,50.826331758878943</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458100</ogr:osx>
      <ogr:osy>103300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.656895696963,50.753194545545142</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>424300</ogr:osx>
      <ogr:osy>94900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.166978630441051,51.357708993078056</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458100</ogr:osx>
      <ogr:osy>162400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.144170284768797,51.348552740249261</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459700</ogr:osx>
      <ogr:osy>161400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.049361482969947,50.792991684903271</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467100</ogr:osx>
      <ogr:osy>99700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.985309243373003,50.867991634341706</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471500</ogr:osx>
      <ogr:osy>108100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
