<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.556225488135099</gml:X><gml:Y>50.75555296505455</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7448023158454253</gml:X><gml:Y>51.3026917384391</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.126222796721579,51.235120939758545</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461100</ogr:osx>
      <ogr:osy>148800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.968160183793921,50.872337688562986</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472700</ogr:osx>
      <ogr:osy>108600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.409731667972214,50.905979480130398</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441600</ogr:osx>
      <ogr:osy>112000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.492030230888536,50.923451993828856</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435800</ogr:osx>
      <ogr:osy>113900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.329421455100056,51.051222855379478</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447100</ogr:osx>
      <ogr:osy>128200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.979704439206683,50.864345692128538</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471900</ogr:osx>
      <ogr:osy>107700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.326542283504511,51.053004764946905</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447300</ogr:osx>
      <ogr:osy>128400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.339125859183267,51.071061094848801</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446400</ogr:osx>
      <ogr:osy>130400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.339125859183267,51.071061094848801</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446400</ogr:osx>
      <ogr:osy>130400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.556225488135099,50.755552965054548</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431400</ogr:osx>
      <ogr:osy>95200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.176146602606911,51.086212607733486</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457800</ogr:osx>
      <ogr:osy>132200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.744802315845425,51.302691738439101</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487600</ogr:osx>
      <ogr:osy>156700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
