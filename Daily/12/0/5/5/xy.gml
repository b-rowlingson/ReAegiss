<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.764899741242571</gml:X><gml:Y>50.84113954753855</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8127816008559637</gml:X><gml:Y>51.33667154262983</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.373123482306174,50.93141729357442</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444150</ogr:osx>
      <ogr:osy>114850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.764899741242571,50.841139547538553</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>416650</ogr:osx>
      <ogr:osy>104650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.349673597586045,50.878233344838236</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445850</ogr:osx>
      <ogr:osy>108950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.128275339150107,50.974359726650462</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461300</ogr:osx>
      <ogr:osy>119800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.27717011112252,50.925017867656344</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450900</ogr:osx>
      <ogr:osy>114200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.209978341089171,50.859836193314266</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455700</ogr:osx>
      <ogr:osy>107000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.204356872217786,50.856200966104382</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456100</ogr:osx>
      <ogr:osy>106600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.395966638244525,50.980546254704429</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442500</ogr:osx>
      <ogr:osy>120300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.849273096735614,51.25520688547924</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480400</ogr:osx>
      <ogr:osy>151300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.850369458647409,51.268705988748096</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480300</ogr:osx>
      <ogr:osy>152800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.830420494162176,51.264012314823283</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481700</ogr:osx>
      <ogr:osy>152300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.999479061352628,50.869912475334885</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470500</ogr:osx>
      <ogr:osy>108300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.466370643489999,50.927833825780652</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437600</ogr:osx>
      <ogr:osy>114400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.035446797192735,50.848632666765951</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468000</ogr:osx>
      <ogr:osy>105900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.812781600855964,51.33667154262983</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>482800</ogr:osx>
      <ogr:osy>160400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.261151196784143,50.858372724969371</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452100</ogr:osx>
      <ogr:osy>106800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
