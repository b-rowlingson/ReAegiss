<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.669596676767423</gml:X><gml:Y>50.76132456864894</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7809405813129872</gml:X><gml:Y>51.2922827980768</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.363856286984829,50.932716067149705</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444800</ogr:osx>
      <ogr:osy>115000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.780940581312987,51.292282798076798</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485100</ogr:osx>
      <ogr:osy>155500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.669596676767423,50.761324568648938</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423400</ogr:osx>
      <ogr:osy>95800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.319338914945714,50.960341768352528</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447900</ogr:osx>
      <ogr:osy>118100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.626483339853064,50.834034535870408</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>426400</ogr:osx>
      <ogr:osy>103900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.306912805122729,50.934190606641401</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448800</ogr:osx>
      <ogr:osy>115200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
