<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.46204001799076</gml:X><gml:Y>50.79380776123813</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7540630553138191</gml:X><gml:Y>51.30503867807509</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.312872328481805,50.964350512295319</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448350</ogr:osx>
      <ogr:osy>118550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.381595716981497,50.828947620234409</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443650</ogr:osx>
      <ogr:osy>103450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.080547621847489,50.830561548810216</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464850</ogr:osx>
      <ogr:osy>103850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.135220093879896,51.251823090674208</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460450</ogr:osx>
      <ogr:osy>150650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.138016087443258,50.794129907798322</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460850</ogr:osx>
      <ogr:osy>99750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.754063055313819,51.305038678075086</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486950</ogr:osx>
      <ogr:osy>156950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.095457067597152,50.793807761238135</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463850</ogr:osx>
      <ogr:osy>99750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.070654174366151,50.899726868208731</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465450</ogr:osx>
      <ogr:osy>111550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.272530976696669,51.085503028872189</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451050</ogr:osx>
      <ogr:osy>132050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.46204001799076,50.933209420790469</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437900</ogr:osx>
      <ogr:osy>115000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.396381722435604,50.838467234886707</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442600</ogr:osx>
      <ogr:osy>104500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.855660653585666,51.11319061614374</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480200</ogr:osx>
      <ogr:osy>135500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.112073854184513,50.852837753264993</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462600</ogr:osx>
      <ogr:osy>106300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.089924444633209,50.822991215435948</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464200</ogr:osx>
      <ogr:osy>103000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
