<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.426487992935624</gml:X><gml:Y>50.79412990779832</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8468205010469895</gml:X><gml:Y>51.2673222589527</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.846820501046989,51.267322258952696</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480550</ogr:osx>
      <ogr:osy>152650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.138016087443258,50.794129907798322</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460850</ogr:osx>
      <ogr:osy>99750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.385882216015912,50.935082037149179</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443250</ogr:osx>
      <ogr:osy>115250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.426487992935624,50.931241826830941</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440400</ogr:osx>
      <ogr:osy>114800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.369438630306565,50.940839532450411</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444400</ogr:osx>
      <ogr:osy>115900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.390762929909701,50.942751160864375</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442900</ogr:osx>
      <ogr:osy>116100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.05783682200884,50.794858986495221</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466500</ogr:osx>
      <ogr:osy>99900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
