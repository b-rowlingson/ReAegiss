<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.554154585637351</gml:X><gml:Y>50.74969983307624</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7910006854047508</gml:X><gml:Y>51.29148786254754</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.554154585637351,50.749699833076242</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431550</ogr:osx>
      <ogr:osy>94550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.109674929214221,51.23904271651304</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462250</ogr:osx>
      <ogr:osy>149250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.328005545388738,50.902390503320852</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447350</ogr:osx>
      <ogr:osy>111650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.937844937660248,50.861724147744717</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>474850</ogr:osx>
      <ogr:osy>107450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.046071263549186,50.850068882093559</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467250</ogr:osx>
      <ogr:osy>106050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.997164477261749,50.878435691639524</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470650</ogr:osx>
      <ogr:osy>109250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.288625225230456,50.9669031406562</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450050</ogr:osx>
      <ogr:osy>118850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.75">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.828168240440146,51.268036299810994</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481850</ogr:osx>
      <ogr:osy>152750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.76">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.119735197892562,51.237320366629341</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461550</ogr:osx>
      <ogr:osy>149050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.77">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.03363116910886,50.902124149983948</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468050</ogr:osx>
      <ogr:osy>111850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.78">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.418600401716806,50.936148679835767</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440950</ogr:osx>
      <ogr:osy>115350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.79">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.482536484455572,51.20171943387804</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436250</ogr:osx>
      <ogr:osy>144850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.80">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.471953760992416,51.000247780088671</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437150</ogr:osx>
      <ogr:osy>122450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.81">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.267368004189171,50.869652646434616</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451650</ogr:osx>
      <ogr:osy>108050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.82">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.819201337478195,51.282333775958669</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>482450</ogr:osx>
      <ogr:osy>154350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.83">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.256683490664981,51.094395636750761</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452150</ogr:osx>
      <ogr:osy>133050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.84">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.33400052866928,50.930751185365281</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446900</ogr:osx>
      <ogr:osy>114800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.85">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.181790739416153,51.251708162193026</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457200</ogr:osx>
      <ogr:osy>150600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.86">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.791000685404751,51.291487862547541</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484400</ogr:osx>
      <ogr:osy>155400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.87">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.444942995297917,50.934927933060919</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439100</ogr:osx>
      <ogr:osy>115200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.88">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.849160885470356,51.25970189729275</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480400</ogr:osx>
      <ogr:osy>151800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.89">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.976981048652636,50.858926213764384</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472100</ogr:osx>
      <ogr:osy>107100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.90">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.35182298160536,50.979410523123434</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445600</ogr:osx>
      <ogr:osy>120200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.91">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.082220751009421,51.286938346694257</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464100</ogr:osx>
      <ogr:osy>154600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.92">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.119739253942039,51.274637484862517</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461500</ogr:osx>
      <ogr:osy>153200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.93">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.004798896226569,50.887044075248447</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470100</ogr:osx>
      <ogr:osy>110200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.94">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.120459881341142,51.236876208033308</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461500</ogr:osx>
      <ogr:osy>149000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.95">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.342311357416953,51.047699154885585</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446200</ogr:osx>
      <ogr:osy>127800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.96">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.087190599835251,50.817574219179747</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464400</ogr:osx>
      <ogr:osy>102400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
