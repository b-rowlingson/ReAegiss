<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.401116112248804</gml:X><gml:Y>50.81529856954869</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7381001800448237</gml:X><gml:Y>51.30803721439351</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.401116112248804,50.856926389486645</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442250</ogr:osx>
      <ogr:osy>106550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.330888190041237,50.8997092987019</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447150</ogr:osx>
      <ogr:osy>111350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.95121021440415,51.278195129088651</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>473250</ogr:osx>
      <ogr:osy>153750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.034263295464139,50.871554296418587</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468050</ogr:osx>
      <ogr:osy>108450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.083686235844694,50.815298569548695</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464650</ogr:osx>
      <ogr:osy>102150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.782675240409329,51.308037214393508</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484950</ogr:osx>
      <ogr:osy>157250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.322838128094267,51.062425264359462</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447550</ogr:osx>
      <ogr:osy>129450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.82507290840535,51.276997505267843</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>482050</ogr:osx>
      <ogr:osy>153750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.27824052828821,50.855781839285683</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450900</ogr:osx>
      <ogr:osy>106500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.738100180044824,51.285534434623585</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>488100</ogr:osx>
      <ogr:osy>154800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.387543003772157,51.079417683448732</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443000</ogr:osx>
      <ogr:osy>131300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
