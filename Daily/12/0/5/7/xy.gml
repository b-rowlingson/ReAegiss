<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.587625929593458</gml:X><gml:Y>50.7313879081979</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7497882434273129</gml:X><gml:Y>51.27756676225977</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.749788243427313,51.277566762259767</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487300</ogr:osx>
      <ogr:osy>153900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.068739288284682,50.817428445053871</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465700</ogr:osx>
      <ogr:osy>102400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.332963275240026,50.903767799262148</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447000</ogr:osx>
      <ogr:osy>111800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.752244023349843,51.239825311644523</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487200</ogr:osx>
      <ogr:osy>149700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.398146026272873,50.922106807107347</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442400</ogr:osx>
      <ogr:osy>113800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.587625929593458,50.731387908197902</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>429200</ogr:osx>
      <ogr:osy>92500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
