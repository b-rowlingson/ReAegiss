<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.54207134775992</gml:X><gml:Y>50.78962766518632</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7648313847040583</gml:X><gml:Y>51.30470312376264</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.315096445958971,50.861399412811899</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448300</ogr:osx>
      <ogr:osy>107100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.491636703271191,51.220192872438005</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435600</ogr:osx>
      <ogr:osy>146900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.408854757778625,50.975216732690804</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441600</ogr:osx>
      <ogr:osy>119700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.131509773475319,50.789627665186316</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461314.444660379</ogr:osx>
      <ogr:osy>99254.7130817909</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.54207134775992,50.898480029974664</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>432300</ogr:osx>
      <ogr:osy>111100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.127739937211749,51.230636202566401</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461000</ogr:osx>
      <ogr:osy>148300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.086838204809168,50.908397531058057</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464300</ogr:osx>
      <ogr:osy>112500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.469042310299388,50.943133104887941</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437400</ogr:osx>
      <ogr:osy>116100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.764831384704058,51.304703123762636</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486200</ogr:osx>
      <ogr:osy>156900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.205838486458073,50.936244955884376</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455900</ogr:osx>
      <ogr:osy>115500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.354045172863655,50.921871271620489</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445500</ogr:osx>
      <ogr:osy>113800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.386161743188318,50.967905831378005</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443200</ogr:osx>
      <ogr:osy>118900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.452132021750042,50.928667166295433</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438600</ogr:osx>
      <ogr:osy>114500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.412462786412299,50.914985721403838</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441400</ogr:osx>
      <ogr:osy>113000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.102303496631955,50.843770335183365</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463300</ogr:osx>
      <ogr:osy>105300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.073530748394982,50.790488252399591</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465400</ogr:osx>
      <ogr:osy>99400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
