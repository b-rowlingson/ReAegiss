<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.393541578252014</gml:X><gml:Y>50.83845251598405</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7450589093532339</gml:X><gml:Y>51.2810550070665</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.83082904761307,51.276155896524919</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481650</ogr:osx>
      <ogr:osy>153650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.077389166033998,50.846723524057275</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465050</ogr:osx>
      <ogr:osy>105650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.100853470986469,51.25066502749376</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462850</ogr:osx>
      <ogr:osy>150550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.077319687544465,51.281055007066499</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464450</ogr:osx>
      <ogr:osy>153950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.028336497437296,50.883195616057186</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468450</ogr:osx>
      <ogr:osy>109750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.099139794409078,51.26503917933902</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462950</ogr:osx>
      <ogr:osy>152150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.092451932033434,51.277126822229093</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463400</ogr:osx>
      <ogr:osy>153500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.745058909353234,51.240647738667136</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487700</ogr:osx>
      <ogr:osy>149800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.393541578252014,50.838452515984052</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442800</ogr:osx>
      <ogr:osy>104500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.265511950364854,50.852105412377853</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451800</ogr:osx>
      <ogr:osy>106100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
