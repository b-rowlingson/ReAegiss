<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.653394968574105</gml:X><gml:Y>50.74733897526582</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.77268715432545</gml:X><gml:Y>51.25218103114774</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                               
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.653394968574105,50.747338975265819</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>424550</ogr:osx>
      <ogr:osy>94250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.311276293405377,51.072249234830501</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448350</ogr:osx>
      <ogr:osy>130550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.542780212369767,50.753252892134746</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>432350</ogr:osx>
      <ogr:osy>94950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.018028312195672,50.900195455745859</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469150</ogr:osx>
      <ogr:osy>111650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.169497683511965,50.85910561770757</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458550</ogr:osx>
      <ogr:osy>106950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.77268715432545,51.252181031147742</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485750</ogr:osx>
      <ogr:osy>151050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.526443447839549,50.967209611131217</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>433350</ogr:osx>
      <ogr:osy>118750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.336243558526578,50.923120348996221</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446750</ogr:osx>
      <ogr:osy>113950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.272352004118162,50.914646568459411</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451250</ogr:osx>
      <ogr:osy>113050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.27448687949687,50.868797838177841</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451150</ogr:osx>
      <ogr:osy>107950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.395328965469312,50.864989861883515</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442650</ogr:osx>
      <ogr:osy>107450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.374099930867718,50.964694510165799</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444050</ogr:osx>
      <ogr:osy>118550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.647718760226369,50.748221246328534</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>424950</ogr:osx>
      <ogr:osy>94350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
