<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.677539628077082</gml:X><gml:Y>50.74021404969597</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7802117820799157</gml:X><gml:Y>51.29641001517717</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.33050143604345,50.926684571861344</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447150</ogr:osx>
      <ogr:osy>114350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.294651636599883,50.850936207477773</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449750</ogr:osx>
      <ogr:osy>105950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.788719960175033,51.296410015177166</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484550</ogr:osx>
      <ogr:osy>155950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.136703271784078,51.249136414355952</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460350</ogr:osx>
      <ogr:osy>150350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.57287721501885,50.87296820138441</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>430150</ogr:osx>
      <ogr:osy>108250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.352553499968196,50.875551584116096</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445650</ogr:osx>
      <ogr:osy>108650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.025962392094517,50.860694088865998</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468650</ogr:osx>
      <ogr:osy>107250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.780211782079916,51.292724820811664</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485150</ogr:osx>
      <ogr:osy>155550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.564076545030255,50.749737319651338</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>430850</ogr:osx>
      <ogr:osy>94550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.010411724903546,50.856966212880273</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469750</ogr:osx>
      <ogr:osy>106850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.441304675629161,50.941654906697991</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439350</ogr:osx>
      <ogr:osy>115950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.965582053455028,51.148834989755976</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472450</ogr:osx>
      <ogr:osy>139350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.548294610869651,50.769461160240759</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431950</ogr:osx>
      <ogr:osy>96750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.677539628077082,50.740214049695972</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>422850</ogr:osx>
      <ogr:osy>93450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.346944907143798,50.971739830019281</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445950</ogr:osx>
      <ogr:osy>119350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.21796490573413,50.849548437199878</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455150</ogr:osx>
      <ogr:osy>105850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.021944983539124,50.848969955678506</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468950</ogr:osx>
      <ogr:osy>105950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
