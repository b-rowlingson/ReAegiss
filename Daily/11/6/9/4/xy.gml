<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.501300013580007</gml:X><gml:Y>50.90714698882073</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.147784550001072</gml:X><gml:Y>51.3472302930078</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                               
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.291691823924211,50.952533865126398</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449850</ogr:osx>
      <ogr:osy>117250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.321450653578218,51.059719539274653</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447650</ogr:osx>
      <ogr:osy>129150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.147784550001072,51.347230293007804</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459450</ogr:osx>
      <ogr:osy>161250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.374871628516914,50.907146988820728</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444050</ogr:osx>
      <ogr:osy>112150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.394676949858916,50.915344534059464</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442650</ogr:osx>
      <ogr:osy>113050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.501300013580007,51.253504616318089</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434900</ogr:osx>
      <ogr:osy>150600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
