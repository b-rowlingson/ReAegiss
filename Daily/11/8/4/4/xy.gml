<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.661873287376878</gml:X><gml:Y>50.75096090842046</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8605951478666656</gml:X><gml:Y>51.28993758233643</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.427238079182612,50.928098153340528</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440350</ogr:osx>
      <ogr:osy>114450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.418846820044332,50.916366525451409</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440950</ogr:osx>
      <ogr:osy>113150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.661873287376878,50.75096090842046</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423950</ogr:osx>
      <ogr:osy>94650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.860595147866666,51.289937582336428</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>479550</ogr:osx>
      <ogr:osy>155150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.351855078532864,50.925905753050991</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445650</ogr:osx>
      <ogr:osy>114250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.394688609419859,50.914445347372101</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442650</ogr:osx>
      <ogr:osy>112950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.387527855853803,51.242624157082815</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442850</ogr:osx>
      <ogr:osy>149450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
