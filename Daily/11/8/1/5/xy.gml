<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.649008481852396</gml:X><gml:Y>50.76531116254134</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.006245746589089</gml:X><gml:Y>51.35451613085701</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.025943663404661,50.861593204064441</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468650</ogr:osx>
      <ogr:osy>107350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.047601397510746,50.84468574190079</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467150</ogr:osx>
      <ogr:osy>105450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.006245746589089,50.852434455396626</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470050</ogr:osx>
      <ogr:osy>106350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.078433681309344,50.793674555881999</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465050</ogr:osx>
      <ogr:osy>99750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.42518963581222,50.863342161373403</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440550</ogr:osx>
      <ogr:osy>107250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.649008481852396,50.765311162541337</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>424850</ogr:osx>
      <ogr:osy>96250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.370144149301166,50.941292960043519</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444350</ogr:osx>
      <ogr:osy>115950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.160574154212191,51.354516130857007</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458550</ogr:osx>
      <ogr:osy>162050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.317956262917026,51.054753454978368</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447900</ogr:osx>
      <ogr:osy>128600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.25649316536771,50.972548187356729</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452300</ogr:osx>
      <ogr:osy>119500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.402434988585313,51.253041925794271</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441800</ogr:osx>
      <ogr:osy>150600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.073708646406832,51.282375311050274</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464700</ogr:osx>
      <ogr:osy>154100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.068256247900652,50.841704987689518</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465700</ogr:osx>
      <ogr:osy>105100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
