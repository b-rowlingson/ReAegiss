<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.432841875147798</gml:X><gml:Y>50.86382926932312</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7387800224680151</gml:X><gml:Y>51.35710960607906</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.738780022468015,51.286890598422985</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>488050</ogr:osx>
      <ogr:osy>154950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.146165133699622,51.357109606079057</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459550</ogr:osx>
      <ogr:osy>162350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.362622561216472,50.970927151713383</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444850</ogr:osx>
      <ogr:osy>119250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.202095659458191,50.863829269323119</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456250</ogr:osx>
      <ogr:osy>107450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.200865726793812,50.935761417608013</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456250</ogr:osx>
      <ogr:osy>115450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.787214992505652,51.299092104317218</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484650</ogr:osx>
      <ogr:osy>156250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.976103437391947,50.86656234834529</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472150</ogr:osx>
      <ogr:osy>107950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.432841875147798,50.935319438231971</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439950</ogr:osx>
      <ogr:osy>115250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.051256314387509,51.293435655611567</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466250</ogr:osx>
      <ogr:osy>155350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.761323565954823,51.248464432486024</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486550</ogr:osx>
      <ogr:osy>150650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.766970581589778,51.305175274831747</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486050</ogr:osx>
      <ogr:osy>156950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.034633278307248,51.265424377754449</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467450</ogr:osx>
      <ogr:osy>152250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
