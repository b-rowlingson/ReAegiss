<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.5177221915878</gml:X><gml:Y>50.85968016789008</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.187247795979821</gml:X><gml:Y>50.93052490482741</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                                
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.464916901331521,50.930524904827415</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437700</ogr:osx>
      <ogr:osy>114700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.187247795979821,50.859680167890083</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457300</ogr:osx>
      <ogr:osy>107000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.442172216883352,50.928620036149624</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439300</ogr:osx>
      <ogr:osy>114500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.5177221915878,50.915467688580605</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434000</ogr:osx>
      <ogr:osy>113000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
