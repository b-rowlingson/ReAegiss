<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.477277931340743</gml:X><gml:Y>50.80967746515537</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7703193130001079</gml:X><gml:Y>51.2872260255413</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.770319313000108,51.287226025541301</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485850</ogr:osx>
      <ogr:osy>154950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.446065471021082,50.900312326793951</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439050</ogr:osx>
      <ogr:osy>111350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.082214114286632,50.817984827557119</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464750</ogr:osx>
      <ogr:osy>102450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.364977696609203,50.902597369767513</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444750</ogr:osx>
      <ogr:osy>111650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.127733524826276,51.268852471915018</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460950</ogr:osx>
      <ogr:osy>152550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.055409864906643,50.809677465155367</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466650</ogr:osx>
      <ogr:osy>101550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.069962200663268,50.827330221961752</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465600</ogr:osx>
      <ogr:osy>103500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.880136948400872,51.105334358372602</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>478500</ogr:osx>
      <ogr:osy>134600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.477277931340743,51.223726443660823</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436600</ogr:osx>
      <ogr:osy>147300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
