<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.335898783372709</gml:X><gml:Y>51.0472132945536</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7800450254957466</gml:X><gml:Y>51.33471852495877</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.335898783372709,51.0472132945536</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446650</ogr:osx>
      <ogr:osy>127750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.881764749316264,51.18583168282025</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>478250</ogr:osx>
      <ogr:osy>143550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.842259686588296,51.334718524958774</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480750</ogr:osx>
      <ogr:osy>160150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.780045025495747,51.29901763864487</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485150</ogr:osx>
      <ogr:osy>156250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
