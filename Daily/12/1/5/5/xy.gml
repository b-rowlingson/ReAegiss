<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.821441313128564</gml:X><gml:Y>50.78857466207868</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7406315282938813</gml:X><gml:Y>51.29172088379774</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.611949669544783,50.78857466207868</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>427450</ogr:osx>
      <ogr:osy>98850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.77019926487306,51.291720883797744</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485850</ogr:osx>
      <ogr:osy>155450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.740631528293881,51.271623657530128</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487950</ogr:osx>
      <ogr:osy>153250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.577639630005707,51.131963287691072</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>429650</ogr:osx>
      <ogr:osy>137050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.244559728797651,50.874003603967012</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>453250</ogr:osx>
      <ogr:osy>108550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.828876377742965,51.240167414698504</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481850</ogr:osx>
      <ogr:osy>149650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.247242659198226,50.883912770847203</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>453050</ogr:osx>
      <ogr:osy>109650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.512720179615733,50.917695040841707</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434350</ogr:osx>
      <ogr:osy>113250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.745713870900694,51.242902839678841</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487650</ogr:osx>
      <ogr:osy>150050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.805720563958492,50.929340317369437</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>413750</ogr:osx>
      <ogr:osy>114450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.821441313128564,50.91138063884668</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>412650</ogr:osx>
      <ogr:osy>112450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.023884739152935,50.8921511598652</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468750</ogr:osx>
      <ogr:osy>110750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.598449174093754,51.039417388794966</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>428250</ogr:osx>
      <ogr:osy>126750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.377859694838284,50.896371946109525</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443850</ogr:osx>
      <ogr:osy>110950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.430127621592421,50.92451530709495</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440150</ogr:osx>
      <ogr:osy>114050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.427414605422761,50.913711096923947</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440350</ogr:osx>
      <ogr:osy>112850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.453322629623386,50.887757039195257</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438550</ogr:osx>
      <ogr:osy>109950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.2975599821476,50.846457438357646</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449550</ogr:osx>
      <ogr:osy>105450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.855113105239603,51.106440923712036</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480250</ogr:osx>
      <ogr:osy>134750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
