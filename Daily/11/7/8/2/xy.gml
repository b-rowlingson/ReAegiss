<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.487708228252129</gml:X><gml:Y>50.79451745802582</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7484319489821907</gml:X><gml:Y>51.3007556929066</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.972631532266582,51.281981858915316</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471750</ogr:osx>
      <ogr:osy>154150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.062229779508831,51.106486589712837</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465750</ogr:osx>
      <ogr:osy>134550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.073858053721622,50.809825331227351</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465350</ogr:osx>
      <ogr:osy>101550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.071322828568594,50.794517458025815</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465550</ogr:osx>
      <ogr:osy>99850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.326884164600064,51.078634687622326</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447250</ogr:osx>
      <ogr:osy>131250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.332626454862325,50.977054376000396</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446950</ogr:osx>
      <ogr:osy>119950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.216333675733418,50.8621271806743</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455250</ogr:osx>
      <ogr:osy>107250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.748431948982191,51.248327321785887</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487450</ogr:osx>
      <ogr:osy>150650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.022670086471555,50.882249054756713</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468850</ogr:osx>
      <ogr:osy>109650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.774261194236028,51.300755692906598</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485550</ogr:osx>
      <ogr:osy>156450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.428020310933112,50.864255314900603</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440350</ogr:osx>
      <ogr:osy>107350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.457002056673059,50.938131945981382</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438250</ogr:osx>
      <ogr:osy>115550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.778611035779021,51.299002692843018</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485250</ogr:osx>
      <ogr:osy>156250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.440176886034673,51.153866663890689</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439250</ogr:osx>
      <ogr:osy>139550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.487708228252129,50.993124031133398</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436050</ogr:osx>
      <ogr:osy>121650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.106688509241793,51.245314436536567</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462450</ogr:osx>
      <ogr:osy>149950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
