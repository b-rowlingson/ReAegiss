<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.578282519775833</gml:X><gml:Y>50.7882121962862</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7518042213783526</gml:X><gml:Y>51.29464168490505</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.067138509019172,50.790886962492998</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465850</ogr:osx>
      <ogr:osy>99450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.578282519775833,51.218288974438977</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>429550</ogr:osx>
      <ogr:osy>146650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.298879341458862,51.231336526225164</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449050</ogr:osx>
      <ogr:osy>148250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.791634985682865,51.294641684905045</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484350</ogr:osx>
      <ogr:osy>155750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.070029061528039,50.788212196286196</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465650</ogr:osx>
      <ogr:osy>99150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.11214146500275,51.259743238339965</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462050</ogr:osx>
      <ogr:osy>151550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.751804221378353,51.282534014715935</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487150</ogr:osx>
      <ogr:osy>154450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.001599982842116,50.936925981762826</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470250</ogr:osx>
      <ogr:osy>115750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
