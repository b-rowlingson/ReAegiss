<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.473500835523478</gml:X><gml:Y>50.80248441311584</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.005461751247492</gml:X><gml:Y>51.26973016396247</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.337985035582989,50.900648991979111</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446650</ogr:osx>
      <ogr:osy>111450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.462818798075672,50.92736792730306</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437850</ogr:osx>
      <ogr:osy>114350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.12485003287565,51.269730163962471</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461150</ogr:osx>
      <ogr:osy>152650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.033724197978753,50.897628593746632</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468050</ogr:osx>
      <ogr:osy>111350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.055554866925304,50.802484413115842</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466650</ogr:osx>
      <ogr:osy>100750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.005461751247492,50.88929789439667</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470050</ogr:osx>
      <ogr:osy>110450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.473500835523478,50.989463970398383</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437050</ogr:osx>
      <ogr:osy>121250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
