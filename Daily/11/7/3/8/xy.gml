<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.548381134352657</gml:X><gml:Y>50.76046883820423</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.761209690754971</gml:X><gml:Y>51.34102561551182</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.442460084044189,51.320228849068492</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438950</ogr:osx>
      <ogr:osy>158050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.066637177995946,50.816062738148069</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465850</ogr:osx>
      <ogr:osy>102250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.968160251934988,51.161446985674431</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472250</ogr:osx>
      <ogr:osy>140750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.078773976355922,50.848533002463512</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464950</ogr:osx>
      <ogr:osy>105850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.387352462537309,50.931492778109863</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443150</ogr:osx>
      <ogr:osy>114850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.548381134352657,50.760468838204226</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431950</ogr:osx>
      <ogr:osy>95750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.210817635180367,50.852198132180874</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455650</ogr:osx>
      <ogr:osy>106150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.317949099178664,51.006644492137298</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447950</ogr:osx>
      <ogr:osy>123250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.84353650877945,51.341025615511825</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480650</ogr:osx>
      <ogr:osy>160850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.324304273524627,51.059736063553295</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447450</ogr:osx>
      <ogr:osy>129150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.761209690754971,51.306013706861158</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486450</ogr:osx>
      <ogr:osy>157050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.166165254872187,50.806924278447717</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458850</ogr:osx>
      <ogr:osy>101150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
