<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.502014053226434</gml:X><gml:Y>50.79198744686457</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7890657385846864</gml:X><gml:Y>51.33777763395659</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.421546852664205,50.92807011283432</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440750</ogr:osx>
      <ogr:osy>114450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.789065738584686,51.337777633956591</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484450</ogr:osx>
      <ogr:osy>160550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.403957382297672,50.8569409278047</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442050</ogr:osx>
      <ogr:osy>106550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.986360777839496,50.852263477667499</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471450</ogr:osx>
      <ogr:osy>106350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.389022618249544,50.91261735435053</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443050</ogr:osx>
      <ogr:osy>112750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.352179595048056,50.902527086995157</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445650</ogr:osx>
      <ogr:osy>111650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.092654635484236,50.791987446864574</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464050</ogr:osx>
      <ogr:osy>99550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.421702856420801,50.915481452952839</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440750</ogr:osx>
      <ogr:osy>113050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.038969741502746,50.850010632776758</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467750</ogr:osx>
      <ogr:osy>106050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.077353703406315,50.848521785384179</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465050</ogr:osx>
      <ogr:osy>105850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.502014053226434,50.987790458876589</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435050</ogr:osx>
      <ogr:osy>121050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.142400243309811,51.250976691702107</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459950</ogr:osx>
      <ogr:osy>150550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.082196494202811,50.81888396396743</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464750</ogr:osx>
      <ogr:osy>102550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.326470495541427,50.811556562667874</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447550</ogr:osx>
      <ogr:osy>101550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
