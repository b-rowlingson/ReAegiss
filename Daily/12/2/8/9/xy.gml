<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.733290359738096</gml:X><gml:Y>50.75683234194599</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7922798376965956</gml:X><gml:Y>51.34642403019168</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.262132007798594,50.886255943394744</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452000</ogr:osx>
      <ogr:osy>109900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.061119869718637,50.843446349610396</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466200</ogr:osx>
      <ogr:osy>105300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.494904787615041,50.920766727970481</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435600</ogr:osx>
      <ogr:osy>113600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.733290359738096,50.780371948755956</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>418900</ogr:osx>
      <ogr:osy>97900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.376812989852825,51.346424030191677</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443500</ogr:osx>
      <ogr:osy>161000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.145416988188537,50.816216553787093</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460300</ogr:osx>
      <ogr:osy>102200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.079328401556243,50.784239183230255</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465000</ogr:osx>
      <ogr:osy>98700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.129199606985902,50.924907715313161</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461300</ogr:osx>
      <ogr:osy>114300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.289673336446582,50.851355654938878</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450100</ogr:osx>
      <ogr:osy>106000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.042829885896692,51.042933124806787</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467200</ogr:osx>
      <ogr:osy>127500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.792279837696596,51.078381741710132</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484700</ogr:osx>
      <ogr:osy>131700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.356158200919942,50.974938292725682</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445300</ogr:osx>
      <ogr:osy>119700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.346164451183269,50.875066358533019</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446100</ogr:osx>
      <ogr:osy>108600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.671045965170291,50.756832341945994</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423300</ogr:osx>
      <ogr:osy>95300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.509074066422029,50.926222580451281</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434600</ogr:osx>
      <ogr:osy>114200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.828553354253261,51.281078869750509</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481800</ogr:osx>
      <ogr:osy>154200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
