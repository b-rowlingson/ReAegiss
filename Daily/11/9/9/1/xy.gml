<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.185187310721753</gml:X><gml:Y>50.79042036365015</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8329903934110994</gml:X><gml:Y>51.27572784560527</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.059649077714959,50.810610978917055</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466350</ogr:osx>
      <ogr:osy>101650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.973143390750773,50.871931988607976</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472350</ogr:osx>
      <ogr:osy>108550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.985515237219917,51.152606458301136</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471050</ogr:osx>
      <ogr:osy>139750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.185187310721753,50.855619117066709</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457450</ogr:osx>
      <ogr:osy>106550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.832990393411099,51.275727845605275</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481500</ogr:osx>
      <ogr:osy>153600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.065019694423904,50.790420363650149</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466000</ogr:osx>
      <ogr:osy>99400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
