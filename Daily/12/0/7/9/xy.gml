<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.483035795984303</gml:X><gml:Y>50.78382867698578</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9703750084366148</gml:X><gml:Y>51.25211736417508</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.084301323651378,50.783828676985785</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464650</ogr:osx>
      <ogr:osy>98650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.114437640503602,51.252117364175078</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461900</ogr:osx>
      <ogr:osy>150700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.483035795984303,51.221054413701637</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436200</ogr:osx>
      <ogr:osy>147000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.970375008436615,51.158319192464496</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472100</ogr:osx>
      <ogr:osy>140400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
