<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.441454711435342</gml:X><gml:Y>50.8033276438821</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.062433325198985</gml:X><gml:Y>51.26874484509518</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                               
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.254736842695425,50.859680848103942</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452550</ogr:osx>
      <ogr:osy>106950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.14194281457097,50.812144335841715</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460550</ogr:osx>
      <ogr:osy>101750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.062433325198985,50.813331211730109</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466150</ogr:osx>
      <ogr:osy>101950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.062433325198985,50.813331211730109</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466150</ogr:osx>
      <ogr:osy>101950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.14194281457097,50.812144335841715</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460550</ogr:osx>
      <ogr:osy>101750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.351011607345628,51.088662514620161</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445550</ogr:osx>
      <ogr:osy>132350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.257201792270355,51.062026743301587</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452150</ogr:osx>
      <ogr:osy>129450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.113401466518857,51.268744845095178</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461950</ogr:osx>
      <ogr:osy>152550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.425660308213433,50.94067981082754</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440450</ogr:osx>
      <ogr:osy>115850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.166229253494991,50.803327643882099</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458850</ogr:osx>
      <ogr:osy>100750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.441454711435342,51.166461680842914</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439150</ogr:osx>
      <ogr:osy>140950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.419528618583984,50.861515649036789</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440950</ogr:osx>
      <ogr:osy>107050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.377308156155575,50.93773438598884</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443850</ogr:osx>
      <ogr:osy>115550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.336976087507759,50.971683694548567</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446650</ogr:osx>
      <ogr:osy>119350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
