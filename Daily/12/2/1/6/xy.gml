<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.341303734989406</gml:X><gml:Y>50.78970087841242</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8587607402358194</gml:X><gml:Y>51.25885953615971</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.147032779635554,50.805437098240176</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460200</ogr:osx>
      <ogr:osy>101000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.322239277850895,50.858743319168674</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447800</ogr:osx>
      <ogr:osy>106800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.918291271838138,51.000481368651442</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>476000</ogr:osx>
      <ogr:osy>122900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.341303734989406,51.118732119675037</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446200</ogr:osx>
      <ogr:osy>135700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.087733407423713,50.789700878412418</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464400</ogr:osx>
      <ogr:osy>99300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.950933622431291,51.258859536159711</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>473300</ogr:osx>
      <ogr:osy>151600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.304066885268509,50.934173687586707</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449000</ogr:osx>
      <ogr:osy>115200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.858760740235819,51.103329219039644</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480000</ogr:osx>
      <ogr:osy>134400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
