

var myLayer = new Array();
var oldLayers = new Array();
var ilayer;

var hotspotLayer;
var alertCode;

function myPermalink(){
};
myPermalink.prototype = new OpenLayers.Control.Permalink;
myPermalink.prototype.createParams = function(center,zoom,layers){
    var hash = OpenLayers.Control.Permalink.prototype.createParams.call(this,center,zoom,layers);
    hash['now']=now;
    return hash;
};

var selectControl;
selectControl = new OpenLayers.Control.SelectFeature(
                [],
                {
                    clickout: true, toggle: false,
                    multiple: false, hover: false,
                    toggleKey: "ctrlKey", // ctrl key removes from selection
                    multipleKey: "shiftKey" // shift key adds to selection
                }
            );

var dateLimits = {range: [new Date("1/5/2002"),new Date("4/4/2004")],
		  start: new Date("1/5/2002")
};


function tmapInit(){
    cal = new YAHOO.widget.Calendar("calendar",
				    {pagedate:dateLimits.start,
				     mindate:dateLimits.range[0],
				     maxdate:dateLimits.range[1]
				     }
				    ); 
    alertCode = readAlerts();
    calendarSetup(cal);
    map = new OpenLayers.Map ("map", {
	    controls:[
		      new OpenLayers.Control.Navigation(),
		      new OpenLayers.Control.PanZoomBar(),
		      new OpenLayers.Control.LayerSwitcher()
		      ],
	    maxExtent: new OpenLayers.Bounds(-20037508.34,-20037508.34,20037508.34,20037508.34),
	    maxResolution: 156543.0399,
	    numZoomLevels: 19,
	    units: 'm',
	    projection: new OpenLayers.Projection("EPSG:900913"),
	    displayProjection: new OpenLayers.Projection("EPSG:4326")
	} );

    var extent = new OpenLayers.Bounds(-2.5,50.3,0,51.73).transform(new OpenLayers.Projection("EPSG:4326"),new OpenLayers.Projection("EPSG:900913"));
    map.setOptions({restrictedExtent: extent});

    layerTilesAtHome = new OpenLayers.Layer.OSM.Mapnik("OpenStreetMap");
    map.addLayer(layerTilesAtHome);

    
    if( ! map.getCenter() ){
    	var lonLat = new OpenLayers.LonLat(lon, lat).transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
    	map.setCenter (lonLat, zoom);
    }
    //map.setCenter(new OpenLayers.LonLat(-1.97305,50.68265),9)
//    map.addControl(new myPermalink('permalink'));

};

function dangerDay(workingDate, cell) { 
    YAHOO.util.Dom.addClass(cell, "danger"); 
    cell.innerHTML=workingDate.getDate();
    return "ok";
};
    
function warningDay(workingDate, cell) { 
    YAHOO.util.Dom.addClass(cell, "warning"); 
    cell.innerHTML=workingDate.getDate();
    return "ok";
};
    
function nodataDay(workingDate,cell){
    YAHOO.util.Dom.addClass(cell, "nodata"); 
    cell.innerHTML=workingDate.getDate();
    return "ok";
};

function yDate(d){
    // convert a JS date to a m/d/y string for renderer specs
    return (d.getMonth()+1)+"/"+d.getDate()+"/"+d.getFullYear()
};

function selectHandler(type,args,obj){
    var selected = args[0];
    var selDate = this.toDate(selected[0]);
    var ndays = dateToDay(selDate);
    setDay(ndays,exc);
};

function pageHandler(type,args,obj){
    // this selects the first day of the month
    cal.selectCell(cal.preMonthDays);
    cal.render();
};

function goPrevious(){
    var newDay = now - 1;
    if (+newDay >= +dateToDay(dateLimits.range[0])){
	updateCal(newDay);
    }
};

function goNext(){
    var newDay = now + 1;
    if (+newDay <= +dateToDay(dateLimits.range[1])){
	updateCal(newDay);
    }
};

function updateCal(dayCount){
	var theD = dayToDate(dayCount);
	cal.select(theD);
	cal.setYear(theD.getFullYear());
	cal.setMonth(theD.getMonth());
	cal.render();
};


function calendarSetup(cal){

    var i,d2;

    for(i=0;i<=alertCode.red.dates.length;i++){
	d2 = new Date();
	d2.setTime(+alertCode.red.dates[i] * 24*60*60*1000);
	d2.setHours(0);
	cal.addRenderer(yDate(d2),dangerDay);
    }
    for(i=0;i<=alertCode.orange.dates.length;i++){
	d2 = new Date();
	d2.setTime(+alertCode.orange.dates[i] * 24*60*60*1000);
	d2.setHours(0);
	cal.addRenderer(yDate(d2),warningDay);
    }

    for(i=0;i<=alertCode.nodata.dates.length;i++){
	d2 = new Date();
	d2.setTime(+alertCode.nodata.dates[i] * 24*60*60*1000);
	d2.setHours(0);
	cal.addRenderer(yDate(d2),nodataDay);
    }
    
    cal.selectEvent.subscribe(selectHandler,cal,true);
    cal.changePageEvent.subscribe(pageHandler,cal,true);

    cal.render();

};

function addILayer(t,exceedence){
    extent = new OpenLayers.Bounds();
    extent.extend(new OpenLayers.LonLat(-1.97305,50.68265).transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject()));
    extent.extend(new OpenLayers.LonLat(-0.69110,51.47565).transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject()));

    if(ilayer){
	ilayer.destroy();
    }
    
    ilayer = new OpenLayers.Layer.Image("exceedence",iLayerPath(t,exceedence),
					extent,
					new OpenLayers.Size(16,16), // size controls the zoom visibility
					{
					    opacity: 0.6,
					    isBaseLayer: false,
					    projection:new OpenLayers.Projection("EPSG:4326")
					} );
    
    map.addLayer(ilayer);
};

function dataPath(t){
    var ts = t.toString();
    var p1 = ts.substr(0,2);
    var p2 = ts.substr(2,1);
    var p3 = ts.substr(3,1);
    var p4 = ts.substr(4,1);
    return("Daily/"+p1+"/"+p2+"/"+p3+"/"+p4);
};

function iLayerPath(t,e){
    var tp = dataPath(t);
    return(tp+"/exceed"+e.toString()+".png");
};

function hotspotPath(t,e){
    var tp = dataPath(t);
    return(tp+"/hot-"+e.toString()+".gml");
};

function points(t){
    var tp = dataPath(t);
    return(tp+"/xy.gml");
};

function niceDate(t){
    var d = new Date();
    d.setTime(1000*24*60*60*t);
    return(d.toDateString());
};

function addHotspotLayer(t,e){
    if(hotspotLayer){
	hotspotLayer.destroy();
    }

    var style_hotspot = {
	strokeColor: "red",
	strokeWidth: 3,
	fillColor: "#808080",
	fillOpacity: 0.01
    };


    hotspotLayer = new OpenLayers.Layer.GML("hotspots",hotspotPath(t,e),
					    {
						projection: new OpenLayers.Projection("EPSG:4326"),
						style: style_hotspot
					    }
					    );

    map.addLayer(hotspotLayer);
};


function addTLayer(t){
    var myStyle = new Array();
    var myRadius = [10,8,6,4,2];
		
    for (var i=0;i<=4;i++){
	myStyle[i]= {pointRadius: myRadius[i], fillOpacity: 0.2, fillColor: "black",strokeColor: "#202020", strokeWidth: 1, strokeOpacity: 1}
    }

    for (var i=0;i<=4;i++)
	{
	    oldLayers[i]=myLayer[i];
	    var fileName = points(t-i);
	    myLayer[i] = new OpenLayers.Layer.GML(niceDate(t-i), fileName,
						{
						    projection: new OpenLayers.Projection("EPSG:4326"),
						    style: myStyle[i]
						}
						);
	}

    if(oldLayers[0]){
	for(var i=0;i<=4;i++){
	    map.removeLayer(oldLayers[i]);
	    oldLayers[i].destroy();
	}
    }
    for (var i=0;i<=4;i++){
	if(myLayer[i]){
	    map.addLayer(myLayer[i]);
	    myLayer[i].events.on({
		    "featureselected": function(e){clickedMe(e);},
			"featureunselected": function(e){unclickedMe(e)}
		}
		);
	}
    }

    selectControl.deactivate();
    map.removeControl(selectControl);
    selectControl = new OpenLayers.Control.SelectFeature(
                myLayer,
                {
                    clickout: true, toggle: false,
                    multiple: false, hover: false,
                    toggleKey: "ctrlKey", // ctrl key removes from selection
                    multipleKey: "shiftKey" // shift key adds to selection
                }
            );
            
    map.addControl(selectControl);
    selectControl.activate();


};

function clickedMe(e){
    onFeatureSelect(e.feature);
}
function unclickedMe(e){
    onFeatureUnselect(e.feature);
}

function readAlerts(){

    return({'red': makeArray("alerts/redAlerts.txt"),
		'orange': makeArray("alerts/orangeAlerts.txt"),
		'nodata': makeArray("alerts/nodataAlerts.txt")
		});
};

function alertState(t){
    var xt = "X"+t;
    if(alertCode.red.lookup[xt]){
	return("red");
    };
    if(alertCode.orange.lookup[xt]){
	return("orange");
    };
    if(alertCode.nodata.lookup[xt]){
	return("nodata");
    };
    return("green");
};

function makeArray(path){
    var options = {'url': path, async: false};
    var r = new OpenLayers.Request.GET(options);
    
    var responses = r.responseText.trim().split("\n");
    
    var answer = [];
    for( var i = 0 ; i <= responses.length;i++){
	answer["X"+responses[i]] = "X";
    }

    return({'lookup': answer, 'dates':responses});
};


function setTitle(exc){
    var title = document.getElementById("exceedence");
    title.innerHTML=exc;
    var current = alertState(now);
    var stateImg = document.getElementById("state");
    stateImg.src = "images/"+current+"alert.png";
};

function setNav(t){
    var nt = document.getElementById("navdate");
    var d = new Date();
    d.setTime(t*24*60*60*1000);
    nt.innerHTML=d.toDateString();
};

function dateToDay(d){
    // convert a JS Date to days-since-epoch
    return (+d / (24*60*60*1000));
};

function dayToDate(n){
    var d = new Date();
    d.setTime(n*24*60*60*1000);
    d.setHours(6);
    return(d);
};

function setDay(t,exc){
    now = Math.round(t);
    addILayer(now,exc);
    addHotspotLayer(now,exc);
    addTLayer(now);
    setTitle(exc);
    setNav(now);
    if (popup){
	popup.destroy();
    };
};


var popup;
function onFeatureSelect(feature) {
    selectedFeature = feature;

    popup = new OpenLayers.Popup.FramedCloud("chicken", 
                                     feature.geometry.getBounds().getCenterLonLat(),
                                     null,
                                     "<div style='font-size:.8em'>Date: " + feature.layer.name +"<br />OS Grid: " + feature.data.osx+","+feature.data.osy+"</div>",
                                     null, true, onPopupClose);
            feature.popup = popup;
            map.addPopup(popup);
};

function onFeatureUnselect(feature) {
    map.removePopup(feature.popup);
    feature.popup.destroy();
    feature.popup = null;
};

function onPopupClose(evt) {
    selectControl.unselect(selectedFeature);
};


function showYear(y,cal){
    cal.setYear(y);
    cal.setMonth(0);
    cal.render();
};

function chooseDay(type,args,obj){
    var selected = args[0];
    var selDate = this.toDate(selected[0]);
    var ndays = dateToDay(selDate);
    window.location.href="map1.html?now="+ndays;
};


function tYearInit(y){
    cal = new YAHOO.widget.CalendarGroup(
					 "calendar","calendar",
					 {PAGES:12,
					  mindate:dateLimits.range[0],
					  maxdate:dateLimits.range[1]
				     });
    alertCode = readAlerts();
    calendarSetup(cal);
    showYear(y,cal);
    cal.selectEvent.subscribe(chooseDay,cal,true);
};

				    
