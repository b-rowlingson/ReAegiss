<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.294667692702291</gml:X><gml:Y>50.86593319646795</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.00737988200599</gml:X><gml:Y>51.31943241860775</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                               
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.051608124617615,51.276353585783482</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466250</ogr:osx>
      <ogr:osy>153450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.00737988200599,50.86593319646795</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469950</ogr:osx>
      <ogr:osy>107850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.294667692702291,51.319432418607754</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449250</ogr:osx>
      <ogr:osy>158050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
