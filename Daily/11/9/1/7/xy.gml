<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.794357099676759</gml:X><gml:Y>50.78808679501269</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.036753134497387</gml:X><gml:Y>51.35591084625457</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.483756738766454,51.220607995691019</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436150</ogr:osx>
      <ogr:osy>146950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.441454711435342,51.166461680842914</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439150</ogr:osx>
      <ogr:osy>140950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.062864975535195,50.791751998062615</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466150</ogr:osx>
      <ogr:osy>99550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.489103434625878,50.995827833212168</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435950</ogr:osx>
      <ogr:osy>121950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.043750095697624,50.894114064911534</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467350</ogr:osx>
      <ogr:osy>110950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.054426261123657,50.788086795012688</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466750</ogr:osx>
      <ogr:osy>99150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.370144149301166,50.941292960043519</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444350</ogr:osx>
      <ogr:osy>115950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.37598130037426,50.930533348130375</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443950</ogr:osx>
      <ogr:osy>114750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.117924763124636,51.257089324681189</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461650</ogr:osx>
      <ogr:osy>151250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.382440806895973,50.980025946088034</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443450</ogr:osx>
      <ogr:osy>120250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.428781962968153,50.918214020283479</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440250</ogr:osx>
      <ogr:osy>113350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.467913503972679,51.228629849843728</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437250</ogr:osx>
      <ogr:osy>147850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.370338370720923,50.926906086877196</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444350</ogr:osx>
      <ogr:osy>114350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.794357099676759,50.924824704552229</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>414550</ogr:osx>
      <ogr:osy>113950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.367979907282781,50.89092339323615</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444550</ogr:osx>
      <ogr:osy>110350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.036753134497387,50.888660957862896</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467850</ogr:osx>
      <ogr:osy>110350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.349705281691534,50.978049900056625</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445750</ogr:osx>
      <ogr:osy>120050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.319312648317017,50.96214010079531</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447900</ogr:osx>
      <ogr:osy>118300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.16701123417963,51.355910846254574</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458100</ogr:osx>
      <ogr:osy>162200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.276764352746827,50.859369745185312</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451000</ogr:osx>
      <ogr:osy>106900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.325892018935686,50.901029446530764</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447500</ogr:osx>
      <ogr:osy>111500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
