<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.44068471705657</gml:X><gml:Y>50.93400838287285</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7452189432263602</gml:X><gml:Y>51.30663647638892</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.22">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.44068471705657,50.934008382872847</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439400</ogr:osx>
      <ogr:osy>115100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.23">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.826730554202962,51.296347358831085</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481900</ogr:osx>
      <ogr:osy>155900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.24">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.777691113164933,51.306636476388924</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485300</ogr:osx>
      <ogr:osy>157100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.25">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.74521894322636,51.28740938119774</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487600</ogr:osx>
      <ogr:osy>155000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.26">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.822819535452234,51.281021371419207</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>482200</ogr:osx>
      <ogr:osy>154200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.27">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.426344254487687,50.942931275700339</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440400</ogr:osx>
      <ogr:osy>116100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
