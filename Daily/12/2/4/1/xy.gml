<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.45927744363974</gml:X><gml:Y>50.85470035259467</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7464422011560776</gml:X><gml:Y>51.31976349238402</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.45927744363974,50.926002725930132</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438100</ogr:osx>
      <ogr:osy>114200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.746442201156078,51.242461024844161</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487600</ogr:osx>
      <ogr:osy>150000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.008328635317463,50.854700352594669</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469900</ogr:osx>
      <ogr:osy>106600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.091124893127771,51.271721297228105</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463500</ogr:osx>
      <ogr:osy>152900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.027035798885193,51.31976349238402</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467900</ogr:osx>
      <ogr:osy>158300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.39610326575319,50.860047965869079</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442600</ogr:osx>
      <ogr:osy>106900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
