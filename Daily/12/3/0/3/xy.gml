<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.543458377086545</gml:X><gml:Y>50.75640298309904</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8612786730435753</gml:X><gml:Y>51.29129305615518</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.94018981828709,50.85230305210068</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>474700</ogr:osx>
      <ogr:osy>106400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.861278673043575,51.291293056155176</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>479500</ogr:osx>
      <ogr:osy>155300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.344655036870002,50.881352678736491</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446200</ogr:osx>
      <ogr:osy>109300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.146869061032558,50.814428632694565</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460200</ogr:osx>
      <ogr:osy>102000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.290807495357404,50.870246992631721</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450000</ogr:osx>
      <ogr:osy>108100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.070747192804677,50.787768277649441</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465600</ogr:osx>
      <ogr:osy>99100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.087383307575192,50.807683694733818</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464400</ogr:osx>
      <ogr:osy>101300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.94426815897847,50.860433500136622</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>474400</ogr:osx>
      <ogr:osy>107300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.888963788700556,51.094627624357521</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>477900</ogr:osx>
      <ogr:osy>133400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.484619812990427,51.078986506369347</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436200</ogr:osx>
      <ogr:osy>131200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.31739972531295,50.898282365868283</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448100</ogr:osx>
      <ogr:osy>111200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.543458377086545,50.756402983099036</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>432300</ogr:osx>
      <ogr:osy>95300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.369061456106147,50.968713994550917</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444400</ogr:osx>
      <ogr:osy>119000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
