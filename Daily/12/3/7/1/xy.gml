<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.547650601012976</gml:X><gml:Y>50.76271417440012</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7738195759152201</gml:X><gml:Y>51.35279347893368</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.237432954010761,51.352793478933684</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>453200</ogr:osx>
      <ogr:osy>161800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.320697143974499,50.964845884856089</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447800</ogr:osx>
      <ogr:osy>118600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.385484558925571,50.911249923159282</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443300</ogr:osx>
      <ogr:osy>112600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.83710872203479,51.282962580336942</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481200</ogr:osx>
      <ogr:osy>154400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.547650601012976,50.762714174400124</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>432000</ogr:osx>
      <ogr:osy>96000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.279619549587408,50.858488136753678</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450800</ogr:osx>
      <ogr:osy>106800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.77381957591522,51.290410010220043</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485600</ogr:osx>
      <ogr:osy>155300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.307801914887068,50.970165660465589</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448700</ogr:osx>
      <ogr:osy>119200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.388340906918602,50.910365667069648</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443100</ogr:osx>
      <ogr:osy>112500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.968279253537221,50.866943100520444</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472700</ogr:osx>
      <ogr:osy>108000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
