<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.586272377940971</gml:X><gml:Y>50.72418895240173</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7822792512367466</gml:X><gml:Y>51.29589361391051</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.257896114277615,50.884430588123791</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452300</ogr:osx>
      <ogr:osy>109700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.325194052546057,51.047601618561011</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447400</ogr:osx>
      <ogr:osy>127800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.457997852109265,51.280287970617891</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437900</ogr:osx>
      <ogr:osy>153600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.782279251236747,51.295893613910515</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485000</ogr:osx>
      <ogr:osy>155900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.444482443648508,50.973593040430799</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439100</ogr:osx>
      <ogr:osy>119500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.334782135479443,50.975717807010675</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446800</ogr:osx>
      <ogr:osy>119800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.190120244028039,50.857901601729296</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457100</ogr:osx>
      <ogr:osy>106800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.442139965286021,50.93131761190552</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439300</ogr:osx>
      <ogr:osy>114800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.074931518354296,50.791398645667918</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465300</ogr:osx>
      <ogr:osy>99500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.035112961874134,50.864816797289471</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468000</ogr:osx>
      <ogr:osy>107700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.913877945180964,51.189281447594233</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>476000</ogr:osx>
      <ogr:osy>143900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.214179906722344,50.863461597423829</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455400</ogr:osx>
      <ogr:osy>107400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.786770471470232,51.288746448346878</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484700</ogr:osx>
      <ogr:osy>155100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.14494089053179,50.842291904753672</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460300</ogr:osx>
      <ogr:osy>105100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.393692975307512,50.826762925454275</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442800</ogr:osx>
      <ogr:osy>103200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.029504617799769,50.861173225774131</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468400</ogr:osx>
      <ogr:osy>107300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.973863729744291,50.871488685796272</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472300</ogr:osx>
      <ogr:osy>108500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.586272377940971,50.724188952401725</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>429300</ogr:osx>
      <ogr:osy>91700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.169609285963076,50.812794103541449</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458600</ogr:osx>
      <ogr:osy>101800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.281054018371857,50.857597718673489</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450700</ogr:osx>
      <ogr:osy>106700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.921015844571863,51.005902094699962</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>475800</ogr:osx>
      <ogr:osy>123500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.137050173200306,50.808061529937568</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460900</ogr:osx>
      <ogr:osy>101300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
