<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.695758875661847</gml:X><gml:Y>50.75599988475997</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7655485935901246</gml:X><gml:Y>51.25650599686153</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.435818422872468,50.924542928048169</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439750</ogr:osx>
      <ogr:osy>114050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.078327603460861,50.799069389094278</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465050</ogr:osx>
      <ogr:osy>100350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.441606059135953,50.916477532059702</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439350</ogr:osx>
      <ogr:osy>113150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.555512432973845,50.755999884759973</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431450</ogr:osx>
      <ogr:osy>95250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.127049436150348,51.229282240239201</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461050</ogr:osx>
      <ogr:osy>148150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.400574405036224,50.899188480065654</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442250</ogr:osx>
      <ogr:osy>111250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.364622861089335,50.928673633031742</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444750</ogr:osx>
      <ogr:osy>114550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.123929448470012,51.242747080607025</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461250</ogr:osx>
      <ogr:osy>149650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.476208596346835,51.002065385156449</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436850</ogr:osx>
      <ogr:osy>122650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.765548593590125,51.251206749210397</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486250</ogr:osx>
      <ogr:osy>150950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.487747802235015,50.989527274761343</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436050</ogr:osx>
      <ogr:osy>121250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.376533873101742,50.889170867317233</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443950</ogr:osx>
      <ogr:osy>110150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.549712628268383,50.769466639886645</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431850</ogr:osx>
      <ogr:osy>96750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.072403350638195,50.811612330729375</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465450</ogr:osx>
      <ogr:osy>101750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.695758875661847,50.771737205360793</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>421550</ogr:osx>
      <ogr:osy>96950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.069582908536901,50.810690601645597</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465650</ogr:osx>
      <ogr:osy>101650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.844224976673015,51.256505996861527</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480750</ogr:osx>
      <ogr:osy>151450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.076607636728132,50.814343500941533</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465150</ogr:osx>
      <ogr:osy>102050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.298648951721476,51.057786542920439</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449250</ogr:osx>
      <ogr:osy>128950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
