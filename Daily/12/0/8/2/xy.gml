<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.499804634591377</gml:X><gml:Y>50.78900888512965</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7536746296915484</gml:X><gml:Y>51.2930910576984</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.322578391105437,50.884374092144014</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447750</ogr:osx>
      <ogr:osy>109650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.057245045655646,50.789008885129647</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466550</ogr:osx>
      <ogr:osy>99250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.384435474716126,50.936872911832239</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443350</ogr:osx>
      <ogr:osy>115450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.86123420303929,51.293091057698398</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>479500</ogr:osx>
      <ogr:osy>155500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.498883562106853,51.212131331376135</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435100</ogr:osx>
      <ogr:osy>146000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.499804634591377,50.994525266153346</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435200</ogr:osx>
      <ogr:osy>121800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.753674629691548,51.29289502690979</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487000</ogr:osx>
      <ogr:osy>155600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.355393250383779,50.92727420026813</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445400</ogr:osx>
      <ogr:osy>114400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
