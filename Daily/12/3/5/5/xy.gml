<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.577408239943688</gml:X><gml:Y>50.78603746492344</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7535546909134168</gml:X><gml:Y>51.25321004330378</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.402425395057252,50.921229530094934</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442100</ogr:osx>
      <ogr:osy>113700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.059735893081677,50.841636664621539</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466300</ogr:osx>
      <ogr:osy>105100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.300280917386842,51.091517300965563</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449100</ogr:osx>
      <ogr:osy>132700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.186093132943638,50.844384620224375</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457400</ogr:osx>
      <ogr:osy>105300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.126261274603383,50.853844069333128</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461600</ogr:osx>
      <ogr:osy>106400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.217081677562654,50.859884043507456</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455200</ogr:osx>
      <ogr:osy>107000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.079293092080327,50.786037464923439</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465000</ogr:osx>
      <ogr:osy>98900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.47459825135332,51.207528879740408</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436800</ogr:osx>
      <ogr:osy>145500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.829261588426468,51.253210043303781</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481800</ogr:osx>
      <ogr:osy>151100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.577408239943688,50.921993510639943</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>429800</ogr:osx>
      <ogr:osy>113700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.312180295659985,50.96299757681826</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448400</ogr:osx>
      <ogr:osy>118400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.753554690913417,51.244335434456971</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487100</ogr:osx>
      <ogr:osy>150200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.996503779599235,51.00837318756205</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470500</ogr:osx>
      <ogr:osy>123700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.114627512413514,51.242227506175993</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461900</ogr:osx>
      <ogr:osy>149600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.091936644813355,50.792431500791906</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464100</ogr:osx>
      <ogr:osy>99600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
