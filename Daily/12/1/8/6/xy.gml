<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.378037685572311</gml:X><gml:Y>50.79301476509213</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7678294662225564</gml:X><gml:Y>51.30881143251367</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.27">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.767829466222556,51.24628501224214</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486100</ogr:osx>
      <ogr:osy>150400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.804104453888394,51.118070270624422</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>483800</ogr:osx>
      <ogr:osy>136100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.906754958736665,51.308811432513671</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>476300</ogr:osx>
      <ogr:osy>157200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.05219864021151,50.793014765092131</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466900</ogr:osx>
      <ogr:osy>99700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.129053179594103,51.236940448103859</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460900</ogr:osx>
      <ogr:osy>149000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.782224178299346,51.243737944597314</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485100</ogr:osx>
      <ogr:osy>150100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.074860540368383,50.794995199584832</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465300</ogr:osx>
      <ogr:osy>99900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.378037685572311,50.936389405556241</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443800</ogr:osx>
      <ogr:osy>115400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
