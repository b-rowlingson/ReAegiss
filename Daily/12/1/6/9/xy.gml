<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.641960086012487</gml:X><gml:Y>50.73005646465034</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7427513947572093</gml:X><gml:Y>51.29079176617248</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.395738467764684,50.943226620743303</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442550</ogr:osx>
      <ogr:osy>116150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.059243361657736,50.90143402686116</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466250</ogr:osx>
      <ogr:osy>111750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.641960086012487,50.759894171191945</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>425350</ogr:osx>
      <ogr:osy>95650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.742751394757209,51.246467990534221</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487850</ogr:osx>
      <ogr:osy>150450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.341502546360038,51.054438813802328</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446250</ogr:osx>
      <ogr:osy>128550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.183782472700366,50.85471003914104</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457550</ogr:osx>
      <ogr:osy>106450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.344668700074813,51.234299357154576</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445850</ogr:osx>
      <ogr:osy>148550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.439225498095492,50.877798448256087</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439550</ogr:osx>
      <ogr:osy>108850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.508452651319622,50.9176771636171</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434650</ogr:osx>
      <ogr:osy>113250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.198157053872922,50.84491774142127</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456550</ogr:osx>
      <ogr:osy>105350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.905191535901382,51.284068461189058</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>476450</ogr:osx>
      <ogr:osy>154450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.313600564084293,50.914896248608258</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448350</ogr:osx>
      <ogr:osy>113050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.334459473742979,51.048104339690624</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446750</ogr:osx>
      <ogr:osy>127850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.762683606091058,51.251176505078014</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486450</ogr:osx>
      <ogr:osy>150950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.592596685753257,50.730056464650339</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>428850</ogr:osx>
      <ogr:osy>92350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.767355819892892,51.290791766172482</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486050</ogr:osx>
      <ogr:osy>155350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
