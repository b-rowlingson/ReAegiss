<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.615045123481279</gml:X><gml:Y>50.75711070741515</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7726632233730065</gml:X><gml:Y>51.35068933899095</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.586921377095988,51.212924593052527</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>428950</ogr:osx>
      <ogr:osy>146050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.07316413913461,50.844891511524395</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465350</ogr:osx>
      <ogr:osy>105450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.615045123481279,50.757110707415151</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>427250</ogr:osx>
      <ogr:osy>95350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.129052869697434,51.350689338990954</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460750</ogr:osx>
      <ogr:osy>161650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.041515563474663,50.864419903824938</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467550</ogr:osx>
      <ogr:osy>107650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.772663223373007,51.253080009307126</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485750</ogr:osx>
      <ogr:osy>151150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.50838636221656,50.923971583682253</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434650</ogr:osx>
      <ogr:osy>113950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.073299674839711,50.802177041418631</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465400</ogr:osx>
      <ogr:osy>100700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.042378810960701,50.78753889940478</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467600</ogr:osx>
      <ogr:osy>99100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.968991980451204,51.220353567233687</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472100</ogr:osx>
      <ogr:osy>147300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.313153428921537,50.993577473325622</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448300</ogr:osx>
      <ogr:osy>121800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.345862426072026,50.896646745478762</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446100</ogr:osx>
      <ogr:osy>111000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.059796525204335,51.331720589897074</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465600</ogr:osx>
      <ogr:osy>159600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.75">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.082435682637185,51.276149569965625</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464100</ogr:osx>
      <ogr:osy>153400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.76">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.062218655137678,50.78859932447395</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466200</ogr:osx>
      <ogr:osy>99200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
