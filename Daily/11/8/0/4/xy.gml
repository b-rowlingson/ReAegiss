<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.647718760226369</gml:X><gml:Y>50.74822124632853</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7472437688873351</gml:X><gml:Y>51.26049958933828</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.315531352882038,51.074072668408618</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448050</ogr:osx>
      <ogr:osy>130750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.647718760226369,50.748221246328534</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>424950</ogr:osx>
      <ogr:osy>94350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.003215080728685,50.927947155981599</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470150</ogr:osx>
      <ogr:osy>114750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.080565280929821,50.829662414626583</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464850</ogr:osx>
      <ogr:osy>103750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.321516336771332,51.055223780696679</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447650</ogr:osx>
      <ogr:osy>128650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.492337780242844,51.221544709585785</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435550</ogr:osx>
      <ogr:osy>147050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.30714234756776,51.062333318438704</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448650</ogr:osx>
      <ogr:osy>129450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.358375034781042,50.969105427595068</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445150</ogr:osx>
      <ogr:osy>119050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.09349588914442,51.260499589338281</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463350</ogr:osx>
      <ogr:osy>151650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.209488363470379,50.846793553188917</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455750</ogr:osx>
      <ogr:osy>105550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.18805958530598,50.853840599595976</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457250</ogr:osx>
      <ogr:osy>106350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.491104090618579,51.203555387146707</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435650</ogr:osx>
      <ogr:osy>145050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.747243768887335,51.239322291295537</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487550</ogr:osx>
      <ogr:osy>149650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
