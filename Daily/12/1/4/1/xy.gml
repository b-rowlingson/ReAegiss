<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.573352890583519</gml:X><gml:Y>50.74700753296577</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.77268715432545</gml:X><gml:Y>51.25218103114774</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                               
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.326790919658474,50.887995467901249</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447450</ogr:osx>
      <ogr:osy>110050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.417514129388054,50.909165914433942</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441050</ogr:osx>
      <ogr:osy>112350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.455725810045317,50.925536612204702</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438350</ogr:osx>
      <ogr:osy>114150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.082689515762868,50.793708090077203</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464750</ogr:osx>
      <ogr:osy>99750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.348909260768636,50.933083343468887</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445850</ogr:osx>
      <ogr:osy>115050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.222644216706527,50.824400252102535</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>454850</ogr:osx>
      <ogr:osy>103050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.302388782218058,51.093777950628073</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448950</ogr:osx>
      <ogr:osy>132950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.555597532353397,50.747007532965775</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431450</ogr:osx>
      <ogr:osy>94250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.445755988095565,50.92638898373977</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439050</ogr:osx>
      <ogr:osy>114250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.058320328255183,50.806103875833287</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466450</ogr:osx>
      <ogr:osy>101150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.77268715432545,51.252181031147742</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485750</ogr:osx>
      <ogr:osy>151050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.117104715850644,50.849728471157746</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462250</ogr:osx>
      <ogr:osy>105950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.254651893946417,50.954101934552405</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452450</ogr:osx>
      <ogr:osy>117450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.299967261538373,51.064988268284793</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449150</ogr:osx>
      <ogr:osy>129750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.573352890583519,50.820813297838846</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>430150</ogr:osx>
      <ogr:osy>102450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.053336554080421,50.842034576842082</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466750</ogr:osx>
      <ogr:osy>105150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.15972138225712,51.086545358509774</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458950</ogr:osx>
      <ogr:osy>132250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.499672381856707,51.205390714427466</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435050</ogr:osx>
      <ogr:osy>145250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
