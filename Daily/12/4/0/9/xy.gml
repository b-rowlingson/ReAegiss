<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.439131991343908</gml:X><gml:Y>50.81030300088037</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7381987061703278</gml:X><gml:Y>51.35766802395221</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.077396946531596,50.810303000880367</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465100</ogr:osx>
      <ogr:osy>101600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.321999949178521,51.070963059473591</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447600</ogr:osx>
      <ogr:osy>130400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.825744216871854,51.335003758696821</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481900</ogr:osx>
      <ogr:osy>160200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.341520889102769,50.902017856729778</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446400</ogr:osx>
      <ogr:osy>111600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.425175866802272,50.922242935512415</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440500</ogr:osx>
      <ogr:osy>113800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.161234653451867,51.357668023952215</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458500</ogr:osx>
      <ogr:osy>162400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.738198706170328,51.281938588334796</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>488100</ogr:osx>
      <ogr:osy>154400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.439131991343908,50.944791842057441</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439500</ogr:osx>
      <ogr:osy>116300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
