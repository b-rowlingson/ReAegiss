<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.792196938832828</gml:X><gml:Y>50.9303496134816</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9096449828446055</gml:X><gml:Y>51.30793915887647</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.45225741174938,50.978575535621921</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438550</ogr:osx>
      <ogr:osy>120050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.427921922107144,50.930349613481596</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440300</ogr:osx>
      <ogr:osy>114700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.909644982844605,51.307939158876472</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>476100</ogr:osx>
      <ogr:osy>157100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.792196938832828,50.930665920035729</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>414700</ogr:osx>
      <ogr:osy>114600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
