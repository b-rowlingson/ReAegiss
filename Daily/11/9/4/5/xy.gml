<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.665388156443881</gml:X><gml:Y>50.75501773064101</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7966182543705522</gml:X><gml:Y>51.29604182644287</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.187231286132479,50.901495667754581</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457250</ogr:osx>
      <ogr:osy>111650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.856539467034091,51.280006652377097</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>479850</ogr:osx>
      <ogr:osy>154050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.267212969098922,50.879543511732948</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451650</ogr:osx>
      <ogr:osy>109150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.434515566389607,50.914644933047597</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439850</ogr:osx>
      <ogr:osy>112950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.015639486083775,50.878592916707184</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469350</ogr:osx>
      <ogr:osy>109250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.075081872020601,50.819727071959441</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465250</ogr:osx>
      <ogr:osy>102650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.125412984065326,51.24006054497972</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461150</ogr:osx>
      <ogr:osy>149350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.149087790341103,50.809498830556201</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460050</ogr:osx>
      <ogr:osy>101450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.91467674956699,51.002695995954795</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>476250</ogr:osx>
      <ogr:osy>123150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.250690886264736,50.846166021797131</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452850</ogr:osx>
      <ogr:osy>105450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.955332578615239,51.159534694096592</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>473150</ogr:osx>
      <ogr:osy>140550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.75">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.347637201452189,50.92228528545958</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445950</ogr:osx>
      <ogr:osy>113850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.76">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.131943094085616,50.853886456398378</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461200</ogr:osx>
      <ogr:osy>106400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.77">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.829261588426468,51.253210043303781</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481800</ogr:osx>
      <ogr:osy>151100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.78">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.665388156443881,50.755017730641015</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423700</ogr:osx>
      <ogr:osy>95100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.79">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.796618254370552,51.296041826442874</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484000</ogr:osx>
      <ogr:osy>155900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
