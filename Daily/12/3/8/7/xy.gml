<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.655491344056926</gml:X><gml:Y>50.75139187817126</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7490801438855046</gml:X><gml:Y>51.30363672978218</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.36598087808326,50.985782853766786</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444600</ogr:osx>
      <ogr:osy>120900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.299379264732064,50.962022279225479</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449300</ogr:osx>
      <ogr:osy>118300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.294378186526854,50.916130583713098</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449700</ogr:osx>
      <ogr:osy>113200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.749080143885505,51.303636729782177</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487300</ogr:osx>
      <ogr:osy>156800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.968477621780755,50.857952108847165</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472700</ogr:osx>
      <ogr:osy>107000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.10865477114016,51.254771109881389</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462300</ogr:osx>
      <ogr:osy>151000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.304066885268509,50.934173687586707</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449000</ogr:osx>
      <ogr:osy>115200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.365266985100967,50.933622985526888</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444700</ogr:osx>
      <ogr:osy>115100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.945118746604985,51.011517674644878</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>474100</ogr:osx>
      <ogr:osy>124100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.830534550408867,51.25951733613983</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481700</ogr:osx>
      <ogr:osy>151800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.032215630207104,50.86749062611117</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468200</ogr:osx>
      <ogr:osy>108000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.422308350059781,50.924027280616045</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440700</ogr:osx>
      <ogr:osy>114000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.338230213554918,51.233813545944713</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446300</ogr:osx>
      <ogr:osy>148500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.655491344056926,50.751391878171262</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>424400</ogr:osx>
      <ogr:osy>94700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.422241554717262,50.929422416579982</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440700</ogr:osx>
      <ogr:osy>114600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
