<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.486750287271475</gml:X><gml:Y>50.85194263708267</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9690193878751424</gml:X><gml:Y>51.33035471906904</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.057671450326434,51.330354719069042</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465750</ogr:osx>
      <ogr:osy>159450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.478670451333687,50.90945485739821</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436750</ogr:osx>
      <ogr:osy>112350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.486750287271475,51.208931579064064</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435950</ogr:osx>
      <ogr:osy>145650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.969019387875142,50.865600728387065</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472650</ogr:osx>
      <ogr:osy>107850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.303830129900178,51.092887315337528</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448850</ogr:osx>
      <ogr:osy>132850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.173886628088467,50.851942637082672</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458250</ogr:osx>
      <ogr:osy>106150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
