<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.653428136753319</gml:X><gml:Y>50.74284274135266</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9512583883965223</gml:X><gml:Y>51.37379249745925</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.029645765888827,50.888602109530929</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468350</ogr:osx>
      <ogr:osy>110350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.150441687108541,50.81310578522551</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459950</ogr:osx>
      <ogr:osy>101850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.951258388396522,51.023712836348125</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>473650</ogr:osx>
      <ogr:osy>125450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.170988962567579,51.254779403701427</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457950</ogr:osx>
      <ogr:osy>150950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.029552374315106,50.893097667351462</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468350</ogr:osx>
      <ogr:osy>110850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.505617157633929,50.91676595577632</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434850</ogr:osx>
      <ogr:osy>113150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.301481282140305,50.964282972047997</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449150</ogr:osx>
      <ogr:osy>118550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.36498992290647,50.901698186152629</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444750</ogr:osx>
      <ogr:osy>111550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.092697458269875,51.373792497459249</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463250</ogr:osx>
      <ogr:osy>164250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.35060753513084,50.913309353647165</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445750</ogr:osx>
      <ogr:osy>112850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.420313932079452,50.912776829735535</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440850</ogr:osx>
      <ogr:osy>112750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.074248881948065,50.790044312385845</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465350</ogr:osx>
      <ogr:osy>99350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.076696250341303,50.809847820981027</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465150</ogr:osx>
      <ogr:osy>101550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.466564555080399,51.221430135027809</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437350</ogr:osx>
      <ogr:osy>147050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.466730683554792,51.207043710017636</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437350</ogr:osx>
      <ogr:osy>145450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.438696271704167,50.921859057229028</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439550</ogr:osx>
      <ogr:osy>113750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.653428136753319,50.742842741352661</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>424550</ogr:osx>
      <ogr:osy>93750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.303348691345322,50.934619030670675</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449050</ogr:osx>
      <ogr:osy>115250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.306622257468156,50.905862522790216</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448850</ogr:osx>
      <ogr:osy>112050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.380896887941833,50.989010146616799</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443550</ogr:osx>
      <ogr:osy>121250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.150751363597827,50.7960218377712</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459950</ogr:osx>
      <ogr:osy>99950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
