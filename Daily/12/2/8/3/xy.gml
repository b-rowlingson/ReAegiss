<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.439963068950263</gml:X><gml:Y>50.81566407542856</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7791968881612693</gml:X><gml:Y>51.34942027350159</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.327799715212965,51.064701957794348</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447200</ogr:osx>
      <ogr:osy>129700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.324665080483699,50.887533567111184</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447600</ogr:osx>
      <ogr:osy>110000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.139846356340076,51.349420273501586</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460000</ogr:osx>
      <ogr:osy>161500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.2984021016946,50.932341308241853</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449400</ogr:osx>
      <ogr:osy>115000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.047261897653692,51.105016768645989</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466800</ogr:osx>
      <ogr:osy>134400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.34436487019371,50.902033855870386</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446200</ogr:osx>
      <ogr:osy>111600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.073032860383182,50.815664075428558</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465400</ogr:osx>
      <ogr:osy>102200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.439963068950263,50.875553858780862</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439500</ogr:osx>
      <ogr:osy>108600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.779196888161269,51.303954518445259</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485200</ogr:osx>
      <ogr:osy>156800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
