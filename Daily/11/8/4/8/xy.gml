<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.610553460708652</gml:X><gml:Y>50.75325289213475</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.794218659723369</gml:X><gml:Y>51.33288509807689</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.542780212369767,50.753252892134746</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>432350</ogr:osx>
      <ogr:osy>94950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.069457883357388,50.816984539013085</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465650</ogr:osx>
      <ogr:osy>102350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.521264470390759,50.916831122286446</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>433750</ogr:osx>
      <ogr:osy>113150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.836607683303206,51.274415007631816</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481250</ogr:osx>
      <ogr:osy>153450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.386213432328839,50.909904883288881</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443250</ogr:osx>
      <ogr:osy>112450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.4082989041067,50.850668216896928</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441750</ogr:osx>
      <ogr:osy>105850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.610553460708652,50.785872230018967</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>427550</ogr:osx>
      <ogr:osy>98550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.336927316695858,51.075095162275751</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446550</ogr:osx>
      <ogr:osy>130850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.994485334909831,50.936864951844022</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470750</ogr:osx>
      <ogr:osy>115750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.481484999975041,50.91216511912765</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436550</ogr:osx>
      <ogr:osy>112650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.08575371124085,50.818462246732956</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464500</ogr:osx>
      <ogr:osy>102500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.799556397603925,51.293374316539165</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>483800</ogr:osx>
      <ogr:osy>155600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.794218659723369,51.332885098076893</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484100</ogr:osx>
      <ogr:osy>160000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.964176258476335,50.85971257121993</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>473000</ogr:osx>
      <ogr:osy>107200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.440824816202321,50.922318887081772</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439400</ogr:osx>
      <ogr:osy>113800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.377461481095594,50.979549909316091</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443800</ogr:osx>
      <ogr:osy>120200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
