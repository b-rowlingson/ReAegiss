<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.641234037308852</gml:X><gml:Y>50.75739401173306</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8556828550112444</gml:X><gml:Y>51.11229158936715</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.336882209566165,50.878161409464347</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446750</ogr:osx>
      <ogr:osy>108950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.172450309350976,50.852831729730326</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458350</ogr:osx>
      <ogr:osy>106250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.173085922793925,50.816865508663952</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458350</ogr:osx>
      <ogr:osy>102250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.384637113987123,50.921586804785484</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443350</ogr:osx>
      <ogr:osy>113750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.273802335506448,50.9128570910703</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451150</ogr:osx>
      <ogr:osy>112850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.319735497909607,50.884357566423866</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447950</ogr:osx>
      <ogr:osy>109650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.150621010203751,50.803215085109656</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459950</ogr:osx>
      <ogr:osy>100750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.465705568375643,50.923784191907814</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437650</ogr:osx>
      <ogr:osy>113950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.329434434774756,51.050323701157595</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447100</ogr:osx>
      <ogr:osy>128100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.390841845844003,50.827647343204468</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443000</ogr:osx>
      <ogr:osy>103300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.855682855011244,51.112291589367146</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480200</ogr:osx>
      <ogr:osy>135400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.641234037308852,50.762140102383761</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>425400</ogr:osx>
      <ogr:osy>95900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.320452758793586,50.883912115152206</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447900</ogr:osx>
      <ogr:osy>109600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.56754972643412,50.757394011733062</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>430600</ogr:osx>
      <ogr:osy>95400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.253660552681295,50.882605079980884</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452600</ogr:osx>
      <ogr:osy>109500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.020698339260805,50.874588745794021</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469000</ogr:osx>
      <ogr:osy>108800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
