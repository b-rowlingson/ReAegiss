<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.503400343850831</gml:X><gml:Y>50.79003304951279</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9809535992770304</gml:X><gml:Y>51.09135997075878</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.052826172693535,50.797066620785849</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466850</ogr:osx>
      <ogr:osy>100150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.98095359927703,50.839626531519635</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471850</ogr:osx>
      <ogr:osy>104950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.503400343850831,50.991393285768318</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434950</ogr:osx>
      <ogr:osy>121450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.072830383818738,50.790033049512793</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465450</ogr:osx>
      <ogr:osy>99350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.054371855219482,50.790784196433037</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466750</ogr:osx>
      <ogr:osy>99450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.089188459844491,50.824334384921528</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464250</ogr:osx>
      <ogr:osy>103150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.087803977004879,50.895365778508058</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464250</ogr:osx>
      <ogr:osy>111050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.085403935083981,50.800024343978947</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464550</ogr:osx>
      <ogr:osy>100450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.350973868240129,51.091359970758781</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445550</ogr:osx>
      <ogr:osy>132650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.187872242852338,50.864630461504916</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457250</ogr:osx>
      <ogr:osy>107550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
