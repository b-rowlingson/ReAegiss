<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.636316969336187</gml:X><gml:Y>50.75627966499982</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7519016670290113</gml:X><gml:Y>51.27893814771475</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.767101277583864,51.246726960864628</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486150</ogr:osx>
      <ogr:osy>150450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.075756008231342,50.785559860401747</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465250</ogr:osx>
      <ogr:osy>98850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.636316969336187,50.756279664999816</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>425750</ogr:osx>
      <ogr:osy>95250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.751901667029011,51.278938147714754</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487150</ogr:osx>
      <ogr:osy>154050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.117649432980547,51.271474542972072</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461650</ogr:osx>
      <ogr:osy>152850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.037368229935131,50.789745872747432</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467950</ogr:osx>
      <ogr:osy>99350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.225557756458636,50.819923280977164</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>454650</ogr:osx>
      <ogr:osy>102550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.865131580708172,51.105639328895428</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>479550</ogr:osx>
      <ogr:osy>134650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.318412259544682,51.072291043099277</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447850</ogr:osx>
      <ogr:osy>130550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.080229603667616,50.846745939506143</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464850</ogr:osx>
      <ogr:osy>105650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.511297669700379,50.917689099095746</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434450</ogr:osx>
      <ogr:osy>113250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.261456794933318,50.884003533436029</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452050</ogr:osx>
      <ogr:osy>109650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.846572964691335,51.277211251001617</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480550</ogr:osx>
      <ogr:osy>153750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.920209426694795,51.009941296073826</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>475850</ogr:osx>
      <ogr:osy>123950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
