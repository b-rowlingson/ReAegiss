<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.641220294500201</gml:X><gml:Y>50.76004913645934</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7607885473315167</gml:X><gml:Y>51.34687611533306</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.760788547331517,51.241714537890019</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486600</ogr:osx>
      <ogr:osy>149900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.844107209377146,51.34687611533306</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480600</ogr:osx>
      <ogr:osy>161500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.261222207797993,50.853876868937604</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452100</ogr:osx>
      <ogr:osy>106300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.549016580471913,50.768115054678674</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431900</ogr:osx>
      <ogr:osy>96600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.641220294500201,50.763938588021496</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>425400</ogr:osx>
      <ogr:osy>96100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.851981712068343,51.261528033831631</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480200</ogr:osx>
      <ogr:osy>152000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.86293446657451,51.282316967296921</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>479400</ogr:osx>
      <ogr:osy>154300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.556182991121033,50.760049136459337</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431400</ogr:osx>
      <ogr:osy>95700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.425175866802272,50.922242935512415</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440500</ogr:osx>
      <ogr:osy>113800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
