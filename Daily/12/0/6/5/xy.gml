<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.540649417314799</gml:X><gml:Y>50.8288866263706</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7693502204392846</gml:X><gml:Y>51.29665769208518</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.25">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.159656230020417,51.090141811527204</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458950</ogr:osx>
      <ogr:osy>132650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.26">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.221149844872801,50.828886626370597</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>454950</ogr:osx>
      <ogr:osy>103550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.27">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.267273313982374,51.237437422773077</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451250</ogr:osx>
      <ogr:osy>148950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.437708671441292,50.944785001245613</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439600</ogr:osx>
      <ogr:osy>116300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.503583828728274,50.907315269523643</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435000</ogr:osx>
      <ogr:osy>112100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.769350220439285,51.296657692085184</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485900</ogr:osx>
      <ogr:osy>156000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.341533566929906,50.901118677183575</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446400</ogr:osx>
      <ogr:osy>111500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.845483584695656,51.120284547163322</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480900</ogr:osx>
      <ogr:osy>136300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.540649417314799,50.898474447300259</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>432400</ogr:osx>
      <ogr:osy>111100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
