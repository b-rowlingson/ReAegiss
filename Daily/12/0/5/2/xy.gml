<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.665375342800084</gml:X><gml:Y>50.7568162230798</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8641462585359893</gml:X><gml:Y>51.31509571558247</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.057651075125118,50.839371669864043</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466450</ogr:osx>
      <ogr:osy>104850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.75">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.190153101140252,50.897019677806007</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457050</ogr:osx>
      <ogr:osy>111150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.76">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.089258411334063,50.82073783478895</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464250</ogr:osx>
      <ogr:osy>102750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.77">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.083563085992508,50.821592526906748</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464650</ogr:osx>
      <ogr:osy>102850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.78">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.332774712302521,51.066079289159362</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446850</ogr:osx>
      <ogr:osy>129850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.79">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.280433617830355,50.851748703318755</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450750</ogr:osx>
      <ogr:osy>106050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.80">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.374859590215753,50.908046173297549</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444050</ogr:osx>
      <ogr:osy>112250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.81">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.519767982663769,50.924018920261013</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>433850</ogr:osx>
      <ogr:osy>113950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.82">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.118965417422235,51.315095715582473</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461500</ogr:osx>
      <ogr:osy>157700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.83">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.864146258535989,51.291320902724472</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>479300</ogr:osx>
      <ogr:osy>155300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.84">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.665375342800084,50.756816223079802</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423700</ogr:osx>
      <ogr:osy>95300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.85">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.272278465502888,50.873730015963417</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451300</ogr:osx>
      <ogr:osy>108500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
