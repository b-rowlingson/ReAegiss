<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.554850511863135</gml:X><gml:Y>50.75105139158553</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.016302750807908</gml:X><gml:Y>51.25009901289257</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.425341925898373,50.908755062428277</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440500</ogr:osx>
      <ogr:osy>112300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.016302750807908,50.880846679774997</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469300</ogr:osx>
      <ogr:osy>109500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.085819771347677,51.250099012892569</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463900</ogr:osx>
      <ogr:osy>150500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.187981335709335,50.89925274160057</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457200</ogr:osx>
      <ogr:osy>111400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.484477667603027,51.220161573165491</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436100</ogr:osx>
      <ogr:osy>146900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.091396286198523,50.820304860958629</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464100</ogr:osx>
      <ogr:osy>102700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.554850511863135,50.751051391585534</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431500</ogr:osx>
      <ogr:osy>94700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.043833894218702,50.785752269179575</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467500</ogr:osx>
      <ogr:osy>98900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
