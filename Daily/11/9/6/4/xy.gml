<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.391468686198191</gml:X><gml:Y>50.84917554865331</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8295327864027713</gml:X><gml:Y>51.32308110059319</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.342980073968009,51.050850213733185</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446150</ogr:osx>
      <ogr:osy>128150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.829532786402771,51.270747615456344</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481750</ogr:osx>
      <ogr:osy>153050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.94239146296538,50.849175548653307</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>474550</ogr:osx>
      <ogr:osy>106050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.003079225033464,50.867695094354957</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470250</ogr:osx>
      <ogr:osy>108050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.060153599246833,51.279120097059803</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465650</ogr:osx>
      <ogr:osy>153750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.391468686198191,50.943204460289685</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442850</ogr:osx>
      <ogr:osy>116150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.364769753596309,50.917883469565325</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444750</ogr:osx>
      <ogr:osy>113350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.047774717031219,51.323081100593186</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466450</ogr:osx>
      <ogr:osy>158650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.316787431978683,50.891534388436263</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448150</ogr:osx>
      <ogr:osy>110450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.326884164600064,51.078634687622326</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447250</ogr:osx>
      <ogr:osy>131250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.385834860549025,50.938678764366635</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443250</ogr:osx>
      <ogr:osy>115650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
