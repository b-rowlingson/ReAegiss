<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.445821069345959</gml:X><gml:Y>50.85758856001394</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.030496441600209</gml:X><gml:Y>51.06829856372224</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.030999780192531,50.857588560013944</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468300</ogr:osx>
      <ogr:osy>106900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.030496441600209,50.881864668924777</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468300</ogr:osx>
      <ogr:osy>109600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.399626478700975,50.917618194093308</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442300</ogr:osx>
      <ogr:osy>113300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.327747639200828,51.068298563722244</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447200</ogr:osx>
      <ogr:osy>130100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.445821069345959,50.980793282721862</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439000</ogr:osx>
      <ogr:osy>120300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
