<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.55184394844121</gml:X><gml:Y>50.76902521104806</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7761612487749279</gml:X><gml:Y>51.34416498451231</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.2984021016946,50.932341308241853</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449400</ogr:osx>
      <ogr:osy>115000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.372502736131723,50.924669617136168</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444200</ogr:osx>
      <ogr:osy>114100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.829101763602333,51.259503016403045</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481800</ogr:osx>
      <ogr:osy>151800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.55184394844121,50.76902521104806</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431700</ogr:osx>
      <ogr:osy>96700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.38005806956093,50.891437756056412</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443700</ogr:osx>
      <ogr:osy>110400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.953406857056677,51.023282469088116</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>473500</ogr:osx>
      <ogr:osy>125400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.20450978314039,50.847209366547908</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456100</ogr:osx>
      <ogr:osy>105600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.416460201269152,50.936587631539382</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441100</ogr:osx>
      <ogr:osy>115400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.842739614173637,51.344164984512311</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480700</ogr:osx>
      <ogr:osy>161200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.073299674839711,50.802177041418631</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465400</ogr:osx>
      <ogr:osy>100700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.829426028062835,51.190265615943808</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481900</ogr:osx>
      <ogr:osy>144100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.106443718425639,50.850096987671442</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463000</ogr:osx>
      <ogr:osy>106000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.776161248774928,51.310217383084542</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485400</ogr:osx>
      <ogr:osy>157500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
