<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.760892137805337</gml:X><gml:Y>50.78385735707072</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9769220373697753</gml:X><gml:Y>51.06115424559679</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.336412915156045,51.061154245596789</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446600</ogr:osx>
      <ogr:osy>129300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.147425362563307,50.783857357070723</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460200</ogr:osx>
      <ogr:osy>98600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.760892137805337,50.93060609011868</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>416900</ogr:osx>
      <ogr:osy>114600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.976922037369775,50.86162351952089</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472100</ogr:osx>
      <ogr:osy>107400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.4253087242989,50.911452639585782</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440500</ogr:osx>
      <ogr:osy>112600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.323571053092459,50.963064072534031</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447600</ogr:osx>
      <ogr:osy>118400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
