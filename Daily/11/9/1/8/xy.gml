<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.475664828519555</gml:X><gml:Y>50.79703465087331</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8440999646352574</gml:X><gml:Y>51.26642325879912</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.475664828519555,50.797034650873307</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437050</ogr:osx>
      <ogr:osy>99850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.126624007790298,51.251759290093801</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461050</ogr:osx>
      <ogr:osy>150650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.443722024968056,50.97763598632185</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439150</ogr:osx>
      <ogr:osy>119950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.052644422131973,50.806057935721917</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466850</ogr:osx>
      <ogr:osy>101150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.846842997384254,51.266423258799122</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480550</ogr:osx>
      <ogr:osy>152550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.863333780343802,51.266134892884644</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>479400</ogr:osx>
      <ogr:osy>152500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.012852363428938,50.909594104251809</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469500</ogr:osx>
      <ogr:osy>112700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.844099964635257,51.118472393485206</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481000</ogr:osx>
      <ogr:osy>136100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
