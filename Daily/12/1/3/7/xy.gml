<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.471076340041897</gml:X><gml:Y>50.78990801966722</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.807184245997711</gml:X><gml:Y>51.35509872097824</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.305082813598163,51.104584654493657</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448750</ogr:osx>
      <ogr:osy>134150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.13634892620654,50.807606705501996</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460950</ogr:osx>
      <ogr:osy>101250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.198018349592556,50.853010172343083</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456550</ogr:osx>
      <ogr:osy>106250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.130854262653731,51.255387593040652</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460750</ogr:osx>
      <ogr:osy>151050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.038932797525229,50.851808873243463</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467750</ogr:osx>
      <ogr:osy>106250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.449057877375049,50.887736999649825</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438850</ogr:osx>
      <ogr:osy>109950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.117480460055194,51.355098720978241</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461550</ogr:osx>
      <ogr:osy>162150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.165480236243594,51.083888903603274</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458550</ogr:osx>
      <ogr:osy>131950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.436991583765919,50.945231168732697</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439650</ogr:osx>
      <ogr:osy>116350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.807184245997711,51.081682129887113</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>483650</ogr:osx>
      <ogr:osy>132050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.317104114112632,51.064190373307504</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447950</ogr:osx>
      <ogr:osy>129650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.75">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.057226964758568,50.789908019667223</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466550</ogr:osx>
      <ogr:osy>99350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.76">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.471076340041897,51.202567421633049</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437050</ogr:osx>
      <ogr:osy>144950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.77">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.374075774452631,50.966492860873018</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444050</ogr:osx>
      <ogr:osy>118750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.78">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.166202066589371,51.083444437055725</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458500</ogr:osx>
      <ogr:osy>131900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
