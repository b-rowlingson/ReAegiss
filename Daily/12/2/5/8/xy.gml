<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.671052263500289</gml:X><gml:Y>50.75593309528359</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8562802995473042</gml:X><gml:Y>51.26157010158304</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.095424652653403,51.052344325475786</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463500</ogr:osx>
      <ogr:osy>128500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.392291625094435,50.934665930279067</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442800</ogr:osx>
      <ogr:osy>115200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.856280299547304,51.26157010158304</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>479900</ogr:osx>
      <ogr:osy>152000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.450867906783972,50.915172568272851</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438700</ogr:osx>
      <ogr:osy>113000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.587641191252651,51.212477532056873</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>428900</ogr:osx>
      <ogr:osy>146000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.07205884447684,50.793174394658323</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465500</ogr:osx>
      <ogr:osy>99700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.986535179216597,51.138677218659922</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471000</ogr:osx>
      <ogr:osy>138200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.390727686968407,50.945448705378531</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442900</ogr:osx>
      <ogr:osy>116400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.671052263500289,50.755933095283595</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423300</ogr:osx>
      <ogr:osy>95200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
