<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.612246937439995</gml:X><gml:Y>50.75260511541473</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8267123000054748</gml:X><gml:Y>51.34494997963724</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.384376166462605,50.833458617593607</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443450</ogr:osx>
      <ogr:osy>103950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.424592033547324,50.911898734767441</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440550</ogr:osx>
      <ogr:osy>112650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.08130614658904,50.791898648947019</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464850</ogr:osx>
      <ogr:osy>99550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.294389132422607,50.962441873021703</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449650</ogr:osx>
      <ogr:osy>118350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.826712300005475,51.268920942916864</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481950</ogr:osx>
      <ogr:osy>152850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.286005605948076,50.858976994044838</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450350</ogr:osx>
      <ogr:osy>106850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.831171156861187,51.262670975695876</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481650</ogr:osx>
      <ogr:osy>152150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.131241215640399,50.853431601663196</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461250</ogr:osx>
      <ogr:osy>106350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.586882258718115,50.896398979330634</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>429150</ogr:osx>
      <ogr:osy>110850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.400830575916602,50.990013271266946</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442150</ogr:osx>
      <ogr:osy>121350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.75">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.612246937439995,50.752605115414731</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>427450</ogr:osx>
      <ogr:osy>94850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.76">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.831233336701168,51.344949979637242</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481500</ogr:osx>
      <ogr:osy>161300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.77">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.399129524819254,50.84567547444815</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442400</ogr:osx>
      <ogr:osy>105300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.78">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.141759207443535,51.246925564085437</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460000</ogr:osx>
      <ogr:osy>150100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.79">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.439358974483878,50.925908838654721</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439500</ogr:osx>
      <ogr:osy>114200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.80">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.22195682907565,50.823046770636566</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>454900</ogr:osx>
      <ogr:osy>102900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.81">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.321887239527788,50.883021199127548</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447800</ogr:osx>
      <ogr:osy>109500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.82">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.089620557069821,51.275306436142749</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463600</ogr:osx>
      <ogr:osy>153300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
