<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.736111943939252</gml:X><gml:Y>50.78307613779238</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7890703344386001</gml:X><gml:Y>51.31035155387244</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.270139784614445,50.874166296591255</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451450</ogr:osx>
      <ogr:osy>108550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.407421595444054,50.919906111853727</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441750</ogr:osx>
      <ogr:osy>113550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.276102724675545,50.856218234105221</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451050</ogr:osx>
      <ogr:osy>106550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.440474552204365,50.892192442541159</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439450</ogr:osx>
      <ogr:osy>110450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.31220468765709,50.913089537087195</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448450</ogr:osx>
      <ogr:osy>112850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.75">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.378815800166053,50.824436555033941</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443850</ogr:osx>
      <ogr:osy>102950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.76">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.492456366533681,51.210754850300404</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435550</ogr:osx>
      <ogr:osy>145850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.77">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.449387441765762,50.980360520914608</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438750</ogr:osx>
      <ogr:osy>120250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.78">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.102093528608787,51.260565830414194</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462750</ogr:osx>
      <ogr:osy>151650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.79">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.11619890182768,51.272362809979903</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461750</ogr:osx>
      <ogr:osy>152950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.80">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.084648329499288,51.273019663792901</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463950</ogr:osx>
      <ogr:osy>153050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.81">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.394041610179687,50.909496131232707</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442700</ogr:osx>
      <ogr:osy>112400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.82">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.7890703344386,51.310351553872437</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484500</ogr:osx>
      <ogr:osy>157500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.83">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.464720879624646,50.947609588988044</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437700</ogr:osx>
      <ogr:osy>116600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.84">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.736111943939252,50.783076137792385</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>418700</ogr:osx>
      <ogr:osy>98200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.85">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.853436936479228,51.260643070823534</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480100</ogr:osx>
      <ogr:osy>151900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.86">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.363022491523878,50.889547582483502</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444900</ogr:osx>
      <ogr:osy>110200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.87">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.014407345593671,50.903312381164312</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469400</ogr:osx>
      <ogr:osy>112000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.88">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.295422782354209,50.846894190187683</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449700</ogr:osx>
      <ogr:osy>105500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.89">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.481563582125449,51.224644693119423</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436300</ogr:osx>
      <ogr:osy>147400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.90">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.172590658436931,50.804721829269667</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458400</ogr:osx>
      <ogr:osy>100900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
