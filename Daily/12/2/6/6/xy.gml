<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.653321968718079</gml:X><gml:Y>50.75723067753515</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7576820009216566</gml:X><gml:Y>51.29549619977766</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.03755573431069,51.262750803108084</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467250</ogr:osx>
      <ogr:osy>151950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.653321968718079,50.757230677535148</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>424550</ogr:osx>
      <ogr:osy>95350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.359786721211655,50.970012410129378</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445050</ogr:osx>
      <ogr:osy>119150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.010633454294952,50.913622018042517</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469650</ogr:osx>
      <ogr:osy>113150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.787309729493783,51.295496199777659</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484650</ogr:osx>
      <ogr:osy>155850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.37972376481507,50.970119934696001</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443650</ogr:osx>
      <ogr:osy>119150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.054611291305215,50.84923908643767</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466650</ogr:osx>
      <ogr:osy>105950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.121703817772883,51.246776844918699</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461400</ogr:osx>
      <ogr:osy>150100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.046790577388738,50.849625122035071</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467200</ogr:osx>
      <ogr:osy>106000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.757682000921657,51.250673922487643</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486800</ogr:osx>
      <ogr:osy>150900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.55046900630111,50.764523596427694</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431800</ogr:osx>
      <ogr:osy>96200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
