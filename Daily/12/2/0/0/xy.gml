<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.589082113242527</gml:X><gml:Y>50.72689669412397</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8037667435032199</gml:X><gml:Y>51.27971205612484</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.499804634591377,50.994525266153346</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435200</ogr:osx>
      <ogr:osy>121800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.289933134918718,50.927793894944145</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450000</ogr:osx>
      <ogr:osy>114500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.078063463363864,51.279712056124843</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464400</ogr:osx>
      <ogr:osy>153800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.80376674350322,51.075802073684038</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>483900</ogr:osx>
      <ogr:osy>131400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.017232248108264,50.904235454681469</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469200</ogr:osx>
      <ogr:osy>112100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.318926482159906,50.89109725581865</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448000</ogr:osx>
      <ogr:osy>110400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.944078376048191,51.245309652586798</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>473800</ogr:osx>
      <ogr:osy>150100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.589082113242527,50.726896694123973</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>429100</ogr:osx>
      <ogr:osy>92000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
