<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.44685631493527</gml:X><gml:Y>50.89357171323722</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.004473150626816</gml:X><gml:Y>50.91229529684531</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                               
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.004473150626816,50.902328847377191</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470100</ogr:osx>
      <ogr:osy>111900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.072760135912598,50.901092503758555</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465300</ogr:osx>
      <ogr:osy>111700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.413919057024749,50.912295296845308</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441300</ogr:osx>
      <ogr:osy>112700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.44685631493527,50.893571713237222</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439000</ogr:osx>
      <ogr:osy>110600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
