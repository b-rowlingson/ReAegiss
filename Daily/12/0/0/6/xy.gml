<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.772481570605087</gml:X><gml:Y>50.74751778371103</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7908354111689344</gml:X><gml:Y>51.29778070686329</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.146708012215623,50.784301751834853</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460250</ogr:osx>
      <ogr:osy>98650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.720011566202172,50.747517783711025</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>419850</ogr:osx>
      <ogr:osy>94250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.828191101263936,51.267137305711643</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481850</ogr:osx>
      <ogr:osy>152650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.413270045518597,50.90734617881872</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441350</ogr:osx>
      <ogr:osy>112150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.139550262147958,50.787846296228508</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460750</ogr:osx>
      <ogr:osy>99050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.112809642557693,50.851494444910145</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462550</ogr:osx>
      <ogr:osy>106150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.339831939530609,50.920892563548925</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446500</ogr:osx>
      <ogr:osy>113700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.790835411168934,51.297780706863293</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484400</ogr:osx>
      <ogr:osy>156100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.017402537518589,50.896143493228188</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469200</ogr:osx>
      <ogr:osy>111200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.772481570605087,50.888364977541812</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>416100</ogr:osx>
      <ogr:osy>109900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.489011840750994,51.198600673354704</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435800</ogr:osx>
      <ogr:osy>144500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.450613902643427,50.936753220486416</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438700</ogr:osx>
      <ogr:osy>115400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.367405432709614,50.985790573118344</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444500</ogr:osx>
      <ogr:osy>120900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.513473604912924,50.913651585688768</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434300</ogr:osx>
      <ogr:osy>112800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.061894780155593,50.804783748320304</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466200</ogr:osx>
      <ogr:osy>101000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
