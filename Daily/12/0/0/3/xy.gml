<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.514011623650755</gml:X><gml:Y>50.78554861656469</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7876833601955585</gml:X><gml:Y>51.30853876747762</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.074337645194435,50.785548616564689</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465350</ogr:osx>
      <ogr:osy>98850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.514011623650755,50.930289809117589</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434250</ogr:osx>
      <ogr:osy>114650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.357743966732939,50.911550312942467</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445250</ogr:osx>
      <ogr:osy>112650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.787683360195559,51.308538767477621</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484600</ogr:osx>
      <ogr:osy>157300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.829747619950227,51.121027400624932</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>482000</ogr:osx>
      <ogr:osy>136400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.105928102491115,50.877071048548736</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463000</ogr:osx>
      <ogr:osy>109000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.384678354992773,50.972394224882741</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443300</ogr:osx>
      <ogr:osy>119400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
