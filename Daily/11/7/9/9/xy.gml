<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.442362286562161</gml:X><gml:Y>50.78550346878208</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7555458401464662</gml:X><gml:Y>51.30325599695977</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.373676835251421,50.996165565878258</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444050</ogr:osx>
      <ogr:osy>122050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.36174034268447,50.931355657738379</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444950</ogr:osx>
      <ogr:osy>114850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.168795968056713,50.81863357683671</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458650</ogr:osx>
      <ogr:osy>102450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.068664202143196,50.785503468782082</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465750</ogr:osx>
      <ogr:osy>98850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.079304716338168,50.821559011394449</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464950</ogr:osx>
      <ogr:osy>102850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.828938625758443,51.294121411189714</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481750</ogr:osx>
      <ogr:osy>155650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.834901587014049,51.285188692190722</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481350</ogr:osx>
      <ogr:osy>154650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.041660416948342,50.787982641942904</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467650</ogr:osx>
      <ogr:osy>99150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.190962741583409,50.850263695742647</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457050</ogr:osx>
      <ogr:osy>105950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.755545840146466,51.30325599695977</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486850</ogr:osx>
      <ogr:osy>156750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.442362286562161,50.972234088465783</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439250</ogr:osx>
      <ogr:osy>119350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.072794817750749,50.791831326116032</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465450</ogr:osx>
      <ogr:osy>99550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
