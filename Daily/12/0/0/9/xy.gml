<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.620010560076053</gml:X><gml:Y>50.75667734692708</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.067846849756574</gml:X><gml:Y>51.28862324028705</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.490886102683137,51.223336800740292</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435650</ogr:osx>
      <ogr:osy>147250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.51116583551126,50.930277937659199</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434450</ogr:osx>
      <ogr:osy>114650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.299093287522789,50.933694329058213</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449350</ogr:osx>
      <ogr:osy>115150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.620010560076053,50.756677346927084</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>426900</ogr:osx>
      <ogr:osy>95300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.067846849756574,51.288623240287052</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465100</ogr:osx>
      <ogr:osy>154800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.513614101839059,50.900163500024725</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434300</ogr:osx>
      <ogr:osy>111300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.382556748088162,50.917529216884269</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443500</ogr:osx>
      <ogr:osy>113300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.305089483767949,51.056476152981993</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448800</ogr:osx>
      <ogr:osy>128800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.083552675572722,50.930853285020667</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464500</ogr:osx>
      <ogr:osy>115000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.269343429707108,51.24239597127427</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451100</ogr:osx>
      <ogr:osy>149500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
