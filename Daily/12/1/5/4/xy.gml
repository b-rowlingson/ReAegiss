<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.387928241266335</gml:X><gml:Y>50.80398142380579</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7468727215201517</gml:X><gml:Y>51.27933405204788</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.746872721520152,51.279334052047879</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487500</ogr:osx>
      <ogr:osy>154100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.861267289262421,51.233742777535447</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>479600</ogr:osx>
      <ogr:osy>148900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.263539321904196,50.887164083472463</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451900</ogr:osx>
      <ogr:osy>110000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.837244895376037,51.277568610224684</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481200</ogr:osx>
      <ogr:osy>153800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.763701137514943,51.239946870555734</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486400</ogr:osx>
      <ogr:osy>149700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.387928241266335,50.941837102090794</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443100</ogr:osx>
      <ogr:osy>116000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.195309886204914,50.803981423805787</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456800</ogr:osx>
      <ogr:osy>100800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
