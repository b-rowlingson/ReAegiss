<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.497788686605395</gml:X><gml:Y>50.78978904839932</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7705440146629673</gml:X><gml:Y>51.30566245060184</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.31647820251666,50.961224311775538</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448100</ogr:osx>
      <ogr:osy>118200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.770544014662967,51.305662450601837</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485800</ogr:osx>
      <ogr:osy>157000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.497788686605395,50.917182187257815</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435400</ogr:osx>
      <ogr:osy>113200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.288157130039464,50.85764124191013</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450200</ogr:osx>
      <ogr:osy>106700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.363917585893462,50.928220171983114</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444800</ogr:osx>
      <ogr:osy>114500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.371407022989324,50.900384023047309</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444300</ogr:osx>
      <ogr:osy>111400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.178203508287821,50.808358577985949</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458000</ogr:osx>
      <ogr:osy>101300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.099081384357545,50.789789048399321</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463600</ogr:osx>
      <ogr:osy>99300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
