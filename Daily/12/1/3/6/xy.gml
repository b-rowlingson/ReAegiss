<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.373702710133331</gml:X><gml:Y>50.78887011342103</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9663170434654458</gml:X><gml:Y>51.06871104580446</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.040223588124146,50.788870113421034</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467750</ogr:osx>
      <ogr:osy>99250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.031495901996769,50.867934292702024</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468250</ogr:osx>
      <ogr:osy>108050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.321319236516003,51.068711045804456</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447650</ogr:osx>
      <ogr:osy>130150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.136032994344144,50.978463962999363</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460750</ogr:osx>
      <ogr:osy>120250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.966317043465446,50.859281909709985</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472850</ogr:osx>
      <ogr:osy>107150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.212540740354702,50.834224465580924</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455550</ogr:osx>
      <ogr:osy>104150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.355135352209301,50.894450184625811</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445450</ogr:osx>
      <ogr:osy>110750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.373702710133331,50.888256467314498</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444150</ogr:osx>
      <ogr:osy>110050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.153426377388679,50.805034017353812</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459750</ogr:osx>
      <ogr:osy>100950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.067084829338671,50.79358437243036</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465850</ogr:osx>
      <ogr:osy>99750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
