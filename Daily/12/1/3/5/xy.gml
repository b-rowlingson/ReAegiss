<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.499672381856707</gml:X><gml:Y>50.81058807423855</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7456649719858917</gml:X><gml:Y>51.29256461359787</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.295645447354353,51.067659999041133</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449450</ogr:osx>
      <ogr:osy>130050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.499672381856707,51.205390714427466</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435050</ogr:osx>
      <ogr:osy>145250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.853359253678704,51.292564613597868</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480050</ogr:osx>
      <ogr:osy>155450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.296016689801036,50.948963022992743</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449550</ogr:osx>
      <ogr:osy>116850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.745664971985892,51.244700780369143</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487650</ogr:osx>
      <ogr:osy>150250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.822347800013403,51.159170919138539</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>482450</ogr:osx>
      <ogr:osy>140650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.056810848631386,50.810588074238545</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466550</ogr:osx>
      <ogr:osy>101650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.025643873816249,50.875979027597587</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468650</ogr:osx>
      <ogr:osy>108950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.203746085578995,50.850351586261368</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456150</ogr:osx>
      <ogr:osy>105950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
