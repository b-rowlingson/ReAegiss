<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.455455549008162</gml:X><gml:Y>50.78960036784543</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7551568167581125</gml:X><gml:Y>51.24628501224214</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.333533390726148,51.062936323058707</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446800</ogr:osx>
      <ogr:osy>129500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.755156816758113,51.238057857825289</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487000</ogr:osx>
      <ogr:osy>149500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.349652068390361,50.930839365756292</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445800</ogr:osx>
      <ogr:osy>114800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.191727295485679,50.847121563597845</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457000</ogr:osx>
      <ogr:osy>105600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.455455549008162,51.00961401823244</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438300</ogr:osx>
      <ogr:osy>123500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.074967001899326,50.789600367845431</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465300</ogr:osx>
      <ogr:osy>99300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.356112663363783,50.875121635927336</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445400</ogr:osx>
      <ogr:osy>108600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.440738612401398,50.929512425774071</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439400</ogr:osx>
      <ogr:osy>114600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.767829466222556,51.24628501224214</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486100</ogr:osx>
      <ogr:osy>150400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
