<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.481044660151093</gml:X><gml:Y>50.81757421917975</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.014104511416804</gml:X><gml:Y>51.20710803033285</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.481044660151093,51.207108030332847</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436350</ogr:osx>
      <ogr:osy>145450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.085000112633823,50.820704527524661</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464550</ogr:osx>
      <ogr:osy>102750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.210863137532622,50.849500647740562</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455650</ogr:osx>
      <ogr:osy>105850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.014104511416804,50.883975579718864</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469450</ogr:osx>
      <ogr:osy>109850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.308547308331371,50.967921968641939</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448650</ogr:osx>
      <ogr:osy>118950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.2984021016946,50.932341308241853</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449400</ogr:osx>
      <ogr:osy>115000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.087190599835251,50.817574219179747</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464400</ogr:osx>
      <ogr:osy>102400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
