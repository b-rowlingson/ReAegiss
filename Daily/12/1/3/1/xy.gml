<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.482314839256222</gml:X><gml:Y>50.8054191818077</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8253708872659582</gml:X><gml:Y>51.3490075154695</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                               
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.144879770585331,51.349007515469502</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459650</ogr:osx>
      <ogr:osy>161450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.085298635928896,50.8054191818077</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464550</ogr:osx>
      <ogr:osy>101050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.98095359927703,50.839626531519635</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471850</ogr:osx>
      <ogr:osy>104950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.825370887265958,51.265310601511189</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>482050</ogr:osx>
      <ogr:osy>152450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.058481203438161,51.290796340455891</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465750</ogr:osx>
      <ogr:osy>155050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.482314839256222,51.221500827197147</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436250</ogr:osx>
      <ogr:osy>147050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.209184382759787,50.864776760967452</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455750</ogr:osx>
      <ogr:osy>107550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.862032685032168,51.115500898695664</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>479750</ogr:osx>
      <ogr:osy>135750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.343056366003327,51.045455271130344</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446150</ogr:osx>
      <ogr:osy>127550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
