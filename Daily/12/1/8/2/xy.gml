<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.669735798502739</gml:X><gml:Y>50.74154112792029</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7604320513518827</gml:X><gml:Y>51.3082535341228</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.310703250406888,50.966585842986909</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448500</ogr:osx>
      <ogr:osy>118800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.669735798502739,50.741541127920286</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423400</ogr:osx>
      <ogr:osy>93600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.197408393213794,50.847160759958136</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456600</ogr:osx>
      <ogr:osy>105600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.655445168853161,50.757686596746254</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>424400</ogr:osx>
      <ogr:osy>95400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.18295442406101,50.861448728028108</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457600</ogr:osx>
      <ogr:osy>107200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.86275789058669,51.115058331065235</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>479700</ogr:osx>
      <ogr:osy>135700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.486428556661211,50.915334312156503</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436200</ogr:osx>
      <ogr:osy>113000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.020604133403721,50.879084302521171</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469000</ogr:osx>
      <ogr:osy>109300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.762011435505445,51.302874906361247</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486400</ogr:osx>
      <ogr:osy>156700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.059482735563596,51.276866694314869</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465700</ogr:osx>
      <ogr:osy>153500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.760432051351883,51.308253534122798</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486500</ogr:osx>
      <ogr:osy>157300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
