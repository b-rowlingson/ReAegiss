<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.767023363374756</gml:X><gml:Y>50.72327456224616</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.823279867370755</gml:X><gml:Y>51.15063751836121</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.582030402350015,50.723274562246161</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>429600</ogr:osx>
      <ogr:osy>91600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.328020918800936,51.049416357318663</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447200</ogr:osx>
      <ogr:osy>128000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.823279867370755,51.150637518361208</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>482400</ogr:osx>
      <ogr:osy>139700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.767023363374756,50.842492676048188</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>416500</ogr:osx>
      <ogr:osy>104800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.581220850598121,50.814097357393308</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>429600</ogr:osx>
      <ogr:osy>101700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
