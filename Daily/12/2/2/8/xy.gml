<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.422464125933731</gml:X><gml:Y>50.79415208190231</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8483182047658456</gml:X><gml:Y>51.29158938011943</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.259630922090836,50.864657916567268</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452200</ogr:osx>
      <ogr:osy>107500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.057004491628958,50.836218991297322</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466500</ogr:osx>
      <ogr:osy>104500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.228408097018168,51.291589380119433</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>453900</ogr:osx>
      <ogr:osy>155000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.316412252712474,50.965720138460476</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448100</ogr:osx>
      <ogr:osy>118700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.848318204765846,51.121211737026925</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480700</ogr:osx>
      <ogr:osy>136400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.081971404892416,50.794152081902311</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464800</ogr:osx>
      <ogr:osy>99800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.314877452768077,51.070022322509274</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448100</ogr:osx>
      <ogr:osy>130300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.422464125933731,50.911438610271247</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440700</ogr:osx>
      <ogr:osy>112600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.980184868217791,51.167397397242922</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471400</ogr:osx>
      <ogr:osy>141400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
