<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.580605751457132</gml:X><gml:Y>50.72416872126642</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9588807199108961</gml:X><gml:Y>51.28770529900014</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.349823304271856,51.07157067034511</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445650</ogr:osx>
      <ogr:osy>130450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.580605751457132,50.724168721266416</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>429700</ogr:osx>
      <ogr:osy>91700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.958880719910896,51.287705299000145</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472700</ogr:osx>
      <ogr:osy>154800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.377250669632436,50.888725065809929</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443900</ogr:osx>
      <ogr:osy>110100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.068614119711846,50.823722373597548</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465700</ogr:osx>
      <ogr:osy>103100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
