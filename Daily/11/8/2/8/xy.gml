<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.49909704464189</gml:X><gml:Y>50.8430704334382</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8028328212733961</gml:X><gml:Y>51.08433541252856</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                               
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.802832821273396,51.08433541252856</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>483950</ogr:osx>
      <ogr:osy>132350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.19108711028797,50.8430704334382</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457050</ogr:osx>
      <ogr:osy>105150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.393648561995072,50.994472409168203</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442650</ogr:osx>
      <ogr:osy>121850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.129887672987673,50.849824450589949</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461350</ogr:osx>
      <ogr:osy>105950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.49909704464189,50.994072620390561</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435250</ogr:osx>
      <ogr:osy>121750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.29375541228247,50.910281687988139</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449750</ogr:osx>
      <ogr:osy>112550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.297453647896705,50.948072427003062</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449450</ogr:osx>
      <ogr:osy>116750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.394805172146031,50.90545347272468</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442650</ogr:osx>
      <ogr:osy>111950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.366582629063679,50.889117319596977</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444650</ogr:osx>
      <ogr:osy>110150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.200511186103528,50.95644174114743</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456250</ogr:osx>
      <ogr:osy>117750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
