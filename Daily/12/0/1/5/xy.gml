<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.64763781425412</gml:X><gml:Y>50.75901218524943</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7410901526789146</gml:X><gml:Y>51.29483633038264</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.64763781425412,50.759012185249432</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>424950</ogr:osx>
      <ogr:osy>95550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.080865347027106,50.814377111484667</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464850</ogr:osx>
      <ogr:osy>102050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.198945997408111,51.212711289158548</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456050</ogr:osx>
      <ogr:osy>146250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.902173976463958,51.049340411721083</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>477050</ogr:osx>
      <ogr:osy>128350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.330849541412419,50.902406831759471</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447150</ogr:osx>
      <ogr:osy>111650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.207839728907965,50.860271333666581</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455850</ogr:osx>
      <ogr:osy>107050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.371833755016597,50.921518658556231</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444250</ogr:osx>
      <ogr:osy>113750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.206205368953114,50.872849898531811</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455950</ogr:osx>
      <ogr:osy>108450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.20495251662352,50.862949495993149</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456050</ogr:osx>
      <ogr:osy>107350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.353259823650321,50.97851924476187</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445500</ogr:osx>
      <ogr:osy>120100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.741090152678915,51.281070481862209</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487900</ogr:osx>
      <ogr:osy>154300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.172193486454712,50.827200788549597</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458400</ogr:osx>
      <ogr:osy>103400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.397484097966791,50.973360187691135</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442400</ogr:osx>
      <ogr:osy>119500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.26963291319414,50.861123872081137</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451500</ogr:osx>
      <ogr:osy>107100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.019031957593508,50.886265252650823</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469100</ogr:osx>
      <ogr:osy>110100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.057681476455058,51.294836330382644</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465800</ogr:osx>
      <ogr:osy>155500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
