<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.500643288677828</gml:X><gml:Y>50.81848444980837</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7395337283333586</gml:X><gml:Y>51.28554987359551</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.193006883305223,51.01978684592995</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456700</ogr:osx>
      <ogr:osy>124800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.739533728333359,51.285549873595507</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>488000</ogr:osx>
      <ogr:osy>154800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.180837311507592,50.82006761645583</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457800</ogr:osx>
      <ogr:osy>102600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.500643288677828,50.916295180032527</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435200</ogr:osx>
      <ogr:osy>113100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.088592440792389,50.818484449808373</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464300</ogr:osx>
      <ogr:osy>102500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.012890388275328,50.907795895633541</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469500</ogr:osx>
      <ogr:osy>112500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
