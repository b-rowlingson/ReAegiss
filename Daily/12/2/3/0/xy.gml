<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.806433881753372</gml:X><gml:Y>50.84434547670266</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7747747898256744</gml:X><gml:Y>51.30840444720861</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.061101828235403,50.844345476702657</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466200</ogr:osx>
      <ogr:osy>105400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.429223790301158,50.940247652269505</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440200</ogr:osx>
      <ogr:osy>115800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.840043688621612,51.280294028376929</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481000</ogr:osx>
      <ogr:osy>154100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.30498062521822,50.96835042999956</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448900</ogr:osx>
      <ogr:osy>119000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.487570732562118,51.199493575208763</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435900</ogr:osx>
      <ogr:osy>144600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.806433881753372,50.928891882420132</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>413700</ogr:osx>
      <ogr:osy>114400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.774774789825674,51.308404447208609</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485500</ogr:osx>
      <ogr:osy>157300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
