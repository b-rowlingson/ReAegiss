<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.660520557654103</gml:X><gml:Y>50.80174645018185</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.079461060502619</gml:X><gml:Y>51.28152146032597</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.497540022871336,51.204032783844013</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435200</ogr:osx>
      <ogr:osy>145100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.399684298381905,50.913122256916374</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442300</ogr:osx>
      <ogr:osy>112800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.079461060502619,51.281521460325969</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464300</ogr:osx>
      <ogr:osy>154000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.660520557654103,51.230695322566405</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423800</ogr:osx>
      <ogr:osy>148000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.134328324218554,50.801746450181845</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461100</ogr:osx>
      <ogr:osy>100600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
