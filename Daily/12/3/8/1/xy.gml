<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.661193734460229</gml:X><gml:Y>50.72425759277665</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.039762941349905</gml:X><gml:Y>50.97515119181374</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.661193734460229,50.746912241944024</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>424000</ogr:osx>
      <ogr:osy>94200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.606105621024903,50.724257592776652</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>427900</ogr:osx>
      <ogr:osy>91700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.039762941349905,50.845970433153013</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467700</ogr:osx>
      <ogr:osy>105600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.396036599829402,50.975151191813737</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442500</ogr:osx>
      <ogr:osy>119700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.594586189963394,50.745800597879132</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>428700</ogr:osx>
      <ogr:osy>94100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
