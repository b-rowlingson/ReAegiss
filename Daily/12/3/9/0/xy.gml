<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.455788723864297</gml:X><gml:Y>50.78200809692969</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7898229763412277</gml:X><gml:Y>51.30901050349615</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.085456572536603,50.797326923121936</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464550</ogr:osx>
      <ogr:osy>100150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.081499919800792,50.782008096929687</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464850</ogr:osx>
      <ogr:osy>98450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.310795644396416,50.912181981036632</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448550</ogr:osx>
      <ogr:osy>112750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.29231955520857,50.911172248612857</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449850</ogr:osx>
      <ogr:osy>112650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.202295047561671,50.852140208914712</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456250</ogr:osx>
      <ogr:osy>106150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.455788723864297,50.920141441735097</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438350</ogr:osx>
      <ogr:osy>113550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.102584610047459,50.792064239541965</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463350</ogr:osx>
      <ogr:osy>99550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.789822976341228,51.309010503496147</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484450</ogr:osx>
      <ogr:osy>157350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.09754827417153,51.273119780408997</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463050</ogr:osx>
      <ogr:osy>153050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.393196095792921,50.919833084676576</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442750</ogr:osx>
      <ogr:osy>113550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
