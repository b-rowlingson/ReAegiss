<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.668922650751128</gml:X><gml:Y>50.75637670646207</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7597270334168073</gml:X><gml:Y>51.30779645976144</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.759727033416807,51.307796459761441</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486550</ogr:osx>
      <ogr:osy>157250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.478522505310001,50.79524892764929</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436850</ogr:osx>
      <ogr:osy>99650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.360723965515865,50.901674915168577</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445050</ogr:osx>
      <ogr:osy>111550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.445606417687846,50.938977672232049</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439050</ogr:osx>
      <ogr:osy>115650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.452722051978868,50.817612352185613</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438650</ogr:osx>
      <ogr:osy>102150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.512907802055765,50.899710934138106</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434350</ogr:osx>
      <ogr:osy>111250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.391136573975708,50.859572545379038</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442950</ogr:osx>
      <ogr:osy>106850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.390987113578054,50.980070793846437</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442850</ogr:osx>
      <ogr:osy>120250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.668922650751128,50.756376706462071</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423450</ogr:osx>
      <ogr:osy>95250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.91548970660623,51.151079053166818</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>475950</ogr:osx>
      <ogr:osy>139650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
