<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.499891565716728</gml:X><gml:Y>50.84683176861198</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8447805521791297</gml:X><gml:Y>51.27940220931948</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.472127111405718,50.984961606895531</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437150</ogr:osx>
      <ogr:osy>120750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.095991465428847,51.279402209319478</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463150</ogr:osx>
      <ogr:osy>153750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.84478055217913,51.119827983494702</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480950</ogr:osx>
      <ogr:osy>136250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.435535598081411,50.947921866781122</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439750</ogr:osx>
      <ogr:osy>116650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.982217115312745,50.846831768611978</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471750</ogr:osx>
      <ogr:osy>105750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.472844478820772,50.984515227551533</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437100</ogr:osx>
      <ogr:osy>120700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.499891565716728,50.986432548000671</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435200</ogr:osx>
      <ogr:osy>120900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.94693067346082,50.994448177359729</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>474000</ogr:osx>
      <ogr:osy>122200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.265187067665372,50.872786346913628</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451800</ogr:osx>
      <ogr:osy>108400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.420863350303984,50.925818615600804</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440800</ogr:osx>
      <ogr:osy>114200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.964106790446692,50.99100468449948</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472800</ogr:osx>
      <ogr:osy>121800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
