<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.51400225816516</gml:X><gml:Y>50.79323075392984</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7543307125519789</gml:X><gml:Y>51.30830150684871</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.377799807992091,50.900867877986322</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443850</ogr:osx>
      <ogr:osy>111450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.138032621100547,50.793230753929841</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460850</ogr:osx>
      <ogr:osy>99650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.754330712551979,51.295150067915259</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486950</ogr:osx>
      <ogr:osy>155850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.378263686963031,50.972809892694094</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443750</ogr:osx>
      <ogr:osy>119450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.346831062390316,50.878217480322519</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446050</ogr:osx>
      <ogr:osy>108950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.138376562623086,50.851685949316057</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460750</ogr:osx>
      <ogr:osy>106150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.51400225816516,50.931189011195528</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434250</ogr:osx>
      <ogr:osy>114750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.058848665231604,51.272815167302475</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465750</ogr:osx>
      <ogr:osy>153050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.38300364884227,50.829854330676874</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443550</ogr:osx>
      <ogr:osy>103550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.900311093971158,51.308301506848707</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>476750</ogr:osx>
      <ogr:osy>157150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
