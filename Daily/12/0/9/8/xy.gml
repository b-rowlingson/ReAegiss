<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.397111639278705</gml:X><gml:Y>50.78294782754327</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.013460293327396</gml:X><gml:Y>51.0021338142228</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                               
  <gml:featureMember>
    <ogr:points fid="points.21">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.195347562755874,50.843099878336169</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456750</ogr:osx>
      <ogr:osy>105150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.22">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.388268249176349,50.970164975052008</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443050</ogr:osx>
      <ogr:osy>119150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.23">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.261499424972862,50.88130603198266</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452050</ogr:osx>
      <ogr:osy>109350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.24">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.29775156023704,50.92829078210216</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449450</ogr:osx>
      <ogr:osy>114550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.25">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.032197017272654,50.868389741920168</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468200</ogr:osx>
      <ogr:osy>108100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.26">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.074487667451569,50.813877069848182</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465300</ogr:osx>
      <ogr:osy>102000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.27">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.013460293327396,50.880822696986385</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469500</ogr:osx>
      <ogr:osy>109500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.146023397820019,50.782947827543268</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460300</ogr:osx>
      <ogr:osy>98500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.31927324331737,50.964837598395988</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447900</ogr:osx>
      <ogr:osy>118600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.084422232622988,50.813955432787026</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464600</ogr:osx>
      <ogr:osy>102000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.13993782699589,50.805385025116266</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460700</ogr:osx>
      <ogr:osy>101000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.058966611763612,50.809256561512292</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466400</ogr:osx>
      <ogr:osy>101500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.397111639278705,51.002133814222802</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442400</ogr:osx>
      <ogr:osy>122700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.341913599662522,50.874143224902646</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446400</ogr:osx>
      <ogr:osy>108500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.034119352794121,50.844125324168687</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468100</ogr:osx>
      <ogr:osy>105400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
