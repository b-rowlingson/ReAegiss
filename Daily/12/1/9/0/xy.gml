<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-0.8416666999245256</gml:X><gml:Y>51.24540110102286</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7692858016338111</gml:X><gml:Y>51.32976699548144</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                            
  <gml:featureMember>
    <ogr:points fid="points.26">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.769285801633811,51.245401101022857</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486000</ogr:osx>
      <ogr:osy>150300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.27">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.841666699924526,51.329766995481435</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480800</ogr:osx>
      <ogr:osy>159600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
