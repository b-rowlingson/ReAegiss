<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.462878113426712</gml:X><gml:Y>50.79856921863803</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.049868543519822</gml:X><gml:Y>50.99576524219779</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.462878113426712,50.860374211837438</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437900</ogr:osx>
      <ogr:osy>106900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.049868543519822,50.837959668993747</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467000</ogr:osx>
      <ogr:osy>104700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.137050173200306,50.808061529937568</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460900</ogr:osx>
      <ogr:osy>101300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.341267195979969,50.920001417897439</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446400</ogr:osx>
      <ogr:osy>113600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.382944491267653,50.995765242197791</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443400</ogr:osx>
      <ogr:osy>122000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.22684056533804,50.957068567328548</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>454400</ogr:osx>
      <ogr:osy>117800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.401210434283212,50.905036859974707</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442200</ogr:osx>
      <ogr:osy>111900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.071952034598876,50.798569218638029</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465500</ogr:osx>
      <ogr:osy>100300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
