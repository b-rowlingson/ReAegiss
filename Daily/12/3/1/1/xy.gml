<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.471215709174209</gml:X><gml:Y>50.81667508131311</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7938682110383461</gml:X><gml:Y>51.33898803602596</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.08720812320533,50.816675081313107</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464400</ogr:osx>
      <ogr:osy>102300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.471215709174209,51.002492522305857</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437200</ogr:osx>
      <ogr:osy>122700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.793868211038346,51.291517430114361</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484200</ogr:osx>
      <ogr:osy>155400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.962026446220489,51.33898803602596</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472400</ogr:osx>
      <ogr:osy>160500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
