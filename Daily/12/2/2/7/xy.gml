<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.625681138008041</gml:X><gml:Y>50.75669567139247</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7666375406231539</gml:X><gml:Y>51.30563230599682</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.76767563252202,51.305632305996816</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486000</ogr:osx>
      <ogr:osy>157000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.067427378096062,50.812022311471551</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465800</ogr:osx>
      <ogr:osy>101800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.945709008004584,50.85954724582939</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>474300</ogr:osx>
      <ogr:osy>107200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.39963804382344,50.916719006940539</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442300</ogr:osx>
      <ogr:osy>113200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.035446797192735,50.848632666765951</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468000</ogr:osx>
      <ogr:osy>105900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.625681138008041,50.756695671392471</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>426500</ogr:osx>
      <ogr:osy>95300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.320585954630633,51.070055627088344</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447700</ogr:osx>
      <ogr:osy>130300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.408854757778625,50.975216732690804</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441600</ogr:osx>
      <ogr:osy>119700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.086541603925486,50.850842219112678</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464400</ogr:osx>
      <ogr:osy>106100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.169688968339313,50.808298310602005</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458600</ogr:osx>
      <ogr:osy>101300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.766637540623154,51.237280150271573</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486200</ogr:osx>
      <ogr:osy>149400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.290809058964509,50.963769064084133</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449900</ogr:osx>
      <ogr:osy>118500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
