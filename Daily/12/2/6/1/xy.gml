<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.511910201787726</gml:X><gml:Y>50.81738300453676</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.829935220353331</gml:X><gml:Y>51.33954175290636</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.388140561857308,50.925651814280634</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443100</ogr:osx>
      <ogr:osy>114200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.829935220353331,51.339541752906364</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481600</ogr:osx>
      <ogr:osy>160700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.093779274607428,51.282532328779965</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463300</ogr:osx>
      <ogr:osy>154100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.98850102228284,50.851832405532939</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471300</ogr:osx>
      <ogr:osy>106300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.063061992631528,50.817383004536758</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466100</ogr:osx>
      <ogr:osy>102400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.511910201787726,50.927133704637043</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434400</ogr:osx>
      <ogr:osy>114300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.422241554717262,50.929422416579982</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440700</ogr:osx>
      <ogr:osy>114600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
