<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.785490905955301</gml:X><gml:Y>50.75325289213475</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9011034050703026</gml:X><gml:Y>51.30516170013149</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.542780212369767,50.753252892134746</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>432350</ogr:osx>
      <ogr:osy>94950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.497691607589832,50.992268125757825</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435350</ogr:osx>
      <ogr:osy>121550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.231265116273087,50.818162474730848</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>454250</ogr:osx>
      <ogr:osy>102350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.349335579478448,50.90251127809988</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445850</ogr:osx>
      <ogr:osy>111650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.923101318075163,51.008169470092987</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>475650</ogr:osx>
      <ogr:osy>123750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.277540308474149,50.947051739293968</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450850</ogr:osx>
      <ogr:osy>116650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.224227656195614,50.814518836632196</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>454750</ogr:osx>
      <ogr:osy>101950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.344665709212375,50.93126112365988</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446150</ogr:osx>
      <ogr:osy>114850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.901103405070303,51.305161700131492</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>476700</ogr:osx>
      <ogr:osy>156800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.13875982626669,51.254097195244228</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460200</ogr:osx>
      <ogr:osy>150900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.081707163165248,50.80763916748905</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464800</ogr:osx>
      <ogr:osy>101300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.785490905955301,50.841628790329374</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>415200</ogr:osx>
      <ogr:osy>104700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.45506169072288,50.921486919073367</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438400</ogr:osx>
      <ogr:osy>113700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.75">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.379388950590486,50.941792054906884</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443700</ogr:osx>
      <ogr:osy>116000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
