<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.347183887590666</gml:X><gml:Y>50.81056133381685</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7671974059700289</gml:X><gml:Y>51.3578390231955</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.17514848882133,50.860944207220449</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458150</ogr:osx>
      <ogr:osy>107150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.321441829423256,50.962602106008958</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447750</ogr:osx>
      <ogr:osy>118350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.12317257321763,51.357839023195503</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461150</ogr:osx>
      <ogr:osy>162450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.314223663420527,51.065971970585089</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448150</ogr:osx>
      <ogr:osy>129850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.779902043298436,51.304411476773424</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485150</ogr:osx>
      <ogr:osy>156850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.042016527781214,51.323933358002286</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466850</ogr:osx>
      <ogr:osy>158750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.347183887590666,51.05626912480534</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445850</ogr:osx>
      <ogr:osy>128750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.29245582264452,50.902180553079468</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449850</ogr:osx>
      <ogr:osy>111650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.171777864083408,50.81056133381685</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458450</ogr:osx>
      <ogr:osy>101550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.117855951901537,51.260685632689658</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461650</ogr:osx>
      <ogr:osy>151650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.767197405970029,51.24313105091813</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486150</ogr:osx>
      <ogr:osy>150050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
