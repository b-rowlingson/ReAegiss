<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.796450104473215</gml:X><gml:Y>50.84223967720725</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8213927144662868</gml:X><gml:Y>51.33675848856419</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.821392714466287,51.336758488564186</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>482200</ogr:osx>
      <ogr:osy>160400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.796450104473215,50.934270342455711</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>414400</ogr:osx>
      <ogr:osy>115000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.137840354206786,50.842239677207253</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460800</ogr:osx>
      <ogr:osy>105100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.328257235087077,51.23015995273898</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447000</ogr:osx>
      <ogr:osy>148100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
