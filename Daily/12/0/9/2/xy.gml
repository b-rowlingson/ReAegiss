<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.531857370900643</gml:X><gml:Y>50.78243571760978</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9098153639992305</gml:X><gml:Y>51.3007470492512</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.531857370900643,50.782435717609779</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>433100</ogr:osx>
      <ogr:osy>98200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.90981536399923,51.3007470492512</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>476100</ogr:osx>
      <ogr:osy>156300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.34610155992303,50.879562279210255</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446100</ogr:osx>
      <ogr:osy>109100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
