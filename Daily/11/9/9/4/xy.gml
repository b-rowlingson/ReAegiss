<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.651977601788632</gml:X><gml:Y>50.74733475741436</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7377652985247934</gml:X><gml:Y>51.27159279487169</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.737765298524793,51.271592794871694</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>488150</ogr:osx>
      <ogr:osy>153250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.651977601788632,50.747334757414357</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>424650</ogr:osx>
      <ogr:osy>94250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.144961716293948,50.802274482891846</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460350</ogr:osx>
      <ogr:osy>100650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.299994374115399,51.0631899750285</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449150</ogr:osx>
      <ogr:osy>129550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.325900748399097,51.048055309634854</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447350</ogr:osx>
      <ogr:osy>127850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.330436919163765,50.931180438317249</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447150</ogr:osx>
      <ogr:osy>114850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.074089050860738,50.798136555790997</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465350</ogr:osx>
      <ogr:osy>100250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.109397202426522,51.253427947371044</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462250</ogr:osx>
      <ogr:osy>150850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.163118372864981,50.818593004038263</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459050</ogr:osx>
      <ogr:osy>102450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
