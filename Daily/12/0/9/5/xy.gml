<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.565850343940484</gml:X><gml:Y>50.80585204826833</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8292615884264679</gml:X><gml:Y>51.25321004330378</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.565850343940484,51.093703650693541</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>430500</ogr:osx>
      <ogr:osy>132800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.342603636923529,51.22844297777246</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446000</ogr:osx>
      <ogr:osy>147900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.849947248667739,51.17068593638718</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480500</ogr:osx>
      <ogr:osy>141900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.08316138741141,50.805852048268335</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464700</ogr:osx>
      <ogr:osy>101100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.829261588426468,51.253210043303781</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481800</ogr:osx>
      <ogr:osy>151100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.327495228156192,50.888449152604011</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447400</ogr:osx>
      <ogr:osy>110100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.334689715766138,51.081826635913082</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446700</ogr:osx>
      <ogr:osy>131600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
