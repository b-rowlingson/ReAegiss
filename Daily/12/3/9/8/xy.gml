<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.668223358326209</gml:X><gml:Y>50.74399718422423</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8412431120518135</gml:X><gml:Y>51.27082222870847</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.593184453872089,50.743997184224227</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>428800</ogr:osx>
      <ogr:osy>93900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.974356855950139,50.849011157677708</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472300</ogr:osx>
      <ogr:osy>106000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.499105041077526,50.927079508972469</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435300</ogr:osx>
      <ogr:osy>114300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.547641935963531,50.76361340679766</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>432000</ogr:osx>
      <ogr:osy>96100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.091142625690928,51.270822228708468</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463500</ogr:osx>
      <ogr:osy>152800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.042231863786511,50.794731942507106</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467600</ogr:osx>
      <ogr:osy>99900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.977861241406135,51.143097912125711</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471600</ogr:osx>
      <ogr:osy>138700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.507641810491299,50.927115795427426</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434700</ogr:osx>
      <ogr:osy>114300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.668223358326209,50.755025824021644</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423500</ogr:osx>
      <ogr:osy>95100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.841243112051813,51.118444123975642</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481200</ogr:osx>
      <ogr:osy>136100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
