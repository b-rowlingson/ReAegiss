<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.782958608391523</gml:X><gml:Y>50.77507926624465</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7536259543398024</gml:X><gml:Y>51.29469295760351</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.386516852812384,50.940930456366893</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443200</ogr:osx>
      <ogr:osy>115900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.160199132873065,51.099587618723262</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458900</ogr:osx>
      <ogr:osy>133700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.76210010286654,51.246224569647005</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486500</ogr:osx>
      <ogr:osy>150400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.753625954339802,51.294692957603509</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487000</ogr:osx>
      <ogr:osy>155800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.782958608391523,50.775079266244646</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>415400</ogr:osx>
      <ogr:osy>97300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
