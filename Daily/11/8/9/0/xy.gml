<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.487570732562118</gml:X><gml:Y>50.79544185274458</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7704239094163648</gml:X><gml:Y>51.31015729499232</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.341489798491825,51.055337969585253</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446250</ogr:osx>
      <ogr:osy>128650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.343584360963406,50.906975349654722</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446250</ogr:osx>
      <ogr:osy>112150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.420313932079452,50.912776829735535</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440850</ogr:osx>
      <ogr:osy>112750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.972649347350852,51.152494188269166</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471950</ogr:osx>
      <ogr:osy>139750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.770423909416365,51.310157294992322</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485800</ogr:osx>
      <ogr:osy>157500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.365938937029471,50.884167938315152</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444700</ogr:osx>
      <ogr:osy>109600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.16388425139029,50.815451043836354</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459000</ogr:osx>
      <ogr:osy>102100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.351599666793413,50.893081731078084</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445700</ogr:osx>
      <ogr:osy>110600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.487570732562118,51.199493575208763</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435900</ogr:osx>
      <ogr:osy>144600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.133025883560072,50.795441852744581</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461200</ogr:osx>
      <ogr:osy>99900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
