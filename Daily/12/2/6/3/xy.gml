<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.776896664956206</gml:X><gml:Y>50.75669567139247</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8376531624632785</gml:X><gml:Y>51.26138666831247</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.949869398621022,51.242664083293043</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>473400</ogr:osx>
      <ogr:osy>149800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.776896664956206,50.85689991997176</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>415800</ogr:osx>
      <ogr:osy>106400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.09184954817969,50.796927213394</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464100</ogr:osx>
      <ogr:osy>100100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.426122931082901,50.960914996624915</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440400</ogr:osx>
      <ogr:osy>118100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.314410794251999,50.908156628184891</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448300</ogr:osx>
      <ogr:osy>112300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.625681138008041,50.756695671392471</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>426500</ogr:osx>
      <ogr:osy>95300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.120013962846245,51.26025226631986</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461500</ogr:osx>
      <ogr:osy>151600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.837653162463278,51.261386668312468</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481200</ogr:osx>
      <ogr:osy>152000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.945383755718598,50.999829760021711</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>474100</ogr:osx>
      <ogr:osy>122800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
