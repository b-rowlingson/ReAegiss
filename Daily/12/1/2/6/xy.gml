<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.664663304963673</gml:X><gml:Y>50.75726381182594</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7827703624279102</gml:X><gml:Y>51.30444132143076</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.331503051328401,51.055281301737836</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446950</ogr:osx>
      <ogr:osy>128650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.070478366366004,51.264815183392422</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464950</ogr:osx>
      <ogr:osy>152150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.664663304963673,50.757263811825943</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423750</ogr:osx>
      <ogr:osy>95350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.358931630468036,50.928642540708758</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445150</ogr:osx>
      <ogr:osy>114550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.102953234177544,50.846922774622804</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463250</ogr:osx>
      <ogr:osy>105650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.073680258983838,50.818816680387378</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465350</ogr:osx>
      <ogr:osy>102550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.788743614596155,51.295511038347989</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484550</ogr:osx>
      <ogr:osy>155850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.575703217807815,50.874776997902764</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>429950</ogr:osx>
      <ogr:osy>108450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.860451365951957,51.237781352070641</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>479650</ogr:osx>
      <ogr:osy>149350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.174015588132451,51.245808741137722</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457750</ogr:osx>
      <ogr:osy>149950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.293809815271813,50.906685009814119</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449750</ogr:osx>
      <ogr:osy>112150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.185343903497041,50.846627558915927</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457450</ogr:osx>
      <ogr:osy>105550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.041660416948342,50.787982641942904</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467650</ogr:osx>
      <ogr:osy>99150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.062559277992421,50.807037282985327</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466150</ogr:osx>
      <ogr:osy>101250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.423091940636359,50.918186058919119</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440650</ogr:osx>
      <ogr:osy>113350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.78277036242791,51.304441321430758</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484950</ogr:osx>
      <ogr:osy>156850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
