<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.630632289093699</gml:X><gml:Y>50.75806034310121</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7374696667974397</gml:X><gml:Y>51.31229128419338</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.19">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.429896910815968,50.943398284985285</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440150</ogr:osx>
      <ogr:osy>116150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.20">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.988987692869127,50.862178273992136</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471250</ogr:osx>
      <ogr:osy>107450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.21">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.630632289093699,50.758060343101214</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>426150</ogr:osx>
      <ogr:osy>95450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.22">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.408162408331344,50.861458593292781</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441750</ogr:osx>
      <ogr:osy>107050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.23">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.570521981619835,50.819903662257552</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>430350</ogr:osx>
      <ogr:osy>102350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.24">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.598319478121625,51.21925860863746</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>428150</ogr:osx>
      <ogr:osy>146750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.25">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.953501198949282,51.240448556717034</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>473150</ogr:osx>
      <ogr:osy>149550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.26">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.367979907282781,50.89092339323615</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444550</ogr:osx>
      <ogr:osy>110350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.27">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.270139784614445,50.874166296591255</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451450</ogr:osx>
      <ogr:osy>108550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.13224163374609,50.799482764049969</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461250</ogr:osx>
      <ogr:osy>100350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.759605862162203,51.312291284193378</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486550</ogr:osx>
      <ogr:osy>157750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.862165145635914,51.110106726030907</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>479750</ogr:osx>
      <ogr:osy>135150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.495741327415614,50.777339311082095</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435650</ogr:osx>
      <ogr:osy>97650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.580885030335004,50.772280536238888</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>429650</ogr:osx>
      <ogr:osy>97050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.73746966679744,51.282380344047816</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>488150</ogr:osx>
      <ogr:osy>154450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.23271398363366,50.816373472089232</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>454150</ogr:osx>
      <ogr:osy>102150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.193132062787827,50.848030546726598</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456900</ogr:osx>
      <ogr:osy>105700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
