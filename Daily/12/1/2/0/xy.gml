<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.212665023769829</gml:X><gml:Y>50.78344006479929</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.015315784847143</gml:X><gml:Y>51.26391471525749</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.015315784847143,51.263914715257492</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468800</ogr:osx>
      <ogr:osy>152100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.092110771240139,50.783440064799294</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464100</ogr:osx>
      <ogr:osy>98600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.180601315589606,50.833554994973504</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457800</ogr:osx>
      <ogr:osy>104100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.212665023769829,50.953376908551867</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455400</ogr:osx>
      <ogr:osy>117400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.059435870322846,50.785879083055079</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466400</ogr:osx>
      <ogr:osy>98900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.065344289294041,50.84527879545653</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465900</ogr:osx>
      <ogr:osy>105500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
