<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.488395929535225</gml:X><gml:Y>50.96299757681826</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.312180295659985</gml:X><gml:Y>51.06556815858882</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.312180295659985,50.96299757681826</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448400</ogr:osx>
      <ogr:osy>118400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.488395929535225,50.995375122983617</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436000</ogr:osx>
      <ogr:osy>121900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.322078734728607,51.065568158588817</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447600</ogr:osx>
      <ogr:osy>129800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
