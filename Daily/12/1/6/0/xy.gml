<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.322578391105437</gml:X><gml:Y>50.85082859897106</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.037290370300429</gml:X><gml:Y>51.27643441200866</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.322578391105437,50.884374092144014</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447750</ogr:osx>
      <ogr:osy>109650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.037290370300429,50.862586596992813</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467850</ogr:osx>
      <ogr:osy>107450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.302834693926483,51.064106171164241</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448950</ogr:osx>
      <ogr:osy>129650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.061642041310458,51.276434412008662</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465550</ogr:osx>
      <ogr:osy>153450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.295781832379836,51.058668539540612</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449450</ogr:osx>
      <ogr:osy>129050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.312284163552459,50.907694500437309</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448450</ogr:osx>
      <ogr:osy>112250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.144074589455261,50.850828598971056</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460350</ogr:osx>
      <ogr:osy>106050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.297419863517558,51.233126182223458</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449150</ogr:osx>
      <ogr:osy>148450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.216424069175478,50.856732212614119</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455250</ogr:osx>
      <ogr:osy>106650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
