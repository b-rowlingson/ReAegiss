<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.561126979166842</gml:X><gml:Y>50.75955306865033</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.05991969661498</gml:X><gml:Y>51.21552835101975</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                               
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.561126979166842,51.215528351019749</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>430750</ogr:osx>
      <ogr:osy>146350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.153637488514471,50.793344980964164</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459750</ogr:osx>
      <ogr:osy>99650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.150621010203751,50.803215085109656</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459950</ogr:osx>
      <ogr:osy>100750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.05991969661498,50.797123984520248</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466350</ogr:osx>
      <ogr:osy>100150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.544136631505236,50.759553068650334</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>432250</ogr:osx>
      <ogr:osy>95650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
