<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.479336739287593</gml:X><gml:Y>50.78581029503162</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8945110711736162</gml:X><gml:Y>51.21226663327137</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.090767695692048,50.852673750926996</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464100</ogr:osx>
      <ogr:osy>106300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.199669534801351,51.212266633271369</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456000</ogr:osx>
      <ogr:osy>146200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.479336739287593,50.785810295031624</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436800</ogr:osx>
      <ogr:osy>98600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.144539475510886,50.786534065638193</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460400</ogr:osx>
      <ogr:osy>98900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.144539475510886,50.786534065638193</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460400</ogr:osx>
      <ogr:osy>98900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.144539475510886,50.786534065638193</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460400</ogr:osx>
      <ogr:osy>98900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.894511071173616,51.041624557924386</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>477600</ogr:osx>
      <ogr:osy>127500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.370045713075029,50.895880436114922</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444400</ogr:osx>
      <ogr:osy>110900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.317824192235999,51.063744962680808</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447900</ogr:osx>
      <ogr:osy>129600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.258409715242458,50.852060514530287</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452300</ogr:osx>
      <ogr:osy>106100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.399742103237344,50.908626316206757</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442300</ogr:osx>
      <ogr:osy>112300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.136221071380534,50.853018921029054</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460900</ogr:osx>
      <ogr:osy>106300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.214783689087517,50.827495073455587</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455400</ogr:osx>
      <ogr:osy>103400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
