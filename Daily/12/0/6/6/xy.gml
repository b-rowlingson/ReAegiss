<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.487391184399926</gml:X><gml:Y>50.79634952861997</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.016189168067089</gml:X><gml:Y>51.31719936982864</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.26">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.017193003859178,50.872311115568586</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469250</ogr:osx>
      <ogr:osy>108550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.27">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.075543264739967,50.79634952861997</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465250</ogr:osx>
      <ogr:osy>100050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.016189168067089,50.886241334495026</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469300</ogr:osx>
      <ogr:osy>110100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.160537746954912,51.317199369828643</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458600</ogr:osx>
      <ogr:osy>157900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.385484558925571,50.911249923159282</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443300</ogr:osx>
      <ogr:osy>112600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.021931218535956,50.883591770315022</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468900</ogr:osx>
      <ogr:osy>109800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.487391184399926,51.215678375610082</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435900</ogr:osx>
      <ogr:osy>146400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
