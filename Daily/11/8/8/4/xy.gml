<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.453438987459328</gml:X><gml:Y>50.78471667919875</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7483508603613678</gml:X><gml:Y>51.30407854999953</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.059667125439882,50.809711846966259</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466350</ogr:osx>
      <ogr:osy>101550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.380429005540174,50.917068327327868</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443650</ogr:osx>
      <ogr:osy>113250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.082865423738162,50.784716679198752</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464750</ogr:osx>
      <ogr:osy>98750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.081649823843249,50.846757121309167</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464750</ogr:osx>
      <ogr:osy>105650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.748350860361368,51.304078549999531</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487350</ogr:osx>
      <ogr:osy>156850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.277823984980182,50.882757015507885</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450900</ogr:osx>
      <ogr:osy>109500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.407446325445767,50.861904589077319</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441800</ogr:osx>
      <ogr:osy>107100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.453438987459328,50.938564953594089</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438500</ogr:osx>
      <ogr:osy>115600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
