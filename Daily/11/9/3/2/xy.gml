<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.568295757603943</gml:X><gml:Y>50.75335007374419</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.752631868537487</gml:X><gml:Y>51.30694297691497</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.499917607878185,50.917640941288553</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435250</ogr:osx>
      <ogr:osy>113250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.092416921962666,51.242507050738212</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463450</ogr:osx>
      <ogr:osy>149650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.209488363470379,50.846793553188917</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455750</ogr:osx>
      <ogr:osy>105550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.752631868537487,51.251969070096692</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487150</ogr:osx>
      <ogr:osy>151050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.764053944473687,51.306942976914968</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486250</ogr:osx>
      <ogr:osy>157150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.367507255640455,50.820778781347371</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444650</ogr:osx>
      <ogr:osy>102550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.416103145994703,50.908259622583813</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441150</ogr:osx>
      <ogr:osy>112250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.516876033518326,50.928503205899247</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434050</ogr:osx>
      <ogr:osy>114450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.326233223273702,50.926660032122918</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447450</ogr:osx>
      <ogr:osy>114350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.485378924998163,51.203530362518165</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436050</ogr:osx>
      <ogr:osy>145050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.01855733847573,50.87502040258633</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469150</ogr:osx>
      <ogr:osy>108850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.568295757603943,50.753350073744194</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>430550</ogr:osx>
      <ogr:osy>94950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
