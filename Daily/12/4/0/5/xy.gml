<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.369548020149014</gml:X><gml:Y>50.81024668930839</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.070301391447917</gml:X><gml:Y>51.24222750617599</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.22">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.369548020149014,50.932746921215021</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444400</ogr:osx>
      <ogr:osy>115000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.23">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.070301391447917,50.810246689308386</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465600</ogr:osx>
      <ogr:osy>101600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.24">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.114627512413514,51.242227506175993</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461900</ogr:osx>
      <ogr:osy>149600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
