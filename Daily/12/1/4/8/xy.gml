<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.514668539384677</gml:X><gml:Y>50.80097235749386</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7463933314078365</gml:X><gml:Y>51.28204934223689</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.746393331407836,51.244258966171799</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487600</ogr:osx>
      <ogr:osy>150200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.151371180770802,50.800972357493862</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459900</ogr:osx>
      <ogr:osy>100500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.281054018371857,50.857597718673489</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450700</ogr:osx>
      <ogr:osy>106700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.222061382969983,50.816752589876231</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>454900</ogr:osx>
      <ogr:osy>102200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.835697929015811,51.282049342236888</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481300</ogr:osx>
      <ogr:osy>154300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.068238344686163,50.842604116881795</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465700</ogr:osx>
      <ogr:osy>105200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.080023275996489,50.821115040399732</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464900</ogr:osx>
      <ogr:osy>102800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.514668539384677,51.208601030514508</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434000</ogr:osx>
      <ogr:osy>145600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.029560594723029,50.858475876702755</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468400</ogr:osx>
      <ogr:osy>107000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.043048484051811,50.893658684376966</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467400</ogr:osx>
      <ogr:osy>110900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
