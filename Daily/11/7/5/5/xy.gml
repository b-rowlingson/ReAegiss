<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.140672055667445</gml:X><gml:Y>50.80404152267504</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7462119113385278</gml:X><gml:Y>51.3036060603545</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.762813067265044,51.299736097974495</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486350</ogr:osx>
      <ogr:osy>156350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.119894620361683,50.852447342818863</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462050</ogr:osx>
      <ogr:osy>106250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.140672055667445,50.804041522675043</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460650</ogr:osx>
      <ogr:osy>100850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.746211911338528,51.303606060354497</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487500</ogr:osx>
      <ogr:osy>156800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
