<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.483746686420393</gml:X><gml:Y>50.79996931209848</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8923612230925074</gml:X><gml:Y>51.28646067309656</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.483746686420393,51.221507148659377</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436150</ogr:osx>
      <ogr:osy>147050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.390775371014815,50.996255938425854</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442850</ogr:osx>
      <ogr:osy>122050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.099157366372272,51.264140107798404</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>462950</ogr:osx>
      <ogr:osy>152050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.073858053721622,50.809825331227351</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465350</ogr:osx>
      <ogr:osy>101550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.091655435524698,51.281167069466917</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463450</ogr:osx>
      <ogr:osy>153950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.292938026406296,50.96423158792193</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449750</ogr:osx>
      <ogr:osy>118550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.176177630682104,50.802499023852</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458150</ogr:osx>
      <ogr:osy>100650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.200345315824569,50.799969312098483</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456450</ogr:osx>
      <ogr:osy>100350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.892361223092507,51.04205383220723</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>477750</ogr:osx>
      <ogr:osy>127550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.078645358760441,51.286460673096556</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464350</ogr:osx>
      <ogr:osy>154550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.125626354683723,51.266588745986155</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461100</ogr:osx>
      <ogr:osy>152300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.098484801055897,51.26188691454216</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463000</ogr:osx>
      <ogr:osy>151800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.120305592627975,51.244967931467976</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461500</ogr:osx>
      <ogr:osy>149900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.244059996857795,50.860961098955826</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>453300</ogr:osx>
      <ogr:osy>107100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.376265887780946,50.962457990731302</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443900</ogr:osx>
      <ogr:osy>118300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.298577845181311,50.920652128671101</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449400</ogr:osx>
      <ogr:osy>113700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
