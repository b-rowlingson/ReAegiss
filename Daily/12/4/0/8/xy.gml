<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.351462347446464</gml:X><gml:Y>50.79036331541863</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7551568167581125</gml:X><gml:Y>51.30856842324706</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.755156816758113,51.238057857825289</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487000</ogr:osx>
      <ogr:osy>149500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.290247688554166,50.907113043910506</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>450000</ogr:osx>
      <ogr:osy>112200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.083231755591752,50.802255492706188</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464700</ogr:osx>
      <ogr:osy>100700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.057927174670018,50.790363315418631</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466500</ogr:osx>
      <ogr:osy>99400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.351462347446464,50.902972731837508</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445700</ogr:osx>
      <ogr:osy>111700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.790551944881975,51.30856842324706</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484400</ogr:osx>
      <ogr:osy>157300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.131943094085616,50.853886456398378</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461200</ogr:osx>
      <ogr:osy>106400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
