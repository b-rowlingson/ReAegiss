<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.54771124395019</gml:X><gml:Y>50.75641954367921</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7454883308653909</gml:X><gml:Y>51.27752077440854</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.745488330865391,51.277520774408544</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487600</ogr:osx>
      <ogr:osy>153900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.004204676270936,50.914916275395868</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470100</ogr:osx>
      <ogr:osy>113300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.468950156385679,50.951225844193431</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437400</ogr:osx>
      <ogr:osy>117000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.54771124395019,50.756419543679208</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>432000</ogr:osx>
      <ogr:osy>95300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.33966653441642,50.932581842746487</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446500</ogr:osx>
      <ogr:osy>115000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.057854894390616,50.793959852568221</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466500</ogr:osx>
      <ogr:osy>99800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.517367007507416,51.087219166688577</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>433900</ogr:osx>
      <ogr:osy>132100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.352460369803201,50.933552703463853</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445600</ogr:osx>
      <ogr:osy>115100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.856012523395232,51.272358138448041</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>479900</ogr:osx>
      <ogr:osy>153200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
