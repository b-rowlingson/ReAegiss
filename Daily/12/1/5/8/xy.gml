<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.29725033292305</gml:X><gml:Y>50.7890773397386</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7915877900256586</gml:X><gml:Y>51.29643964060316</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                               
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.066350380325218,50.830448844953153</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465850</ogr:osx>
      <ogr:osy>103850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.836584980794557,51.275314003300238</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481250</ogr:osx>
      <ogr:osy>153550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.134913270578712,50.808495346035443</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461050</ogr:osx>
      <ogr:osy>101350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.791587790025659,51.296439640603161</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484350</ogr:osx>
      <ogr:osy>155950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.065755824531804,50.789077339738597</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465950</ogr:osx>
      <ogr:osy>99250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.29725033292305,50.961559872765797</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449450</ogr:osx>
      <ogr:osy>118250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.171295940881341,51.237696666234598</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457950</ogr:osx>
      <ogr:osy>149050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
