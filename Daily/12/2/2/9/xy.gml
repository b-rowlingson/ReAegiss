<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.553184902781648</gml:X><gml:Y>50.77712373142644</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8435424543624609</gml:X><gml:Y>51.25515042203111</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.354045172863655,50.921871271620489</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445500</ogr:osx>
      <ogr:osy>113800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.305102937093812,51.055577004127301</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448800</ogr:osx>
      <ogr:osy>128700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.204372166835479,50.855301806791289</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456100</ogr:osx>
      <ogr:osy>106500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.082897378373217,50.81933911111863</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464700</ogr:osx>
      <ogr:osy>102600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.174969434346843,50.83081751886116</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458200</ogr:osx>
      <ogr:osy>103800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.019962892853252,50.909653992486909</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469000</ogr:osx>
      <ogr:osy>112700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.354709068248731,50.976728778210216</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445400</ogr:osx>
      <ogr:osy>119900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.843542454362461,51.255150422031107</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480800</ogr:osx>
      <ogr:osy>151300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.553184902781648,50.777123731426435</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431600</ogr:osx>
      <ogr:osy>97600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.468779551651477,51.215595416826872</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437200</ogr:osx>
      <ogr:osy>146400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
