<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.562600497332174</gml:X><gml:Y>50.75602666685177</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.027214666795022</gml:X><gml:Y>51.2786887064019</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                               
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.064607241800602,50.846621799135868</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465950</ogr:osx>
      <ogr:osy>105650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.396789471303534,50.972007747092242</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442450</ogr:osx>
      <ogr:osy>119350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.061590413586075,50.784547509728206</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466250</ogr:osx>
      <ogr:osy>98750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.060027886016717,50.791729177680288</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466350</ogr:osx>
      <ogr:osy>99550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.078879098607446,51.27477284146137</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464350</ogr:osx>
      <ogr:osy>153250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.300003372195759,50.967871098701238</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449250</ogr:osx>
      <ogr:osy>118950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.268831172330502,50.866964041189163</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451550</ogr:osx>
      <ogr:osy>107750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.487658749338521,50.997619973432457</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436050</ogr:osx>
      <ogr:osy>122150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.027214666795022,50.868797968342875</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468550</ogr:osx>
      <ogr:osy>108150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.257163958694717,50.885774818238652</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452350</ogr:osx>
      <ogr:osy>109850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.214262445398649,50.816250712408426</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455450</ogr:osx>
      <ogr:osy>102150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.562600497332174,50.756026666851767</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>430950</ogr:osx>
      <ogr:osy>95250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.300003372195759,50.967871098701238</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449250</ogr:osx>
      <ogr:osy>118950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.120378707806595,51.278688706401901</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461450</ogr:osx>
      <ogr:osy>153650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
