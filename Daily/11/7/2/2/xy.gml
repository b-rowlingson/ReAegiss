<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.498172666304393</gml:X><gml:Y>50.78634578228473</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7685250385671418</gml:X><gml:Y>51.30069551963029</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.16323081784743,50.812298909601168</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459050</ogr:osx>
      <ogr:osy>101750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.061911039619083,50.839405984164408</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466150</ogr:osx>
      <ogr:osy>104850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.498172666304393,51.211678687626836</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435150</ogr:osx>
      <ogr:osy>145950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.264342096789731,50.881323975114391</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451850</ogr:osx>
      <ogr:osy>109350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.073626902810438,50.821514082327433</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465350</ogr:osx>
      <ogr:osy>102850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.768525038567142,51.300695519630288</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485950</ogr:osx>
      <ogr:osy>156450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.061554423293854,50.786345782284734</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466250</ogr:osx>
      <ogr:osy>98950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.33427907448618,51.060692503606397</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446750</ogr:osx>
      <ogr:osy>129250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.090257367999878,51.279357808383608</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463550</ogr:osx>
      <ogr:osy>153750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.356557115812714,50.894458026210543</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445350</ogr:osx>
      <ogr:osy>110750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.445638477534656,50.93628009845321</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439050</ogr:osx>
      <ogr:osy>115350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.072964699918106,51.283718232852245</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464750</ogr:osx>
      <ogr:osy>154250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.334733980664675,51.227948927913857</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446550</ogr:osx>
      <ogr:osy>147850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
