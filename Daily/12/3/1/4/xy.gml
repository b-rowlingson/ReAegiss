<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.344593278772331</gml:X><gml:Y>50.80839709142094</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.884204559035897</gml:X><gml:Y>51.14409631664275</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.24">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.324387021526249,50.808397091420943</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447700</ogr:osx>
      <ogr:osy>101200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.25">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.884204559035897,51.055015406492643</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>478300</ogr:osx>
      <ogr:osy>129000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.26">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.989275649951154,51.144096316642752</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470800</ogr:osx>
      <ogr:osy>138800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.27">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.201653882649798,50.848089135740885</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456300</ogr:osx>
      <ogr:osy>105700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.344593278772331,51.088177113882828</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446000</ogr:osx>
      <ogr:osy>132300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
