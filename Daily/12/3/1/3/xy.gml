<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.574969532578963</gml:X><gml:Y>50.72324380345776</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7650129312215812</gml:X><gml:Y>51.30226077546826</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.574969532578963,51.033489462191142</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>429900</ogr:osx>
      <ogr:osy>126100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.377034852687356,50.904910423486193</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443900</ogr:osx>
      <ogr:osy>111900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.765012931221581,51.244456872610058</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486300</ogr:osx>
      <ogr:osy>150200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.497788686605395,50.917182187257815</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435400</ogr:osx>
      <ogr:osy>113200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.354032726822425,50.92277045018627</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445500</ogr:osx>
      <ogr:osy>113900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.573530635147712,50.723243803457763</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>430200</ogr:osx>
      <ogr:osy>91600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.789283224657084,51.302260775468255</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484500</ogr:osx>
      <ogr:osy>156600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.368757552917756,50.885981712662407</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444500</ogr:osx>
      <ogr:osy>109800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.34408699905535,50.921815781257138</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446200</ogr:osx>
      <ogr:osy>113800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.423619969996495,50.93302619975924</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440600</ogr:osx>
      <ogr:osy>115000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.040960401642861,50.787527249426326</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467700</ogr:osx>
      <ogr:osy>99100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.399857668491711,50.899634424189024</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442300</ogr:osx>
      <ogr:osy>111300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
