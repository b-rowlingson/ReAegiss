<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.648430821521351</gml:X><gml:Y>50.74777376038644</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7936326465334902</gml:X><gml:Y>51.30050721495547</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.003002524981976,50.871291525208385</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470250</ogr:osx>
      <ogr:osy>108450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.119820954871454,51.232824960928014</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461550</ogr:osx>
      <ogr:osy>148550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.348909260768636,50.933083343468887</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445850</ogr:osx>
      <ogr:osy>115050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.79363264653349,51.300507214955474</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484200</ogr:osx>
      <ogr:osy>156400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.648430821521351,50.747773760386444</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>424900</ogr:osx>
      <ogr:osy>94300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.058189158289779,50.847919100508634</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466400</ogr:osx>
      <ogr:osy>105800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.857354933943551,51.102416246191169</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480100</ogr:osx>
      <ogr:osy>134300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.256431760610084,50.887119043274389</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452400</ogr:osx>
      <ogr:osy>110000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.394041968735368,50.799786856082015</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442800</ogr:osx>
      <ogr:osy>100200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.371484885450766,51.000200357755276</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444200</ogr:osx>
      <ogr:osy>122500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.371484885450766,51.000200357755276</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444200</ogr:osx>
      <ogr:osy>122500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
