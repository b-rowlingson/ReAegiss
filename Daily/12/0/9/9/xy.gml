<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.561199689671403</gml:X><gml:Y>50.74920356195516</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7288938057898436</gml:X><gml:Y>51.30768424875779</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.728893805789844,51.281388340856495</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>488750</ogr:osx>
      <ogr:osy>154350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.384423456967667,50.829861826094806</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443450</ogr:osx>
      <ogr:osy>103550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.175940440346412,50.815986427506488</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458150</ogr:osx>
      <ogr:osy>102150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.219355255181553,50.851356269991349</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455050</ogr:osx>
      <ogr:osy>106050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.398217004944974,50.861407761602592</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442450</ogr:osx>
      <ogr:osy>107050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.36237917137929,50.884598183282037</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444950</ogr:osx>
      <ogr:osy>109650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.561199689671403,50.754222874293994</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431050</ogr:osx>
      <ogr:osy>95050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.953893317728854,51.286312039442059</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>473050</ogr:osx>
      <ogr:osy>154650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.064661165741885,50.843924415784244</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465950</ogr:osx>
      <ogr:osy>105350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.455736297026604,50.924637417478834</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438350</ogr:osx>
      <ogr:osy>114050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.04587447892214,51.27630701335702</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466650</ogr:osx>
      <ogr:osy>153450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.268915523156417,50.861569012234952</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451550</ogr:osx>
      <ogr:osy>107150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.403888628660701,50.862336108467801</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442050</ogr:osx>
      <ogr:osy>107150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.792009839945934,51.307684248757788</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484300</ogr:osx>
      <ogr:osy>157200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.079881937703492,50.828308119352272</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464900</ogr:osx>
      <ogr:osy>103600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.318821594766364,50.898290667440435</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448000</ogr:osx>
      <ogr:osy>111200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.347535420093277,50.878671045045998</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446000</ogr:osx>
      <ogr:osy>109000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.262132007798594,50.886255943394744</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452000</ogr:osx>
      <ogr:osy>109900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.257881834052943,50.88532975420145</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452300</ogr:osx>
      <ogr:osy>109800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.542110904342684,50.749203561955156</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>432400</ogr:osx>
      <ogr:osy>94500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.494992340423404,50.912673913005499</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435600</ogr:osx>
      <ogr:osy>112700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.784957207681548,51.303115197293607</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484800</ogr:osx>
      <ogr:osy>156700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
