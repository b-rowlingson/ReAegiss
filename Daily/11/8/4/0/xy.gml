<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.432940242189447</gml:X><gml:Y>50.81155656266787</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.816219070289752</gml:X><gml:Y>51.34255121135794</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.342567264430542,50.87819355377421</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446350</ogr:osx>
      <ogr:osy>108950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.334756834989495,50.927608129955487</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446850</ogr:osx>
      <ogr:osy>114450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.40103551512764,50.863220763190299</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442250</ogr:osx>
      <ogr:osy>107250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.27090156096153,50.916436026825529</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451350</ogr:osx>
      <ogr:osy>113250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.816219070289752,51.342551211357936</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>482550</ogr:osx>
      <ogr:osy>161050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.432940242189447,50.927226725640118</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439950</ogr:osx>
      <ogr:osy>114350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.326470495541427,50.811556562667874</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447550</ogr:osx>
      <ogr:osy>101550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
