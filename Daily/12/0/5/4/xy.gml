<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.668312256883501</gml:X><gml:Y>50.74243635764441</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8492282157034937</gml:X><gml:Y>51.25700489064278</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.94239146296538,50.849175548653307</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>474550</ogr:osx>
      <ogr:osy>106050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.565485650305004,50.750641842550195</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>430750</ogr:osx>
      <ogr:osy>94650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.75">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.181413442800038,50.827715448619522</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457750</ogr:osx>
      <ogr:osy>103450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.76">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.271840996552369,50.85619173240471</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451350</ogr:osx>
      <ogr:osy>106550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.77">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.365090804409215,50.998816947885018</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444650</ogr:osx>
      <ogr:osy>122350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.78">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.173949201023333,51.089344411009783</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457950</ogr:osx>
      <ogr:osy>132550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.79">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.363059264414981,50.886850026166499</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444900</ogr:osx>
      <ogr:osy>109900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.80">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.365371871975465,50.821216822627648</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444800</ogr:osx>
      <ogr:osy>102600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.81">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.849228215703494,51.257004890642783</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480400</ogr:osx>
      <ogr:osy>151500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.82">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.249425322994744,50.880779418995985</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452900</ogr:osx>
      <ogr:osy>109300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.83">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.390774676351168,50.941851979076993</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442900</ogr:osx>
      <ogr:osy>116000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.84">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.668312256883501,50.742436357644408</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423500</ogr:osx>
      <ogr:osy>93700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
