<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.517731480148478</gml:X><gml:Y>50.82715859468852</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8283853587976844</gml:X><gml:Y>51.34402232357274</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.992962407596999,50.908525015019229</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470900</ogr:osx>
      <ogr:osy>112600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.988082195556887,51.133295217135199</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470900</ogr:osx>
      <ogr:osy>137600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.048667907553783,50.82715859468852</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467100</ogr:osx>
      <ogr:osy>103500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.33066660724722,51.063819175853183</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447000</ogr:osx>
      <ogr:osy>129600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.272586241162262,50.853948226017685</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451300</ogr:osx>
      <ogr:osy>106300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.379388950590486,50.941792054906884</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443700</ogr:osx>
      <ogr:osy>116000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.828385358797684,51.344022323572744</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481700</ogr:osx>
      <ogr:osy>161200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.517731480148478,50.914568483571799</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434000</ogr:osx>
      <ogr:osy>112900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.180617054291102,50.832655837406762</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457800</ogr:osx>
      <ogr:osy>104000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
