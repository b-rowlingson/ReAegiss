<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.330679568631063</gml:X><gml:Y>50.84086664260709</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8270512149168616</gml:X><gml:Y>51.28376149585424</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.827051214916862,51.283761495854243</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481900</ogr:osx>
      <ogr:osy>154500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.94018981828709,50.85230305210068</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>474700</ogr:osx>
      <ogr:osy>106400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.330679568631063,51.062920023353314</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447000</ogr:osx>
      <ogr:osy>129500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.19751633944864,50.840866642607089</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456600</ogr:osx>
      <ogr:osy>104900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
