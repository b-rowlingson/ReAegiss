<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.511900796841638</gml:X><gml:Y>50.81224024905153</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7563950020891891</gml:X><gml:Y>51.25670957081969</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.26">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.127246756017804,51.256709570819694</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461000</ogr:osx>
      <ogr:osy>151200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.27">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.756395002089189,51.245264872980968</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486900</ogr:osx>
      <ogr:osy>150300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.368332215913369,50.812240249051534</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444600</ogr:osx>
      <ogr:osy>101600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.004398290973318,51.172102059872991</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469700</ogr:osx>
      <ogr:osy>141900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.034537300971827,50.892689356904583</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468000</ogr:osx>
      <ogr:osy>110800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.035446797192735,50.848632666765951</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468000</ogr:osx>
      <ogr:osy>105900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.047211699562307,50.828945266623691</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467200</ogr:osx>
      <ogr:osy>103700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.511900796841638,50.928032906923718</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434400</ogr:osx>
      <ogr:osy>114400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
