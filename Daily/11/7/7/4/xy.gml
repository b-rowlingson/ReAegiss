<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.650600361888817</gml:X><gml:Y>50.74193504283508</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9958889034040103</gml:X><gml:Y>50.9377762877365</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.15236517646653,50.78524226189127</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459850</ogr:osx>
      <ogr:osy>98750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.04578758722643,50.794311434022895</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467350</ogr:osx>
      <ogr:osy>99850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.203470494307549,50.866536443410098</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456150</ogr:osx>
      <ogr:osy>107750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.650600361888817,50.741935042835081</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>424750</ogr:osx>
      <ogr:osy>93650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.99588890340401,50.937776287736497</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470650</ogr:osx>
      <ogr:osy>115850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.012740181491674,50.881266244757235</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469550</ogr:osx>
      <ogr:osy>109550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.170390551706507,50.808752936149901</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>458550</ogr:osx>
      <ogr:osy>101350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.592120489369363,50.784910016140806</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>428850</ogr:osx>
      <ogr:osy>98450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.398147577844014,50.866802932453879</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442450</ogr:osx>
      <ogr:osy>107650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
