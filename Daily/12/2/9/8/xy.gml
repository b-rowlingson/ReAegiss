<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.609089973665172</gml:X><gml:Y>50.79021118592286</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7811933437956998</gml:X><gml:Y>51.30982024358529</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.093407479409581,51.264994941409313</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463350</ogr:osx>
      <ogr:osy>152150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.377951460897809,50.99618839359303</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443750</ogr:osx>
      <ogr:osy>122050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.408091219490893,50.979259434959005</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441650</ogr:osx>
      <ogr:osy>120150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.971663109355189,50.874616777259888</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472450</ogr:osx>
      <ogr:osy>108850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.408604856848646,50.938796240981027</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441650</ogr:osx>
      <ogr:osy>115650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.0624873104941,50.810633814561314</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466150</ogr:osx>
      <ogr:osy>101650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.017155192584947,50.874109337513538</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469250</ogr:osx>
      <ogr:osy>108750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.095938942009246,50.842372310043736</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463750</ogr:osx>
      <ogr:osy>105150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.072332127425323,50.815208869533777</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465450</ogr:osx>
      <ogr:osy>102150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.19957726244292,50.84492748788729</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456450</ogr:osx>
      <ogr:osy>105350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.397602992873242,50.909064928067764</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442450</ogr:osx>
      <ogr:osy>112350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.80049855636854,51.11938221394616</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484050</ogr:osx>
      <ogr:osy>136250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.135304385578067,51.247327677669013</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460450</ogr:osx>
      <ogr:osy>150150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.095526462135043,50.790211185922857</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463850</ogr:osx>
      <ogr:osy>99350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.367495112856815,50.821677977949612</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444650</ogr:osx>
      <ogr:osy>102650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.321078559149137,50.889760906262154</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447850</ogr:osx>
      <ogr:osy>110250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.609089973665172,50.791262902269651</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>427650</ogr:osx>
      <ogr:osy>99150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.564883886602852,50.968258273551761</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>430650</ogr:osx>
      <ogr:osy>118850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.7811933437957,51.309820243585293</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485050</ogr:osx>
      <ogr:osy>157450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.428737943332057,50.921810785839902</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440250</ogr:osx>
      <ogr:osy>113750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.834036930740465,51.26269954823546</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481450</ogr:osx>
      <ogr:osy>152150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.342124212759891,50.909664884515848</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446350</ogr:osx>
      <ogr:osy>112450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.790764561064482,51.300477637940276</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484400</ogr:osx>
      <ogr:osy>156400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.002493505938683,50.861844821953142</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470300</ogr:osx>
      <ogr:osy>107400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
