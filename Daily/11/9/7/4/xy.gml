<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.662617547530935</gml:X><gml:Y>50.74601710033239</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8433781369485018</gml:X><gml:Y>51.34731852549671</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.125464113932081,51.237363299162951</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461150</ogr:osx>
      <ogr:osy>149050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.227515123850105,50.959321146610442</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>454350</ogr:osx>
      <ogr:osy>118050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.971259782475494,51.150683516103406</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472050</ogr:osx>
      <ogr:osy>139550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.046162924629532,50.845573269126859</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467250</ogr:osx>
      <ogr:osy>105550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.334115006499293,50.972566666863322</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446850</ogr:osx>
      <ogr:osy>119450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.43027031804179,50.912825813406684</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440150</ogr:osx>
      <ogr:osy>112750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.843378136948502,51.347318525496711</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480650</ogr:osx>
      <ogr:osy>161550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.662617547530935,50.746017100332395</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423900</ogr:osx>
      <ogr:osy>94100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.523320153439983,50.92448308760433</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>433600</ogr:osx>
      <ogr:osy>114000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.006755448542884,50.861881161089613</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470000</ogr:osx>
      <ogr:osy>107400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.001186968682371,50.922983882487152</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470300</ogr:osx>
      <ogr:osy>114200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
