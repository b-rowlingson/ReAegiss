<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.552685776487425</gml:X><gml:Y>50.75508981710959</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.829160328209942</gml:X><gml:Y>51.3417820414675</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                               
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.058908324810327,50.847475264802938</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466350</ogr:osx>
      <ogr:osy>105750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.135472903755083,51.238336840932824</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460450</ogr:osx>
      <ogr:osy>149150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.062253314322082,50.822322526259157</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466150</ogr:osx>
      <ogr:osy>102950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.350373182909475,50.827878783630688</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445850</ogr:osx>
      <ogr:osy>103350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.385855059538483,50.82897010577215</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443350</ogr:osx>
      <ogr:osy>103450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.552685776487425,50.755089817109585</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>431650</ogr:osx>
      <ogr:osy>95150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.829160328209942,51.341782041467496</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481650</ogr:osx>
      <ogr:osy>160950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.971346463989498,50.8890023300278</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472450</ogr:osx>
      <ogr:osy>110450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.925992989143255,51.006397570669876</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>475450</ogr:osx>
      <ogr:osy>123550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.120545566345857,51.232380801114701</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461500</ogr:osx>
      <ogr:osy>148500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.320465833963303,50.883012936662318</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447900</ogr:osx>
      <ogr:osy>109500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.291547023377515,50.915214195572524</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449900</ogr:osx>
      <ogr:osy>113100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.858605773914043,51.109622419867769</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480000</ogr:osx>
      <ogr:osy>135100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.328748854437486,50.9001466487736</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447300</ogr:osx>
      <ogr:osy>111400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.75">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.080005611875837,50.822014175772573</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464900</ogr:osx>
      <ogr:osy>102900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.76">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.354492805498227,50.889500748942837</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445500</ogr:osx>
      <ogr:osy>110200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
