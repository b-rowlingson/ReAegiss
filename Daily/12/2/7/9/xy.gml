<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.781204991913716</gml:X><gml:Y>50.84701633843205</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8343781932144708</gml:X><gml:Y>51.28038190958697</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.45403867493706,50.887310763825795</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438500</ogr:osx>
      <ogr:osy>109900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.834378193214471,51.277540110337682</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481400</ogr:osx>
      <ogr:osy>153800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.049374524577838,51.280381909586971</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466400</ogr:osx>
      <ogr:osy>153900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.781204991913716,50.847016338432049</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>415500</ogr:osx>
      <ogr:osy>105300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
