<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.398424154328906</gml:X><gml:Y>50.80061157049312</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7659280125376582</gml:X><gml:Y>51.31730377632837</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.306404629008554,50.968358891099911</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448800</ogr:osx>
      <ogr:osy>119000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.765928012537658,51.317303776328366</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>486100</ogr:osx>
      <ogr:osy>158300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.31456496649781,50.994484997988664</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448200</ogr:osx>
      <ogr:osy>121900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.398424154328906,50.900526298992709</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442400</ogr:osx>
      <ogr:osy>111400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.103130465441567,50.800611570493125</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463300</ogr:osx>
      <ogr:osy>100500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.38310909705894,51.198090526375715</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443200</ogr:osx>
      <ogr:osy>144500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.358350058809963,50.919197254457238</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445200</ogr:osx>
      <ogr:osy>113500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.249526392065064,50.874485260633996</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452900</ogr:osx>
      <ogr:osy>108600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
