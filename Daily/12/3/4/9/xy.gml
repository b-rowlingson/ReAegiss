<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.503197584968698</gml:X><gml:Y>50.85652013391389</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7836327409059818</gml:X><gml:Y>51.33651049803947</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.123358475596026,51.235099527769421</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461300</ogr:osx>
      <ogr:osy>148800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.402494462189458,50.91583440566091</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442100</ogr:osx>
      <ogr:osy>113100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.252656117289257,50.856520133913889</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452700</ogr:osx>
      <ogr:osy>106600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.065301520497812,51.131240017430926</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465500</ogr:osx>
      <ogr:osy>137300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.503197584968698,51.21035131838552</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434800</ogr:osx>
      <ogr:osy>145800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.249526392065064,50.874485260633996</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452900</ogr:osx>
      <ogr:osy>108600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.796994671642161,51.336510498039466</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>483900</ogr:osx>
      <ogr:osy>160400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.331899459570358,51.077314609084461</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446900</ogr:osx>
      <ogr:osy>131100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.389751411569281,50.91127229243051</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443000</ogr:osx>
      <ogr:osy>112600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.377437441749247,50.981348256569646</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443800</ogr:osx>
      <ogr:osy>120400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.783632740905982,51.244651817920726</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485000</ogr:osx>
      <ogr:osy>150200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.368222498361461,50.92554579383382</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444500</ogr:osx>
      <ogr:osy>114200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.069916037841851,51.257167443299394</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465000</ogr:osx>
      <ogr:osy>151300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
