<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.226825648800191</gml:X><gml:Y>50.95796771569596</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8601887088428199</gml:X><gml:Y>51.10334314562343</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.86018870884282,51.103343145623427</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>479900</ogr:osx>
      <ogr:osy>134400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.226825648800191,50.957967715695958</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>454400</ogr:osx>
      <ogr:osy>117900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
