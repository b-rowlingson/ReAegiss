<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.482985458034027</gml:X><gml:Y>50.85975030834209</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.9684379562623605</gml:X><gml:Y>51.26322972137267</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.25">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.448659270407334,50.981705940987602</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438800</ogr:osx>
      <ogr:osy>120400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.26">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.347133430595446,50.907444866145489</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446000</ogr:osx>
      <ogr:osy>112200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.27">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.019944015909907,50.910553098487334</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469000</ogr:osx>
      <ogr:osy>112800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.021066931822661,51.263063724288394</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468400</ogr:osx>
      <ogr:osy>152000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.482985458034027,51.225550176282844</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436200</ogr:osx>
      <ogr:osy>147500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.007821450963434,50.945521968368155</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469800</ogr:osx>
      <ogr:osy>116700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.041128846793613,51.263229721372667</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467000</ogr:osx>
      <ogr:osy>152000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.968437956262361,50.859750308342086</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472700</ogr:osx>
      <ogr:osy>107200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
