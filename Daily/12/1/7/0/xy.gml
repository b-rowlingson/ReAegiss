<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.649082411928363</gml:X><gml:Y>50.75541947860425</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8057569712551691</gml:X><gml:Y>51.23340022849445</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.431561273678515,50.923623047135635</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440050</ogr:osx>
      <ogr:osy>113950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.973182892433875,50.870133790235684</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>472350</ogr:osx>
      <ogr:osy>108350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.02081539066967,50.902916656464463</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468950</ogr:osx>
      <ogr:osy>111950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.985515237219917,51.152606458301136</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471050</ogr:osx>
      <ogr:osy>139750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.344681467626685,51.233400228494453</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445850</ogr:osx>
      <ogr:osy>148450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.041642037771237,50.788881772578335</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467650</ogr:osx>
      <ogr:osy>99250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.805756971255169,51.081667560420662</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>483750</ogr:osx>
      <ogr:osy>132050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.344681467626685,51.233400228494453</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445850</ogr:osx>
      <ogr:osy>148450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.807045524902716,51.087076223551229</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>483650</ogr:osx>
      <ogr:osy>132650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.649082411928363,50.755419478604246</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>424850</ogr:osx>
      <ogr:osy>95150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.156922752140672,50.847325027160032</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459450</ogr:osx>
      <ogr:osy>105650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
