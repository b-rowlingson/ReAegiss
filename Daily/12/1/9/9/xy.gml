<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.481341668948846</gml:X><gml:Y>50.80639817258412</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8669696025129842</gml:X><gml:Y>51.29314668418179</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.15553054572936,50.806398172584117</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459600</ogr:osx>
      <ogr:osy>101100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.131959779699638,50.85298731323973</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461200</ogr:osx>
      <ogr:osy>106300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.866969602512984,51.293146684181785</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>479100</ogr:osx>
      <ogr:osy>155500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.218313857435765,50.956112583232851</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>455000</ogr:osx>
      <ogr:osy>117700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.453375760397933,50.943960104820391</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438500</ogr:osx>
      <ogr:osy>116200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.440199036129526,50.974471838107419</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439400</ogr:osx>
      <ogr:osy>119600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.481341668948846,50.98904940853285</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436500</ogr:osx>
      <ogr:osy>121200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
