<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.664054165723449</gml:X><gml:Y>50.74332344527067</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7490801438855046</gml:X><gml:Y>51.30363672978218</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.21">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.086541603925486,50.850842219112678</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464400</ogr:osx>
      <ogr:osy>106100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.22">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.406441556026849,50.941033377991751</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441800</ogr:osx>
      <ogr:osy>115900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.23">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.749080143885505,51.303636729782177</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487300</ogr:osx>
      <ogr:osy>156800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.24">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.35464677403574,50.981224627699696</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445400</ogr:osx>
      <ogr:osy>120400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.25">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.371407022989324,50.900384023047309</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444300</ogr:osx>
      <ogr:osy>111400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.26">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.521906645371472,50.923578070274822</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>433700</ogr:osx>
      <ogr:osy>113900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.27">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.664054165723449,50.743323445270669</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>423800</ogr:osx>
      <ogr:osy>93800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.938708289042033,50.854987423985783</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>474800</ogr:osx>
      <ogr:osy>106700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
