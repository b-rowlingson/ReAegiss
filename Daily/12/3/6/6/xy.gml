<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.47414717085141</gml:X><gml:Y>50.81443899884975</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.148288326445807</gml:X><gml:Y>50.99531189238195</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                               
  <gml:featureMember>
    <ogr:points fid="points.25">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.47414717085141,50.995311892381949</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437000</ogr:osx>
      <ogr:osy>121900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.26">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.40296185084901,50.990473795195477</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442000</ogr:osx>
      <ogr:osy>121400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.27">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.148288326445807,50.814438998849745</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460100</ogr:osx>
      <ogr:osy>102000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
