<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.750946113503823</gml:X><gml:Y>50.79434944500309</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.063079979174919</gml:X><gml:Y>50.97633756306281</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.750946113503823,50.794349445003085</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>417650</ogr:osx>
      <ogr:osy>99450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.132308219085011,50.795886156513788</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461250</ogr:osx>
      <ogr:osy>99950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.371874364903356,50.812708996112931</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444350</ogr:osx>
      <ogr:osy>101650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.454420276551893,50.976337563062813</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438400</ogr:osx>
      <ogr:osy>119800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.063079979174919,50.81648387275829</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466100</ogr:osx>
      <ogr:osy>102300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
