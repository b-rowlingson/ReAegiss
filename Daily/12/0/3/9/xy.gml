<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.786194868205253</gml:X><gml:Y>50.76168829979521</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.052216819571625</gml:X><gml:Y>50.84297895464333</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.640528573538854,50.76168829979521</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>425450</ogr:osx>
      <ogr:osy>95850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.786194868205253,50.842978954643328</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>415150</ogr:osx>
      <ogr:osy>104850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.09453624730009,50.841462166956219</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463850</ogr:osx>
      <ogr:osy>105050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.145416988188537,50.816216553787093</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460300</ogr:osx>
      <ogr:osy>102200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.052216819571625,50.79211563233968</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466900</ogr:osx>
      <ogr:osy>99600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
