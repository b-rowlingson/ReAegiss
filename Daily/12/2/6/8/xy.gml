<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.486680347412969</gml:X><gml:Y>50.7969106603457</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.042881087845547</gml:X><gml:Y>51.21522566249103</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                               
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.332626454862325,50.977054376000396</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446950</ogr:osx>
      <ogr:osy>119950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.199489048860111,50.93305423626451</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456350</ogr:osx>
      <ogr:osy>115150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.251535695886581,50.882141851211728</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452750</ogr:osx>
      <ogr:osy>109450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.455589430317355,50.93722613081389</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438350</ogr:osx>
      <ogr:osy>115450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.438598922202083,50.929951785322359</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439550</ogr:osx>
      <ogr:osy>114650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.465674683320348,50.926481780480493</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437650</ogr:osx>
      <ogr:osy>114250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.30294551255837,50.961593986742137</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449050</ogr:osx>
      <ogr:osy>118250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.368428663655639,50.962865514843138</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444450</ogr:osx>
      <ogr:osy>118350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.042881087845547,50.867128916529779</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>467450</ogr:osx>
      <ogr:osy>107950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.40752431685921,50.911813414115336</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441750</ogr:osx>
      <ogr:osy>112650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.055572988003183,50.80158528096208</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466650</ogr:osx>
      <ogr:osy>100650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.486680347412969,51.215225662491029</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435950</ogr:osx>
      <ogr:osy>146350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.149316336660934,50.796910660345695</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460050</ogr:osx>
      <ogr:osy>100050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
