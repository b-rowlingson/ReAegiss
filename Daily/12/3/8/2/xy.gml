<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.4935894928878</gml:X><gml:Y>50.91086935124699</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.204400158233315</gml:X><gml:Y>50.98663534216301</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                                
  <gml:featureMember>
    <ogr:points fid="points.26">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.357421154650641,50.986635342163012</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445200</ogr:osx>
      <ogr:osy>121000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.27">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.4935894928878,50.910869351246994</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435700</ogr:osx>
      <ogr:osy>112500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.204400158233315,50.937134419981085</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>456000</ogr:osx>
      <ogr:osy>115600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
