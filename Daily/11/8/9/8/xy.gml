<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.374690989155542</gml:X><gml:Y>50.79724866670215</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7886767173203609</gml:X><gml:Y>51.07969334775358</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.368416477072499,50.963764689573473</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444450</ogr:osx>
      <ogr:osy>118450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.374690989155542,50.92063474111913</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444050</ogr:osx>
      <ogr:osy>113650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.788676717320361,51.079693347753583</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484950</ogr:osx>
      <ogr:osy>131850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.075525530217827,50.797248666702153</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465250</ogr:osx>
      <ogr:osy>100150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
