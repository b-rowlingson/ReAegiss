<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.478659082178929</gml:X><gml:Y>50.81220663138598</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7444284791464234</gml:X><gml:Y>51.3310228775534</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.420146395423218,50.92626468153172</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440850</ogr:osx>
      <ogr:osy>114250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.744428479146423,51.237493662767434</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>487750</ogr:osx>
      <ogr:osy>149450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.150457993380999,50.812206631385976</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459950</ogr:osx>
      <ogr:osy>101750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.428440574392377,50.946088894289417</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440250</ogr:osx>
      <ogr:osy>116450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.019317948422762,50.906501151673353</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469050</ogr:osx>
      <ogr:osy>112350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.444311812737453,50.928180610729214</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439150</ogr:osx>
      <ogr:osy>114450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.36322459570365,50.926867527283854</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444850</ogr:osx>
      <ogr:osy>114350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.99324964830512,50.862215106451927</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470950</ogr:osx>
      <ogr:osy>107450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.832305099678913,51.331022877553401</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481450</ogr:osx>
      <ogr:osy>159750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.439892249575842,50.940748902833583</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439450</ogr:osx>
      <ogr:osy>115850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.975369071027086,51.158812649614681</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471750</ogr:osx>
      <ogr:osy>140450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.428748949087846,50.920911594662485</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>440250</ogr:osx>
      <ogr:osy>113650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.271644963125179,50.868780145200326</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>451350</ogr:osx>
      <ogr:osy>107950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.302226444713447,51.104567650330985</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448950</ogr:osx>
      <ogr:osy>134150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.374618694487153,50.926029833126343</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444050</ogr:osx>
      <ogr:osy>114250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.817154995156322,51.27826662635065</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>482600</ogr:osx>
      <ogr:osy>153900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.033729374757499,50.863006812532518</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>468100</ogr:osx>
      <ogr:osy>107500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.478659082178929,51.228228584372516</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436500</ogr:osx>
      <ogr:osy>147800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.864146258535989,51.291320902724472</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>479300</ogr:osx>
      <ogr:osy>155300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.987846363766997,51.144083959484135</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470900</ogr:osx>
      <ogr:osy>138800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
