<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.486740297408689</gml:X><gml:Y>50.80792500799235</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.061122252347636</gml:X><gml:Y>51.34983281480716</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.40038969606095,50.913575503503246</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442250</ogr:osx>
      <ogr:osy>112850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.134812854218193,51.349832814807165</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460350</ogr:osx>
      <ogr:osy>161550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.434493781477603,50.916443319001225</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439850</ogr:osx>
      <ogr:osy>113150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.339343383692769,50.905152937867449</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446550</ogr:osx>
      <ogr:osy>111950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.061122252347636,50.807925007992353</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466250</ogr:osx>
      <ogr:osy>101350</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.367894745907254,50.89721769089752</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444550</ogr:osx>
      <ogr:osy>111050</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.076572184931411,50.816141771917948</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465150</ogr:osx>
      <ogr:osy>102250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.180817821522889,50.942816564495374</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457650</ogr:osx>
      <ogr:osy>116250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.486740297408689,51.209830734260812</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435950</ogr:osx>
      <ogr:osy>145750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.070636280314339,50.90062598901558</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465450</ogr:osx>
      <ogr:osy>111650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
