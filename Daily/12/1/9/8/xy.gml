<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.638460727891749</gml:X><gml:Y>50.75403816222136</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.8292615884264679</gml:X><gml:Y>51.26152803383163</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.455093183914806,50.918789332886867</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438400</ogr:osx>
      <ogr:osy>113400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.002301831283866,50.937381609430538</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470200</ogr:osx>
      <ogr:osy>115800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.394973272266433,50.837560685417458</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442700</ogr:osx>
      <ogr:osy>104400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.25228234096408,50.879898490375631</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>452700</ogr:osx>
      <ogr:osy>109200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.829261588426468,51.253210043303781</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481800</ogr:osx>
      <ogr:osy>151100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.321728311540175,50.991829075623073</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>447700</ogr:osx>
      <ogr:osy>121600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.433547930652269,50.935772484868281</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439900</ogr:osx>
      <ogr:osy>115300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.494154069544129,50.9900047900489</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435600</ogr:osx>
      <ogr:osy>121300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.492186753457986,50.909064772386692</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>435800</ogr:osx>
      <ogr:osy>112300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.638460727891749,50.754038162221356</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>425600</ogr:osx>
      <ogr:osy>95000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.945383755718598,50.999829760021711</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>474100</ogr:osx>
      <ogr:osy>122800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.39818081100552,50.919409248044445</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442400</ogr:osx>
      <ogr:osy>113500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.145826850233327,50.793737706205341</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460300</ogr:osx>
      <ogr:osy>99700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.440814042661834,50.923218079911969</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439400</ogr:osx>
      <ogr:osy>113900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.913953803818924,51.003138907643589</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>476300</ogr:osx>
      <ogr:osy>123200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.439380579173856,50.924110454134144</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439500</ogr:osx>
      <ogr:osy>114000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.851981712068343,51.261528033831631</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>480200</ogr:osx>
      <ogr:osy>152000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
