<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.757817238918364</gml:X><gml:Y>50.79172917768029</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7879438270860032</gml:X><gml:Y>51.29865003868755</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.511297669700379,50.917689099095746</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434450</ogr:osx>
      <ogr:osy>113250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.060027886016717,50.791729177680288</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466350</ogr:osx>
      <ogr:osy>99550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.757817238918364,50.837528079966908</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>417150</ogr:osx>
      <ogr:osy>104250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.368720954078366,50.941285278875867</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>444450</ogr:osx>
      <ogr:osy>115950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.403737540829021,50.98553195918268</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441950</ogr:osx>
      <ogr:osy>120850</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.401866669463571,50.853782840695111</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>442200</ogr:osx>
      <ogr:osy>106200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.064301001700503,50.897427975152091</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465900</ogr:osx>
      <ogr:osy>111300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.787943827086003,51.298650038687555</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484600</ogr:osx>
      <ogr:osy>156200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.466020182391575,51.20659087173545</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437400</ogr:osx>
      <ogr:osy>145400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.094773802288861,50.792453545393982</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>463900</ogr:osx>
      <ogr:osy>99600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
