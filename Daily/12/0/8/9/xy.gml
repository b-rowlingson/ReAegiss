<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.481331652608261</gml:X><gml:Y>50.92604713760847</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7709281431225355</gml:X><gml:Y>51.29535296092994</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.909943103679428,51.295352960929939</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>476100</ogr:osx>
      <ogr:osy>155700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.471225925530277,51.001593337228485</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>437200</ogr:osx>
      <ogr:osy>122600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.770928143122536,51.2912789238557</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485800</ogr:osx>
      <ogr:osy>155400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.314207214790503,51.018762271129887</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>448200</ogr:osx>
      <ogr:osy>124600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.446462035147488,50.926841952957965</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439000</ogr:osx>
      <ogr:osy>114300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.481331652608261,50.989948597005885</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436500</ogr:osx>
      <ogr:osy>121300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.29849674736621,50.926047137608471</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449400</ogr:osx>
      <ogr:osy>114300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
