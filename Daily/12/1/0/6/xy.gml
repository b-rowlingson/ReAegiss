<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.661122379458665</gml:X><gml:Y>50.75680395300063</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7770098384078065</gml:X><gml:Y>51.30528053343112</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.777009838407806,51.305280533431123</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485350</ogr:osx>
      <ogr:osy>156950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.303684199928711,50.91213980302863</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>449050</ogr:osx>
      <ogr:osy>112750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.5071345648235,50.907779926526473</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434750</ogr:osx>
      <ogr:osy>112150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.338349117762402,50.975288437294367</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>446550</ogr:osx>
      <ogr:osy>119750</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.074053522842794,50.799934830519121</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465350</ogr:osx>
      <ogr:osy>100450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.349222802748449,50.910603899572742</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445850</ogr:osx>
      <ogr:osy>112550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.988987692869127,50.862178273992136</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>471250</ogr:osx>
      <ogr:osy>107450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.61007959633561,50.757543856263787</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>427600</ogr:osx>
      <ogr:osy>95400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.063924120550182,50.845267415467781</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>466000</ogr:osx>
      <ogr:osy>105500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.07333523462487,50.800378767769132</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465400</ogr:osx>
      <ogr:osy>100500</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.357743579786891,50.963256895250026</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>445200</ogr:osx>
      <ogr:osy>118400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.661122379458665,50.756803953000627</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>424000</ogr:osx>
      <ogr:osy>95300</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.790764561064482,51.300477637940276</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>484400</ogr:osx>
      <ogr:osy>156400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.486980960654517,50.994469689687023</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436100</ogr:osx>
      <ogr:osy>121800</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.075521138983189,51.263506290594997</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464600</ogr:osx>
      <ogr:osy>152000</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.147081878876601,50.802739635109013</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>460200</ogr:osx>
      <ogr:osy>100700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.121823643243075,51.240483282849283</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>461400</ogr:osx>
      <ogr:osy>149400</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.451910190449538,50.947550201486834</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>438600</ogr:osx>
      <ogr:osy>116600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.07881606027323,50.810314211418991</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465000</ogr:osx>
      <ogr:osy>101600</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
