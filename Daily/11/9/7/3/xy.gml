<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.433890661600494</gml:X><gml:Y>50.79246636697422</gml:Y></gml:coord>
      <gml:coord><gml:X>-0.7708561432076046</gml:X><gml:Y>51.29397583798722</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                             
  <gml:featureMember>
    <ogr:points fid="points.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.822160281741774,51.278766688764712</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>482250</ogr:osx>
      <ogr:osy>153950</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.067358044293108,50.851140165296655</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465750</ogr:osx>
      <ogr:osy>106150</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.156490927069128,50.792466366974217</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>459550</ogr:osx>
      <ogr:osy>99550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.411836548185358,50.908238217188448</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>441450</ogr:osx>
      <ogr:osy>112250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.013781847468435,50.899260402908254</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>469450</ogr:osx>
      <ogr:osy>111550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.003810167276694,50.900075026827771</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470150</ogr:osx>
      <ogr:osy>111650</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.005277418536028,50.864566393248005</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>470100</ogr:osx>
      <ogr:osy>107700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.433890661600494,51.258593357543077</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>439600</ogr:osx>
      <ogr:osy>151200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.842671537765579,51.118458267452723</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>481100</ogr:osx>
      <ogr:osy>136100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.388025863376672,50.825834093249284</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>443200</ogr:osx>
      <ogr:osy>103100</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.077467768443197,50.806706453893739</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>465100</ogr:osx>
      <ogr:osy>101200</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-0.770856143207605,51.293975837987219</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>485800</ogr:osx>
      <ogr:osy>155700</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
