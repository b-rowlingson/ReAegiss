<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ xy.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-1.506050983131573</gml:X><gml:Y>50.81893955885958</gml:Y></gml:coord>
      <gml:coord><gml:X>-1.089293381706895</gml:X><gml:Y>51.21126258861593</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:points fid="points.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.483373930664722,50.99850034657814</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>436350</ogr:osx>
      <ogr:osy>122250</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.089293381706895,50.818939558859583</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>464250</ogr:osx>
      <ogr:osy>102550</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.177942446415644,50.863661704679807</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>457950</ogr:osx>
      <ogr:osy>107450</ogr:osy>
    </ogr:points>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:points fid="points.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>-1.506050983131573,51.211262588615931</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osx>434600</ogr:osx>
      <ogr:osy>145900</ogr:osy>
    </ogr:points>
  </gml:featureMember>
</ogr:FeatureCollection>
